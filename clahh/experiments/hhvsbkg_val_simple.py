import torch
import torch.nn as nn
import numpy as np
import awkward as ak
from tqdm import tqdm
from pathlib import Path
from torch.utils.data import DataLoader
from clahh.models.simplenn import SimpleNeuralNetwork
from clahh.utils.data_handler import DataHandler
from clahh.plotting.metrics import (
    plot_vars,
    plot_probabilities,
    plot_discriminants,
)
from clahh.utils.metrics import get_op


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def run(model_path, file_paths, fast_dev_run=False):
    print("Using device:", device)

    stop_event = -1  # select up to second to last event
    batch_size = 32
    max_jets = 20
    event_feature_names = ["X_hh"]
    label_names = ["label_HH", "label_QCD", "label_ttbar"]
    weight_names = ["event_weight"]
    spectator_names = ["jet_pt", "jet_eta", "jet_phi", "jet_mass"]

    if fast_dev_run:
        stop_event = 1_000

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    data_handler = DataHandler(
        event_feature_names=event_feature_names,
        label_names=label_names,
        spectator_names=spectator_names + ["jet_btag"],
        weight_names=weight_names,
        remove_unlabeled=True,
        stop_event=stop_event,
        npad=max_jets,
        sources=file_paths,
    )
    data_handler.load()
    event_selection = {
        # "trigs": {"operator": "or", "value": "Main physics stream"},
        "central_jets": {
            "pt": {"operator": ">", "value": 40000},
            "eta": {"operator": "<", "value": 2.5},
            "count": {"operator": ">=", "value": 4},
        },
        "btagging": {
            "model": "DL1dv01",
            "efficiency": 0.85,
            "count": {"operator": ">=", "value": 4},
        },
        "top_veto": {"operator": ">", "value": 1.5},
        "hh_deltaeta_veto": {"operator": "<", "value": 1.5},
        "hh_mass_veto": {
            "signal": {"inner_boundry": {"operator": "<", "value": 1.6}},
            "control": {
                "inner_boundry": {"operator": ">=", "value": 1.6},
                "outer_boundry": {"operator": "<=", "value": 45},
            },
        },
    }
    # load test dataset indices if any
    test_dataset_indices = None
    events = data_handler[:]
    if Path("../test_dataset_indices.npy").exists() and not fast_dev_run:
        test_dataset_indices = np.load("../test_dataset_indices.npy")
        test_dataset = data_handler[test_dataset_indices]

    plot_vars(
        events,
        label_names=label_names,
        var_names=spectator_names,
        apply_weights=False,
        plot_path=plot_path,
    )
    plot_vars(
        events,
        label_names=label_names,
        var_names=spectator_names,
        apply_weights=True,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )
    plot_vars(
        events,
        label_names=label_names,
        var_names=["event_weight"],
        plot_path=plot_path,
    )

    test_sample = data_handler.to_torch(test_dataset)
    test_sample_size = len(test_sample)
    print(f"Number of testing samples: {test_sample_size}")
    test_loader = DataLoader(data_handler.datasets, batch_size=batch_size, shuffle=True)

    #############################
    # Evaluate on testing data
    #############################
    print("Loading model...")
    # create model
    model = SimpleNeuralNetwork(
        input_size=len(event_feature_names),
        hidden_size=512,
        output_size=len(label_names),
    )
    # load model
    model.load_state_dict(
        torch.load(
            model_path,
            map_location=torch.device(device),
        )
    )

    model.eval()
    y_test = []
    y_predict = []
    weights = []
    for _, batch in tqdm(
        enumerate(test_loader),
        total=np.round(test_sample_size / batch_size),
        ncols=80,
        desc="Evaluating model",
    ):
        X = batch[1].to(device)
        y = batch[2].to(device)
        w = batch[3].to(device)
        batch_output = model(X)
        pred_probab = nn.Softmax(dim=1)(batch_output)
        y_predict.append(pred_probab.detach().cpu().numpy())
        y_test.append(y.cpu().numpy())
        weights.append(w.cpu().numpy())
    y_test = np.concatenate(y_test)
    y_predict = np.concatenate(y_predict)
    weights = np.concatenate(weights)

    plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        plot_path=plot_path,
    )

    plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    #############################
    # Use puma for ROCS
    ############################

    from puma import Roc, RocPlot
    from ftag import Flavour
    from clahh.utils.metrics import get_score

    # from puma.metrics import calc_rej

    from clahh.utils.metrics import calc_rej

    clahh_discs = get_score(
        y_predict,
        class_labels=label_names,
        main_class="label_HH",
        frac_dict={"label_QCD": 0.82, "label_ttbar": 1 - 0.82},
    )

    # define classes
    QCD_bkg_class = Flavour("QCD", "label_QCD", None, "blue", None)
    ttbar_bkg_class = Flavour("ttbar", "label_ttbar", None, "blue", None)
    signal_class = Flavour("HH", "label_HH", None, "blue", None)

    # set mask for each class
    is_HH = y_test[:, 0] == 1
    is_QCD = y_test[:, 1] == 1
    is_ttbar = y_test[:, 2] == 1

    infar = np.array([np.inf])
    edges = np.concatenate([-infar, np.linspace(-20, 20, 1000), infar])
    clahh_disc_hists = [
        np.histogram(clahh_discs[class_mask], edges, weights=weights[class_mask])[0]
        for class_mask in [is_HH, is_QCD, is_ttbar]
    ]

    clahh_HH_eff = clahh_disc_hists[0][::-1].cumsum() / clahh_disc_hists[0].sum()
    clahh_QCD_eff = clahh_disc_hists[1][::-1].cumsum() / clahh_disc_hists[1].sum()
    clahh_ttbar_eff = clahh_disc_hists[2][::-1].cumsum() / clahh_disc_hists[2].sum()
    valid = (clahh_HH_eff > 0) & (clahh_QCD_eff > 0) & (clahh_ttbar_eff > 0)
    clahh_QCD_rej = 1 / clahh_QCD_eff[valid]
    clahh_ttbar_rej = 1 / clahh_ttbar_eff[valid]
    clahh_HH_eff = clahh_HH_eff[valid]

    #############################
    # define the X_HH discriminant
    #############################
    hh_mass_veto = {"operator": "<", "value": 1.6}
    X_HH_disc = test_dataset["X_hh"].to_numpy()
    X_HH_mask = get_op(hh_mass_veto["operator"])(X_HH_disc, hh_mass_veto["value"])
    event_weight = test_dataset["event_weight"].to_numpy()

    HH_mask = (test_dataset["label_HH"] == 1).to_numpy()
    QCD_mask = (test_dataset["label_QCD"] == 1).to_numpy()
    ttbar_mask = (test_dataset["label_ttbar"] == 1).to_numpy()

    disc_X_HH = -X_HH_disc  # multiply disc_X_HH by a negative sign to make it a rejection
    edges = np.concatenate([-infar, np.linspace(-20, 20, 1000), infar])
    X_HH_disc_hists = [
        np.histogram(
            disc_X_HH[class_mask],
            edges,
            weights=event_weight[class_mask],
        )[0]
        for class_mask in [HH_mask, QCD_mask, ttbar_mask]
    ]
    X_HH_HH_eff = X_HH_disc_hists[0][::-1].cumsum() / clahh_disc_hists[0].sum()
    X_HH_QCD_eff = X_HH_disc_hists[1][::-1].cumsum() / clahh_disc_hists[1].sum()
    X_HH_ttbar_eff = X_HH_disc_hists[2][::-1].cumsum() / clahh_disc_hists[2].sum()
    valid = (X_HH_HH_eff > 0) & (X_HH_QCD_eff > 0) & (X_HH_ttbar_eff > 0)
    X_HH_QCD_rej = 1 / X_HH_QCD_eff[valid]
    X_HH_ttbar_rej = 1 / X_HH_ttbar_eff[valid]
    X_HH_HH_eff = X_HH_HH_eff[valid]

    # create ROC plot
    plot_roc = RocPlot(
        n_ratio_panels=2,
        ylabel="Background rejection",
        xlabel="Signal efficiency",
        atlas_second_tag="$\\sqrt{s}=13$ TeV, Work in Progress, $f_{QCD}=0.82$",
        figsize=(6.5, 6),
        y_scale=1.4,
    )

    # add ROC curves
    plot_roc.add_roc(
        Roc(
            X_HH_QCD_eff,
            X_HH_QCD_rej,
            n_test=sum(is_QCD),
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            label="X_HH",
        ),
        reference=True,
    )
    plot_roc.add_roc(
        Roc(
            clahh_HH_eff,
            clahh_QCD_rej,
            n_test=sum(is_QCD),
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            label="CLAHH",
        ),
    )
    plot_roc.add_roc(
        Roc(
            clahh_ttbar_eff,
            clahh_ttbar_rej,
            n_test=sum(is_ttbar),
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            label="X_HH",
        ),
        reference=True,
    )
    plot_roc.add_roc(
        Roc(
            X_HH_HH_eff,
            X_HH_ttbar_rej,
            n_test=sum(is_ttbar),
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            label="CLAHH",
        ),
    )

    # setting which class rejection ratio is drawn in which ratio panel
    plot_roc.set_ratio_class(1, QCD_bkg_class)
    plot_roc.set_ratio_class(2, ttbar_bkg_class)
    plot_roc.draw()
    plot_roc.savefig(plot_path / "roc_weighted.png", transparent=False)
