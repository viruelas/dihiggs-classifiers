import torch
import numpy as np
import torch.nn as nn
import os.path as osp
from pathlib import Path
from tqdm import tqdm, trange
from clahh.models.simplenn import SimpleNeuralNetwork
from torch.utils.data import random_split, DataLoader
from clahh.utils.data_handler import DataHandler
from clahh.plotting.metrics import (
    plot_metrics,
    plot_roc,
    plot_probabilities,
    plot_discriminants,
    plot_vars,
    plot_roc_rej_v_eff,
    visualize_class_count,
)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def run(file_paths, fast_dev_run=False):
    print("Using device:", device)

    stop_event = -1  # select up to second to last event
    n_epochs = 30
    batch_size = 32
    jet_feature_names = []
    event_feature_names = ["X_hh"]
    label_names = ["label_HH", "label_QCD", "label_ttbar"]
    weight_names = ["event_weight"]
    spectator_names = ["jet_pt", "jet_eta", "jet_phi", "jet_mass"]
    test_frac = 0.20
    valid_frac = 0.15
    train_frac = 1 - test_frac - valid_frac
    rnd_seed = 42
    torchgen = torch.Generator().manual_seed(rnd_seed)

    if fast_dev_run:
        stop_event = 1_001
        n_epochs = 10

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    data_handler = DataHandler(
        jet_feature_names=jet_feature_names,
        event_feature_names=event_feature_names,
        label_names=label_names,
        weight_names=weight_names,
        spectator_names=spectator_names,
        remove_unlabeled=True,
        stop_event=stop_event,
        sources=file_paths,
        rnd_seed=rnd_seed,
    )
    data_handler.load()
    plot_vars(
        data_handler.datasets,
        var_names=event_feature_names,
        bins=np.linspace(-2, 70, 30),
        plot_path=plot_path,
    )

    plot_vars(
        data_handler.datasets,
        var_names=event_feature_names,
        label_names=label_names,
        bins=np.linspace(-2, 70, 30),
        plot_path=plot_path,
        plot_name_postfix="per_class",
    )

    plot_vars(
        data_handler.datasets,
        var_names=spectator_names,
        label_names=label_names,
        plot_path=plot_path,
        plot_name_postfix="weighted",
    )

    plot_vars(
        data_handler.datasets,
        var_names=spectator_names,
        label_names=label_names,
        apply_weights=True,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    data_handler.to_torch()
    train_sample, valid_sample, test_sample = random_split(
        data_handler.datasets, [train_frac, test_frac, valid_frac], generator=torchgen
    )
    n_total_samples = len(data_handler.datasets)
    n_train_samples = len(train_sample)
    n_valid_samples = len(valid_sample)
    n_test_samples = len(test_sample)
    print("Number of total samples:", n_total_samples)
    print("Number of training samples:", n_train_samples)
    print("Number of validation samples:", n_valid_samples)
    print("Number of testing samples:", n_test_samples)

    # save indices to avoid re-splitting
    np.save("test_dataset_indices", test_sample.indices)

    train_loader = DataLoader(train_sample, batch_size=batch_size, shuffle=True)
    valid_loader = DataLoader(valid_sample, batch_size=batch_size, shuffle=False)
    test_loader = DataLoader(test_sample, batch_size=batch_size, shuffle=False)

    visualize_class_count(
        train_loader, label_names=label_names, plot_name=plot_path / "class_counts_raw.png"
    )

    #############################
    # Create model
    #############################

    model = SimpleNeuralNetwork(
        input_size=len(event_feature_names),
        hidden_size=512,
        output_size=len(label_names),
    ).to(device)
    print(model)

    #############################
    # Train
    #############################

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    stale_epochs = 0
    best_valid_loss = 99999
    patience = 5
    total_loss = []
    total_valid_loss = []
    class_weights = data_handler._compute_class_weight(
        labels=data_handler.datasets[train_sample.indices][2].numpy()
    )
    class_weights = torch.from_numpy(class_weights).float().to(device)

    for epoch in trange(n_epochs, ncols=80, desc="Training model"):
        loss = train(
            model,
            optimizer,
            train_loader,
            epoch,
            class_weights=class_weights,
        )
        total_loss.append(loss)
        valid_loss = test(
            model,
            valid_loader,
            epoch,
            class_weights=class_weights,
        )
        total_valid_loss.append(valid_loss)

        print("\nEpoch: {:02d}, Training Loss:   {:.4f}".format(epoch, loss))
        print("           Validation Loss: {:.4f}".format(valid_loss))

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            modpath = osp.join("deepsets_best.pth")
            print("New best model saved to:", modpath)
            torch.save(model.state_dict(), modpath)
            stale_epochs = 0
        else:
            print("Stale epoch")
            stale_epochs += 1
        if stale_epochs >= patience:
            print("Early stopping after %i stale epochs" % patience)
            break

    model.eval()
    y_test = []
    y_predict = []
    for batch in tqdm(
        test_loader,
        total=len(test_loader),
        ncols=80,
        desc="Evaluating model",
    ):
        X = batch[1].to(device)
        y = batch[2].to(device)
        batch_output = model(X)
        pred_probab = nn.Softmax(dim=1)(batch_output)
        y_predict.append(pred_probab.detach().cpu().numpy())
        y_test.append(y.cpu().numpy())
    y_test = np.concatenate(y_test)
    y_predict = np.concatenate(y_predict)

    plot_metrics(
        {"train_loss": total_loss, "valid_loss": total_valid_loss},
        plot_path,
    )

    plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        plot_path=plot_path,
    )

    plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        bins=np.linspace(-5, 5, 50),
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )


def train(
    model,
    optimizer,
    loader,
    epoch,
    class_weights=None,
):
    model.train()

    criterion = nn.CrossEntropyLoss(reduction="mean", weight=class_weights)

    sum_loss = 0.0
    for batch in tqdm(
        loader,
        total=len(loader),
        ncols=80,
        desc=f"Train Epoch {epoch}",
        leave=False,
    ):
        X = batch[1].to(device)
        y = batch[2].to(device)
        y = torch.argmax(y, dim=1)
        optimizer.zero_grad()
        batch_output = model(X)
        batch_loss = criterion(batch_output, y)
        batch_loss.backward()
        batch_loss_item = batch_loss.item()
        sum_loss += batch_loss_item
        optimizer.step()

    return sum_loss / len(loader)


@torch.no_grad()
def test(
    model,
    loader,
    epoch,
    class_weights=None,
):
    model.eval()

    criterion = nn.CrossEntropyLoss(reduction="mean", weight=class_weights)

    sum_loss = 0.0
    for batch in tqdm(
        loader,
        total=len(loader),
        ncols=80,
        desc=f"Validation Epoch {epoch}",
        leave=False,
    ):
        X = batch[1].to(device)
        y = batch[2].to(device)
        y = torch.argmax(y, dim=1)
        batch_output = model(X)
        batch_loss_item = criterion(batch_output, y).item()
        sum_loss += batch_loss_item

    return sum_loss / len(loader)
