import torch
import json
import numpy as np
import awkward as ak
from tqdm import tqdm
from pathlib import Path
import onnxruntime as ort
from torch.utils.data import DataLoader
from clahh.models.deepsets_v3 import DeepSets
import clahh.utils.data_handler as dh
import clahh.plotting.metrics as pltmetrics
from clahh.utils.metrics import dump_recommended_working_points
import clahh.plotting.benchmarks as pltbench
from clahh.experiments.hhvsbkg_deep_sets_weighted_batches import get_config

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def run(
    model_path,
    test_data_path: Path,
    file_paths=None,
    luminosity=126.1,  # fb^-1 (Run 2)
    cutflow_path=None,
    export=False,
    sample_campaign=None,
):
    print("Using device:", device)

    config = get_config()

    batch_size = config["batch_size"]
    max_jets = config["num_jets"]
    jet_feature_names = config["jet_feature_names"]
    bb_feature_names = config["bb_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    weight_name = config["weight_name"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    spectator_names = spectator_jet_names + spectator_event_names

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    cutflow = None
    # load test dataset indices if any
    if test_data_path and test_data_path.exists():
        if file_paths:
            if test_data_path.suffix != ".npy":
                raise ValueError(
                    "Test dataset should be in .npy format containing list of event numbers."
                )
            print("Loading datasets...")
            hh4b_dataset = dh.load_data_from_parquet(file_paths, return_cutflow=False)
            test_event_number = np.load(test_data_path)
            test_event_number_idx = dh.get_event_indices(
                hh4b_dataset["event_number"].to_numpy(), test_event_number
            )
            test_dataset = hh4b_dataset[test_event_number_idx]
        else:
            test_dataset = ak.from_parquet(test_data_path)
        print(f"Test dataset size:", len(test_dataset))
    elif file_paths:
        print("Loading datasets...")
        hh4b_dataset, cutflow = dh.load_data_from_parquet(file_paths, return_cutflow=True)
        test_dataset = hh4b_dataset
        print(f"Test dataset size:", len(test_dataset))
    else:
        raise ValueError("Test dataset not provided.")

    # load cutflow if provided
    sample_fraction = 1.0
    luminosity_normalized = luminosity
    cutflow_normalized = None
    if cutflow_path and cutflow_path.exists():
        cutflow = json.load(open(cutflow_path))

    if cutflow:
        sample_fraction = len(test_dataset) / sum(
            [cutflow[target_class]["bjets_tagger_GN2v01_77_count_4"] for target_class in cutflow]
        )
        luminosity_normalized = luminosity * sample_fraction
        cutflow_normalized = {
            target_class: {
                key: value * sample_fraction for key, value in cutflow[target_class].items()
            }
            for target_class in cutflow
        }
        test_benchmarks = pltbench.create_yields_benchmarks(
            test_dataset, cutflow=cutflow_normalized
        )
        print(
            f"Test benchmarks:\n  total signal counts (scaled by test_fract = {sample_fraction}): {test_benchmarks['HH_SR']}\n total eff: {test_benchmarks['total_eff']}"
        )

    # pltmetrics.plot_vars(
    #     x=test_dataset[spectator_names],
    #     y=test_dataset[label_names],
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=test_dataset[spectator_jet_names],
    #     y=test_dataset[label_names],
    #     weights=ak.broadcast_arrays(test_dataset["event_weight"], test_dataset["jet_pt"])[0],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=test_dataset[spectator_event_names],
    #     y=test_dataset[label_names],
    #     weights=test_dataset["event_weight"],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    print("Preparing data")
    test_tensordataset = dh.to_tensordataset(
        test_dataset,
        jet_feature_names=jet_feature_names,
        bb_feature_names=bb_feature_names,
        event_feature_names=event_feature_names,
        label_names=label_names,
        event_weight_name=weight_name,
        max_njets=max_jets,
        pad_value=config["padding_value"],
    )
    test_loader = DataLoader(
        test_tensordataset, batch_size=batch_size, shuffle=False, drop_last=False
    )
    pltmetrics.visualize_class_count(
        test_loader, label_names=label_names, plot_name=plot_path / "class_counts_test.png"
    )

    #############################
    # Evaluate on testing data
    #############################
    print("Loading model...")
    # create model
    model = DeepSets(
        input_jet_size=test_tensordataset[0]["x_jet"].size(0),
        input_bb_size=test_tensordataset[0]["x_bb"].size(0),
        input_event_size=test_tensordataset[0]["x_event"].size(0),
        outputs=test_tensordataset[0]["y"].size(0),
        njets=max_jets,
        dropout=config["dropout"],
    )
    print(model)
    # load model
    model.load_state_dict(
        torch.load(
            model_path,
            map_location=torch.device(device),
        )
    )
    # set model to evaluation mode
    model.eval()

    # Evaluate the model
    y_test = []
    y_predict = []
    weights = []
    idxs = []
    for batch in tqdm(
        test_loader,
        total=len(test_loader),
        ncols=80,
        desc="Evaluating model",
    ):
        X_jet, X_bb, X_evnt, y, wgts, idx = (
            batch.x_jet.to(device),
            batch.x_bb.to(device),
            batch.x_event.to(device),
            batch.y.to(device),
            batch.weights.to(device),
            batch.idx.to(device),
        )
        pred = model(X_jet, X_bb, X_evnt)
        pred_prob = torch.nn.Softmax(dim=1)(pred)
        y_predict.append(dh.to_numpy(pred_prob))
        y_test.append(dh.to_numpy(y))
        weights.append(dh.to_numpy(wgts))
        idxs.append(dh.to_numpy(idx))
    y_test = np.concatenate(y_test)
    y_predict = np.concatenate(y_predict)
    weights = np.concatenate(weights)
    idxs = np.concatenate(idxs)

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltmetrics.plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    hists_dict = pltbench.create_fitting_hists(
        y_predict=y_predict,
        y_target=y_test,
        weights=weights,
        label_names=label_names,
        test_dataset=test_dataset[idxs],
        target_signal_eff=test_benchmarks["total_eff"],
        plot_path=plot_path,
        vars={"clahh": "discrim", "nominal": "hh_mass"},
        cutflow=cutflow_normalized,
        return_hists=True,
    )
    pltbench.plot_limit_histograms(
        hists_dict, plot_path, clahh_x_label="Discriminant", nominal_x_label="m(HH) [GeV]"
    )

    # # compute limits using histograms
    # mu_clahh, mu_nominal = pltbench.calculate_upper_limit(hists_dict)
    # mus_dict = {"CLAHH": mu_clahh, "Nominal": mu_nominal}

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_test,
        weights=weights,
        label_names=label_names,
        test_dataset=test_dataset[idxs],
        plot_path=plot_path,
        model_label="CLAHH DeepSets",
        luminosity=round(luminosity_normalized, 1),
        cutflow=cutflow_normalized,
        sample_campaign=sample_campaign,
    )

    if export:
        print("Saving recommended working points...")
        dump_recommended_working_points(
            y_predict=y_predict,
            y_target=y_test,
            label_names=label_names,
            weights=weights,
            total=cutflow_normalized["label_HH"]["initial_events_weighted"],
            save_path="deepsets_working_points.json",
        )
        print("Exporting model to ONNX...")
        # Export model to ONNX
        model_pathname = "clahh_deepsets_model.onnx"
        # Define fixed input tensors
        input_jet = torch.randn(1, *test_tensordataset[0]["x_jet"].size(), requires_grad=True)
        input_bb = torch.randn(1, *test_tensordataset[0]["x_bb"].size(), requires_grad=True)
        input_event = torch.randn(1, *test_tensordataset[0]["x_event"].size(), requires_grad=True)

        torch_out = model(input_jet, input_bb, input_event)

        torch.onnx.export(
            model,
            (input_jet, input_bb, input_event),
            model_pathname,
            export_params=True,
            opset_version=11,
            do_constant_folding=True,
            input_names=["jet", "bb", "event"],
            output_names=["output"],
            dynamic_axes={
                "jet": {0: "batch_size", 2: "njets"},
                "bb": {0: "batch_size", 2: "ncombos"},
                "event": {0: "batch_size"},
                "output": {0: "batch_size"},
            },
        )

        def to_numpy(tensor):
            return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

        # Load the ONNX model
        ort_session = ort.InferenceSession(model_pathname)
        # compute ONNX Runtime output prediction
        ort_inputs = {
            "jet": to_numpy(input_jet),
            "bb": to_numpy(input_bb),
            "event": to_numpy(input_event),
        }
        ort_outs = ort_session.run(None, ort_inputs)
        # compare ONNX Runtime and PyTorch results
        np.testing.assert_allclose(to_numpy(torch_out), ort_outs[0], rtol=1e-03, atol=1e-05)
        print("Exported model has been tested with ONNXRuntime, and the result looks good!")
