import torch
import onnxruntime as ort
import torch_geometric.loader as gloader
import pickle
import numpy as np
import awkward as ak
from pathlib import Path
import clahh.utils.data_handler as dh
from clahh.plotting.metrics import (
    plot_vars,
    plot_probabilities,
    plot_discriminants,
    plot_roc_rej_v_eff,
    visualize_class_count,
)
import clahh.plotting.benchmarks as pltbench
from clahh.utils.metrics import get_op
from clahh.models.gnn import GNN
from clahh.experiments.hhvsbkg_gnn_weighted_batches_copy import (
    get_config,
    get_normed_weights,
    prepare_data,
    normalize_features,
    create_data_list,
    test,
)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def run(model_path, test_data_path=None, scaler_paths=None, export=False):
    print("Using device:", device)

    config = get_config()

    batch_size = config["batch_size"]
    num_jets = config["num_jets"]
    jet_feature_names = config["jet_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    spectator_names = spectator_jet_names + spectator_event_names
    torchgen = torch.Generator().manual_seed(config["rnd_seed"])
    luminosity = 126.1  # fb^-1 (Run 2)

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    # load test dataset indices if any
    if test_data_path and test_data_path.exists():
        test_dataset = ak.from_parquet(test_data_path)
        print(f"Test dataset size:", len(test_dataset))
    else:
        raise ValueError("Test dataset not provided.")

    # for backward compatibility add fields jet_x, jet_y, jet_z by copying jet_px, jet_py, jet_pz
    for old_field in ["jet_x", "jet_y", "jet_z"]:
        if old_field not in test_dataset.fields:
            test_dataset[old_field] = test_dataset[old_field.replace("jet_", "jet_p")]

    test_dataset["event_weight"] = test_dataset["event_weight"] * luminosity
    # Labels one-hot encoded
    test_labels = np.transpose([test_dataset[y] for y in label_names])

    plot_vars(
        x=test_dataset[spectator_names],
        y=test_dataset[label_names],
        plot_path=plot_path,
    )

    plot_vars(
        x=test_dataset[spectator_jet_names],
        y=test_dataset[label_names],
        weights=ak.broadcast_arrays(test_dataset["event_weight"], test_dataset["jet_pt"])[0],
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    plot_vars(
        x=test_dataset[spectator_event_names],
        y=test_dataset[label_names],
        weights=test_dataset["event_weight"],
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    print("Prepare graph data")
    for jet_feature in jet_feature_names:
        test_dataset[jet_feature] = dh.pad(
            test_dataset[jet_feature], pad_with=np.nan, pad_end=num_jets
        )
    node_features, edge_index, graph_features = prepare_data(
        test_dataset,
        node_cols=jet_feature_names,
        graph_cols=event_feature_names,
    )

    if scaler_paths and len(scaler_paths) == 2:
        print("Normalizing features")
        node_scaler_path, graph_scaler_path = scaler_paths
        with open(node_scaler_path, "rb") as f:
            node_scaler = pickle.load(f)
        with open(graph_scaler_path, "rb") as f:
            graph_scaler = pickle.load(f)
        node_features, graph_features = normalize_features(
            node_features, graph_features, node_scaler, graph_scaler
        )

    print("Creating dataset lists")
    test_data = create_data_list(
        node_features,
        graph_features,
        edge_index,
        test_labels,
        test_dataset["event_weight"].to_numpy(),
    )

    print("Creating weighted data loaders")
    test_loader = gloader.DataLoader(test_data, batch_size=batch_size, drop_last=False)
    visualize_class_count(
        test_loader, label_names=label_names, plot_name=plot_path / "class_counts_test.png"
    )

    print("Loading model")
    model = GNN(
        n_node_features=node_features.shape[-1],
        n_graph_features=graph_features.shape[-1],
        n_classes=len(label_names),
        dropout=config["dropout"],
        dropout_gat=config["dropout_gat"],
    ).to(device)
    # load model from file
    model.load_state_dict(
        torch.load(
            model_path,
            map_location=torch.device(device),
        )
    )
    print(model)

    y_predict, y_truth, y_idx, y_wgts = test(model, test_loader, device=device, return_weights=True)

    # Convert to one-hot encoding
    y_truth = np.eye(len(label_names))[y_truth]

    plot_probabilities(
        y_predict=y_predict,
        y_truth=y_truth,
        label_names=label_names,
        weights=y_wgts,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    plot_probabilities(
        y_predict=y_predict,
        y_truth=y_truth,
        label_names=label_names,
        plot_path=plot_path,
    )

    plot_discriminants(
        y_predict=y_predict,
        y_truth=y_truth,
        label_names=label_names,
        main_class="label_HH",
        weights=y_wgts,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    plot_discriminants(
        y_predict=y_predict,
        y_truth=y_truth,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_truth,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_truth,
        weights=y_wgts,
        label_names=label_names,
        test_dataset=test_dataset,
        plot_path=plot_path,
        model_label="CLAHH (GNN)",
    )
