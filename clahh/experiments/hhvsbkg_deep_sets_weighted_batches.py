import torch
import json
import numpy as np
import awkward as ak
import torch.nn as nn
import os.path as osp
from pathlib import Path
from datetime import datetime
from tqdm import tqdm, trange
from clahh.models.deepsets_v3 import DeepSets
from torch.utils.data import DataLoader, WeightedRandomSampler, Subset
import clahh.utils.data_handler as dh
import clahh.plotting.metrics as pltmetrics
from clahh.utils.fitting import train_epoch, test_epoch
import clahh.plotting.benchmarks as pltbench

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def get_normed_weights(subset):
    # To get tensors from the subset:
    sample_labels = subset[:]["y"].numpy()
    sample_weights = subset[:]["weights"].numpy()
    normed_sample_weights = np.zeros(len(sample_weights), dtype=np.float32)
    for i in range(sample_labels.shape[1]):
        class_mask = sample_labels[:, i] == 1
        class_weights = sample_weights[class_mask]
        normed_sample_weights[class_mask] = class_weights / class_weights.sum()
    return normed_sample_weights


def get_config():
    config = {
        "n_epochs": 30,
        "patience": 5,
        "batch_size": 128,
        "num_jets": 20,
        "padding_value": "nan",
        "jet_feature_names": [
            "jet_px",
            "jet_py",
            "jet_pz",
        ],
        "bb_feature_names": [
            "bb_dM",
            "bb_dR",
            "bb_dEta",
        ],
        "event_feature_names": [
            "m_4b",
        ],
        "label_names": ["label_HH", "label_QCD", "label_ttbar"],
        "weight_name": "event_weight",
        "spectator_jet_names": ["jet_pt", "jet_eta", "jet_phi", "jet_mass"],
        "spectator_event_names": ["X_Wt", "dEta_HH", "X_HH"],
        "rnd_seed": 42,
        "dropout": 0.0,
        "train_split": {"event_number": "%10<=7"},  # 80% of data
        "val_split": {"event_number": "%10==8"},  # 10% of data
        "test_split": {"event_number": "%10==9"},  # 10% of data
    }
    return config


def run(file_paths, sample_campaign=None, fast_dev_run=False):
    print("Using device:", device)

    config = get_config()

    n_epochs = config["n_epochs"] if not fast_dev_run else 1
    batch_size = config["batch_size"]
    jet_feature_names = config["jet_feature_names"]
    max_jets = config["num_jets"]
    bb_feature_names = config["bb_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    weight_name = config["weight_name"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    train_split = config["train_split"]
    val_split = config["val_split"]
    test_split = config["test_split"]
    rnd_seed = config["rnd_seed"]
    torchgen = torch.Generator().manual_seed(rnd_seed)
    rndgen = np.random.default_rng(seed=rnd_seed)
    luminosity = 126.1  # Run2 lumi in fb^-1

    spectator_feature_name = "jet_pt"

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    hh4b_dataset, cutflow = dh.load_data_from_parquet(file_paths, return_cutflow=True)

    # save cutflow to json
    with open("cutflow.json", "w") as f:
        json.dump(cutflow, f)

    for label in label_names:
        pltmetrics.plot_vars(
            x=hh4b_dataset[weight_name],
            y=hh4b_dataset[[label]],
            plot_name_postfix=label,
            plot_path=plot_path,
        )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[spectator_event_names],
    #     y=hh4b_dataset[label_names],
    #     weights=hh4b_dataset["event_weight"],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[spectator_jet_names],
    #     y=hh4b_dataset[label_names],
    #     weights=np.reshape(np.repeat(hh4b_dataset["event_weight"], max_jets), (-1, max_jets)),
    #     bins=np.linspace(0, 250, 200),
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[spectator_event_names],
    #     y=hh4b_dataset[label_names],
    #     bins=np.linspace(0, 1000, 100),
    #     weights=hh4b_dataset["event_weight"],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    event_number = hh4b_dataset["event_number"].to_numpy()
    # Split data into train, validation, and test sets
    train_event_number, val_event_number, test_event_number, split_indices = dh.split_data(
        event_number,
        train_split,
        val_split,
        test_split,
        generator=rndgen,
        return_indices=True,
    )
    train_data_indices, valid_data_indices, test_data_indices = split_indices

    n_total_samples = len(hh4b_dataset)
    n_train_samples = len(train_event_number)
    n_valid_samples = len(val_event_number)
    n_test_samples = len(test_event_number)
    print("Number of total samples:", n_total_samples)
    print("Number of training samples:", n_train_samples)
    print("Number of validation samples:", n_valid_samples)
    print("Number of testing samples:", n_test_samples)

    # save indices to avoid re-splitting
    np.save("test_event_number", test_event_number)
    np.save("train_event_number", train_event_number)
    # Save test dataset
    test_dataset = ak.with_parameter(hh4b_dataset[test_data_indices], "cutflow", cutflow)
    ak.to_parquet(test_dataset, "test_dataset.parquet")

    test_sample_fraction = len(test_dataset) / sum(
        [cutflow[target_class]["bjets_tagger_GN2v01_77_count_4"] for target_class in cutflow]
    )
    luminosity_normalized = luminosity * test_sample_fraction
    cutflow_normalized = {
        target_class: {
            key: value * test_sample_fraction for key, value in cutflow[target_class].items()
        }
        for target_class in cutflow
    }
    # Print benchmarks
    nominal_benchmarks = pltbench.create_yields_benchmarks(hh4b_dataset, cutflow=cutflow_normalized)
    print(
        f"Nomianl benchmarks:\n  total signal counts (scaled to {luminosity} fb^-1): {nominal_benchmarks['HH_SR']}\n total eff: {nominal_benchmarks['total_eff']}"
    )

    print("Preparing data")
    # Convert to TensorDataset
    sample_tensordataset = dh.to_tensordataset(
        hh4b_dataset,
        jet_feature_names=jet_feature_names,
        bb_feature_names=bb_feature_names,
        event_feature_names=event_feature_names,
        label_names=label_names,
        event_weight_name=weight_name,
        spectator_feature_name=spectator_feature_name,
        max_njets=max_jets,
    )

    # Create train, validation, and test loaders
    train_tensordataset = Subset(sample_tensordataset, train_data_indices)
    valid_tensordataset = Subset(sample_tensordataset, valid_data_indices)
    test_tensordataset = Subset(sample_tensordataset, test_data_indices)

    train_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(train_tensordataset)),
        num_samples=n_train_samples,
        replacement=True,
        generator=torchgen,
    )
    train_loader = DataLoader(
        train_tensordataset, sampler=train_sampler, batch_size=batch_size, drop_last=True
    )
    pltmetrics.visualize_class_count(
        train_loader, label_names=label_names, plot_name=plot_path / "class_counts_train.png"
    )
    # pltmetrics.visualize_feature(
    #     train_loader,
    #     label_names,
    #     feature_name=spectator_feature_name,
    #     leading=True,
    #     bins=np.linspace(0, 1000, 200),
    #     dataset=train_tensordataset,
    #     density=True,
    #     log_scale=True,
    #     weight_negative=False,
    #     plot_name=plot_path / f"{spectator_feature_name}_leading_resampled_train.png",
    # )
    valid_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(valid_tensordataset)),
        num_samples=n_valid_samples,
        replacement=True,
        generator=torchgen,
    )
    valid_loader = DataLoader(
        valid_tensordataset, sampler=valid_sampler, batch_size=batch_size, drop_last=True
    )
    pltmetrics.visualize_class_count(
        valid_loader, label_names=label_names, plot_name=plot_path / "class_counts_val.png"
    )

    train_labels = train_tensordataset[:]["y"].numpy()
    train_weights = train_tensordataset[:]["weights"].numpy()
    train_class_counts = dh.get_class_counts(train_labels)
    train_class_mc_weights = dh.get_class_counts(train_labels, train_weights)
    print("Class counts and class sum of mc weights in train sample:")
    for i, label in enumerate(label_names):
        print(f"{label}: {train_class_counts[i]} -- {train_class_mc_weights[i]:.4f}")

    #############################
    # Create model
    #############################
    print("Creating model")
    model = DeepSets(
        input_jet_size=train_tensordataset[0]["x_jet"].size(0),
        input_bb_size=train_tensordataset[0]["x_bb"].size(0),
        input_event_size=train_tensordataset[0]["x_event"].size(0),
        outputs=train_tensordataset[0]["y"].size(0),
        njets=max_jets,
        dropout=config["dropout"],
    ).to(device)
    print(model)

    #############################
    # Train
    #############################

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
    stale_epochs = 0
    best_valid_loss = 99999
    patience = config["patience"]
    total_loss = []
    total_valid_loss = []
    total_class_weight = dh.get_class_weights(train_labels) / train_class_mc_weights
    total_class_weight = torch.tensor(total_class_weight, device=device, dtype=torch.float32)
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")

    for epoch in trange(n_epochs, ncols=80, desc="Training model"):
        loss = train_epoch(
            model,
            optimizer,
            train_loader,
            epoch,
            device,
            reduction="none",
            # class_weights=total_class_weight,
        )
        total_loss.append(loss)
        valid_loss, accuracy = test_epoch(
            model,
            valid_loader,
            epoch,
            device,
            reduction="none",
            return_accuracy=True,
            # class_weights=total_class_weight,
        )
        total_valid_loss.append(valid_loss)

        print("\nEpoch: {:02d}, Training Loss:   {:.4f}".format(epoch + 1, loss))
        print("           Validation Loss: {:.4f}".format(valid_loss))
        print("           Validation Accuracy: {:.4f}".format(accuracy))

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            model_path = osp.join(f"deepsets_{timestamp}.pth")
            print("           New best model saved to:", model_path)
            torch.save(model.state_dict(), model_path)
            stale_epochs = 0
        else:
            print("           Stale epoch")
            stale_epochs += 1
        if stale_epochs >= patience:
            print("Early stopping after %i stale epochs" % patience)
            break

    #############################
    # Evaluate
    #############################

    test_loader = DataLoader(
        test_tensordataset, batch_size=batch_size, shuffle=False, drop_last=False
    )

    model.eval()
    y_test = []
    y_predict = []
    weights = []
    for batch in tqdm(
        test_loader,
        total=len(test_loader),
        ncols=80,
        desc="Evaluating model",
    ):
        X_jet, X_bb, X_evnt, y, wgts = (
            batch.x_jet.to(device),
            batch.x_bb.to(device),
            batch.x_event.to(device),
            batch.y.to(device),
            batch.weights.to(device),
        )
        batch_output = model(X_jet, X_bb, X_evnt)
        pred_probab = nn.Softmax(dim=1)(batch_output)
        y_predict.append(pred_probab.detach().cpu().numpy())
        y_test.append(y.cpu().numpy())
        weights.append(wgts.cpu().numpy())
    y_test = np.concatenate(y_test)
    y_predict = np.concatenate(y_predict)
    weights = np.concatenate(weights)

    pltmetrics.plot_metrics(
        {"train_loss": total_loss, "valid_loss": total_valid_loss},
        plot_path,
    )

    pltmetrics.plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_test,
        weights=weights,
        label_names=label_names,
        test_dataset=test_dataset,
        plot_path=plot_path,
        model_label="CLAHH DeepSets",
        luminosity=round(luminosity_normalized, 1),
        cutflow=cutflow_normalized,
        sample_campaign=sample_campaign,
    )
