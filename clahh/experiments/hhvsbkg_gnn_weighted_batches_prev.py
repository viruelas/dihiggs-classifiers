import torch
import torch.nn as nn
from tqdm import tqdm
from torch.utils.data import random_split, WeightedRandomSampler
from torch_geometric.data import Data
from sklearn.preprocessing import StandardScaler
import torch_geometric.loader as gloader
import pickle
import numpy as np
import awkward as ak
import os.path as osp
from datetime import datetime
from pathlib import Path
import clahh.plotting.metrics as pltmetrics
import clahh.utils.data_handler as dh
from clahh.models.gnn import GNN
from scipy.sparse import coo_matrix
import json
import clahh.plotting.benchmarks as pltbench


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# Prepare the data
def prepare_data(
    features,
    node_cols: list = None,
    graph_cols: list = None,
    connect_self_edges: bool = False,
    use_sparse: bool = False,  # TODO: implement sparse edge index
):
    node_features = [features[node_feat].to_numpy() for node_feat in node_cols]
    # Stack node features into a 3D numpy array (num_samples, num_nodes, num_features)
    node_features = np.stack(
        node_features, axis=-1
    )  # Shape: (num_samples, num_nodes, num_features)
    print("node_features.shape", node_features.shape)

    # Generate fully connected edge index
    num_nodes = node_features.shape[1]

    # Generate fully connected edge index
    row = np.repeat(np.arange(num_nodes), num_nodes)
    col = np.tile(np.arange(num_nodes), num_nodes)

    if not connect_self_edges:
        mask = row != col
        row = row[mask]
        col = col[mask]

    edge_index = np.vstack((row, col)).astype(np.int64)  # Shape: (2, num_edges)
    print(f"edge_index.shape: {edge_index.shape}")

    graph_features = [features[graph_col].to_numpy() for graph_col in graph_cols]
    # Stack graph-level features into a 2D numpy array (num_samples, num_graph_features)
    graph_features = np.concatenate(
        graph_features, axis=-1
    )  # Shape: (num_samples, num_graph_features)
    print("graph_features.shape", graph_features.shape)

    return node_features, edge_index, graph_features


# Normalize features
def normalize_features(node_features, graph_features, node_scaler, graph_scaler, is_train=False):
    node_shape = node_features.shape
    node_features = node_features.reshape(node_features.shape[0], -1)
    if is_train:
        node_features = node_scaler.fit_transform(node_features)
        graph_features = graph_scaler.fit_transform(graph_features)
    else:
        node_features = node_scaler.transform(node_features)
        graph_features = graph_scaler.transform(graph_features)
    node_features = node_features.reshape(node_shape)
    return node_features, graph_features


# Create a list of Data objects
def create_data_list(
    node_features, graph_features, edge_index, labels, weights, spectator_feature=None
):
    num_samples = len(labels)

    # Convert node features, graph features, and labels to tensors
    node_features = torch.from_numpy(node_features).float()
    graph_features = torch.from_numpy(graph_features).float()
    edge_index = torch.from_numpy(edge_index).long()
    # Convert one-hot encoded labels to integers
    labels = torch.from_numpy(np.argmax(labels, axis=1)).long()

    # Create masks for finite values
    node_mask = torch.isfinite(node_features)
    graph_mask = torch.isfinite(graph_features)
    if spectator_feature is not None:
        spectator_feature = torch.from_numpy(spectator_feature)
        # add another dimension after the second dimension to prevent flattening during batch processing
        spectator_feature = spectator_feature.unsqueeze(1)

    # Create Data objects
    data_list = [
        Data(
            x=node_features[i],
            edge_index=edge_index,
            y=labels[i],
            graph_features=graph_features[i],
            node_mask=node_mask[i],
            graph_mask=graph_mask[i],
            weights=weights[i],
            data_idx=torch.tensor(i, dtype=torch.long),
            spectator=spectator_feature[i] if spectator_feature is not None else None,
        )
        for i in range(num_samples)
    ]

    return data_list


def get_normed_weights(sample_weights, sample_labels):
    normed_sample_weights = np.zeros(len(sample_weights), dtype=np.float32)
    for i in range(sample_labels.shape[1]):
        class_mask = sample_labels[:, i] == 1
        class_weights = sample_weights[class_mask]
        normed_sample_weights[class_mask] = class_weights / class_weights.sum()
    return normed_sample_weights


# Training function with a learning rate scheduler
def train(model, train_loader, val_loader, epochs=30, lr=0.001, device=device, patience=5):
    # optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)
    optimizer = torch.optim.SGD(model.parameters(), lr=lr, weight_decay=0.005, momentum=0.9)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, epochs)
    loss_fn = nn.NLLLoss()

    stale_epochs = 0
    best_val_loss = 999_999
    total_train_loss = []
    total_val_loss = []
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    for epoch in range(epochs):
        model.train()
        train_loss = 0
        for data in tqdm(
            train_loader,
            total=len(train_loader),
            ncols=80,
            desc=f"Train Epoch {epoch}",
            leave=False,
        ):
            # Compute prediction and loss
            data = data.to(device)
            log_probs = model(data)
            loss = loss_fn(log_probs, data.y)
            train_loss += loss.item()

            # Backpropagation
            loss.backward()
            optimizer.step()
            scheduler.step()
            optimizer.zero_grad()
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, epochs)
        total_train_loss.append(train_loss / len(train_loader.dataset))

        correct = 0
        val_loss = 0
        model.eval()
        # Disable gradient computation and reduce memory consumption
        with torch.no_grad():
            for data in val_loader:
                data = data.to(device)
                log_probs = model(data)
                loss = loss_fn(log_probs, data.y)
                val_loss += loss.item()
                probs = torch.exp(log_probs)
                pred_class = probs.argmax(dim=1)
                correct += (pred_class == data.y).sum().item()
        total_val_loss.append(val_loss / len(val_loader.dataset))
        val_acc = correct / len(val_loader.dataset)
        print(f"Epoch {epoch+1}, Loss: {train_loss:.4f}, Validation Accuracy: {val_acc:.4f}")
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            model_path = f"gnn_{timestamp}.pth"
            torch.save(model.state_dict(), model_path)
            print("New best model saved to:", model_path)
            stale_epochs = 0
        else:
            print("Stale epoch")
            stale_epochs += 1
        if stale_epochs >= patience:
            print("Early stopping after %i stale epochs" % patience)
            break

    return total_train_loss, total_val_loss


# Testing function
@torch.no_grad()
def test(model, test_loader, device=device, return_weights=False):
    model.eval()

    y_truth = []
    y_weights = []
    y_pred = []
    data_idx = []
    correct = 0
    for data in tqdm(
        test_loader,
        total=len(test_loader),
        ncols=80,
        desc="Evaluating model",
    ):
        data = data.to(device)
        log_probs = model(data)
        probs = torch.exp(log_probs)
        y_pred.append(dh.to_numpy(probs))
        y_truth.append(dh.to_numpy(data.y))
        data_idx.append(dh.to_numpy(data.data_idx))
        y_weights.append(dh.to_numpy(data.weights))
        # Get the class with the highest probability
        pred = probs.argmax(dim=1)
        correct += (pred == data.y).sum().item()

    test_acc = correct / len(test_loader.dataset)
    print(f"Test Accuracy: {test_acc:.4f}")

    y_pred = np.concatenate(y_pred)
    y_truth = np.concatenate(y_truth)
    y_weights = np.concatenate(y_weights)
    data_idx = np.concatenate(data_idx)

    if return_weights:
        return y_pred, y_truth, data_idx, y_weights
    else:
        return y_pred, y_truth, data_idx


def get_config():
    config = {
        "epochs": 30,
        "patience": 5,
        "batch_size": 128,
        "num_jets": 13,
        "jet_feature_names": [
            "jet_px",
            "jet_py",
            "jet_pz",
        ],
        "bb_feature_names": [
            "bb_dM",
            "bb_dR",
            "bb_dEta",
        ],
        "event_feature_names": [
            "m_4b",
        ],
        "label_names": ["label_HH", "label_QCD", "label_ttbar"],
        "weight_names": ["event_weight"],
        "spectator_jet_names": ["jet_pt", "jet_eta", "jet_phi", "jet_mass"],
        "spectator_event_names": ["X_Wt", "deltaEta_hh", "X_hh"],
        "rnd_seed": 42,
        "test_frac": 0.20,
        "valid_frac": 0.20,
        "learning_rate": 0.1,
        "dropout": 0.0,
        "dropout_gat": 0.0,
        "train_split": {"event_number": "%10<=7"},  # 80% of data
        "val_split": {"event_number": "%10==8"},  # 10% of data
        "test_split": {"event_number": "%10==9"},  # 10% of data
    }
    return config


def run(
    file_paths,
    sample_campaign=None,
    fast_dev_run=False,
):
    print("Using device:", device)

    config = get_config()

    epochs = config["epochs"]
    batch_size = config["batch_size"]
    num_jets = config["num_jets"]
    jet_feature_names = config["jet_feature_names"]
    bb_feature_names = config["bb_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    weight_names = config["weight_names"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    spectator_names = spectator_jet_names + spectator_event_names
    train_split = config["train_split"]
    val_split = config["val_split"]
    test_split = config["test_split"]
    rnd_seed = config["rnd_seed"]
    torchgen = torch.Generator().manual_seed(rnd_seed)
    rndgen = np.random.default_rng(seed=rnd_seed)
    luminosity = 126.1  # Run2 lumi in fb^-1

    spectator_feature_name = "jet_pt"

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    hh4b_dataset, cutflow = dh.load_data_from_parquet(file_paths, return_cutflow=True)

    # save cutflow to json
    with open("cutflow.json", "w") as f:
        json.dump(cutflow, f)

    for label in label_names:
        pltmetrics.plot_vars(
            x=hh4b_dataset[weight_names],
            y=hh4b_dataset[[label]],
            plot_name_postfix=label,
            plot_path=plot_path,
        )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[spectator_names],
    #     y=hh4b_dataset[label_names],
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[["jet_pt", "jet_mass"]],
    #     y=hh4b_dataset[label_names],
    #     weights=np.reshape(np.repeat(hh4b_dataset["event_weight"], num_jets), (-1, num_jets)),
    #     bins=np.linspace(0, 250, 200),
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # pltmetrics.plot_vars(
    #     x=hh4b_dataset[["jet_eta", "jet_phi"]],
    #     y=hh4b_dataset[label_names],
    #     weights=np.reshape(np.repeat(hh4b_dataset["event_weight"], num_jets), (-1, num_jets)),
    #     bins=100,
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # Labels one-hot encoded
    labels = np.transpose([hh4b_dataset[y].to_numpy() for y in label_names])
    # All weights
    weights = hh4b_dataset["event_weight"].to_numpy()

    event_number = hh4b_dataset["event_number"].to_numpy()
    # Split data into train, validation, and test sets
    train_event_number, val_event_number, test_event_number, split_indices = dh.split_data(
        event_number,
        train_split,
        val_split,
        test_split,
        generator=rndgen,
        return_indices=True,
    )
    train_data_indices, valid_data_indices, test_data_indices = split_indices

    n_total_samples = len(hh4b_dataset)
    n_train_samples = len(train_event_number)
    n_valid_samples = len(val_event_number)
    n_test_samples = len(test_event_number)
    print("Number of total samples:", n_total_samples)
    print("Number of training samples:", n_train_samples)
    print("Number of validation samples:", n_valid_samples)
    print("Number of testing samples:", n_test_samples)

    # save indices to avoid re-splitting
    np.save("test_event_number", test_event_number)
    np.save("train_event_number", train_event_number)
    # Save test dataset
    test_dataset = ak.with_parameter(hh4b_dataset[test_data_indices], "cutflow", cutflow)
    ak.to_parquet(test_dataset, "test_dataset.parquet")

    test_sample_fraction = len(test_dataset) / sum(
        [cutflow[target_class]["bjets_tagger_GN2v01_77_count_4"] for target_class in cutflow]
    )
    luminosity_normalized = luminosity * test_sample_fraction
    cutflow_normalized = {
        target_class: {
            key: value * test_sample_fraction for key, value in cutflow[target_class].items()
        }
        for target_class in cutflow
    }
    # Print benchmarks
    nominal_benchmarks = pltbench.create_yields_benchmarks(hh4b_dataset, cutflow=cutflow_normalized)
    print(
        f"Nomianl benchmarks:\n  total signal counts (scaled to {luminosity} fb^-1): {nominal_benchmarks['HH_SR']}\n total eff: {nominal_benchmarks['total_eff']}"
    )

    print("Prepare graph data")
    for jet_feature in jet_feature_names + spectator_jet_names:
        hh4b_dataset[jet_feature] = dh.pad(
            hh4b_dataset[jet_feature], pad_with=np.nan, pad_end=num_jets
        )
    node_features, edge_index, graph_features = prepare_data(
        hh4b_dataset,
        node_cols=jet_feature_names,
        # graph_cols=event_feature_names,
        graph_cols=bb_feature_names,
    )

    print("Normalizing features")
    node_scaler = StandardScaler()
    graph_scaler = StandardScaler()
    train_node_features, train_graph_features = normalize_features(
        node_features[train_data_indices],
        graph_features[train_data_indices],
        node_scaler,
        graph_scaler,
        is_train=True,
    )
    # save node_scaler
    with open("node_scaler.pkl", "wb") as f:
        pickle.dump(node_scaler, f)
    # save graph_scaler
    with open("graph_scaler.pkl", "wb") as f:
        pickle.dump(graph_scaler, f)

    val_node_features, val_graph_features = normalize_features(
        node_features[valid_data_indices],
        graph_features[valid_data_indices],
        node_scaler,
        graph_scaler,
    )
    test_node_features, test_graph_features = normalize_features(
        node_features[test_data_indices],
        graph_features[test_data_indices],
        node_scaler,
        graph_scaler,
    )

    print("Creating dataset lists")
    train_data = create_data_list(
        train_node_features,
        train_graph_features,
        edge_index,
        labels[train_data_indices],
        weights[train_data_indices],
        spectator_feature=hh4b_dataset[train_data_indices][spectator_feature_name].to_numpy(),
    )
    val_data = create_data_list(
        val_node_features,
        val_graph_features,
        edge_index,
        labels[valid_data_indices],
        weights[valid_data_indices],
    )
    test_data = create_data_list(
        test_node_features,
        test_graph_features,
        edge_index,
        labels[test_data_indices],
        weights[test_data_indices],
    )

    print("Creating weighted data loaders")
    train_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(weights[train_data_indices], labels[train_data_indices])),
        num_samples=n_train_samples,
        replacement=True,
        generator=torchgen,
    )
    train_loader = gloader.DataLoader(
        train_data, sampler=train_sampler, batch_size=batch_size, drop_last=True
    )
    pltmetrics.visualize_class_count(
        train_loader, label_names=label_names, plot_name=plot_path / "class_counts_train.png"
    )
    pltmetrics.visualize_feature(
        train_loader,
        label_names,
        feature_name=spectator_feature_name,
        leading=True,
        bins=np.linspace(0, 1000, 200),
        dataset=hh4b_dataset[train_data_indices],
        density=True,
        log_scale=True,
        weight_negative=False,
        plot_name=plot_path / f"{spectator_feature_name}_leading_resampled_train.png",
    )
    val_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(weights[valid_data_indices], labels[valid_data_indices])),
        num_samples=n_valid_samples,
        replacement=True,
        generator=torchgen,
    )
    val_loader = gloader.DataLoader(
        val_data, sampler=val_sampler, batch_size=batch_size, drop_last=True
    )
    pltmetrics.visualize_class_count(
        val_loader, label_names=label_names, plot_name=plot_path / "class_counts_val.png"
    )
    test_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(weights[test_data_indices], labels[test_data_indices])),
        num_samples=n_test_samples,
        replacement=True,
        generator=torchgen,
    )
    test_loader = gloader.DataLoader(
        test_data, sampler=test_sampler, batch_size=batch_size, drop_last=True
    )
    pltmetrics.visualize_class_count(
        test_loader, label_names=label_names, plot_name=plot_path / "class_counts_test.png"
    )

    print("Creating model")
    model = GNN(
        n_node_features=node_features.shape[-1],
        n_graph_features=graph_features.shape[-1],
        n_classes=len(label_names),
        dropout=config["dropout"],
        dropout_gat=config["dropout_gat"],
    ).to(device)
    print(model)

    print("Training model")
    train_loss, val_loss = train(
        model,
        train_loader,
        val_loader,
        epochs=epochs,
        lr=config["learning_rate"],
        patience=config["patience"],
    )

    pltmetrics.plot_metrics(
        {"train_loss": train_loss, "valid_loss": val_loss},
        plot_path,
    )

    print("Testing model")
    y_predict, y_test, y_idx = test(model, test_loader)
    # Convert to one-hot encoding
    y_test = np.eye(len(label_names))[y_test]

    pltmetrics.plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_test,
        weights=weights,
        label_names=label_names,
        test_dataset=test_dataset,
        plot_path=plot_path,
        model_label="CLAHH GNN",
        luminosity=round(luminosity_normalized, 1),
        cutflow=cutflow_normalized,
        sample_campaign=sample_campaign,
    )
