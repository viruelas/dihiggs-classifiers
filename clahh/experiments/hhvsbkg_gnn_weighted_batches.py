import torch
import torch.nn as nn
from torch.utils.data import WeightedRandomSampler
from torch_geometric.data import Data, InMemoryDataset
from sklearn.preprocessing import StandardScaler
import torch_geometric.loader as gloader
import pickle
import numpy as np
import awkward as ak
import os.path as osp
from datetime import datetime
from pathlib import Path
import clahh.plotting.metrics as pltmetrics
import clahh.utils.data_handler as dh
from clahh.models.gnn_v2 import GNNClassifier
import json
import clahh.plotting.benchmarks as pltbench

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class StandardScalerTransform:
    def __init__(self):
        self.scaler_x = StandardScaler()
        self.scaler_bb_x = StandardScaler()
        self.scaler_global_x = StandardScaler()

    def fit(self, x, bb_x, global_x):
        self.scaler_x.fit(x)
        self.scaler_bb_x.fit(bb_x)
        self.scaler_global_x.fit(global_x)

    def __call__(self, data):
        data.x = torch.tensor(self.scaler_x.transform(data.x), dtype=torch.float)
        data.bb_x = torch.tensor(self.scaler_bb_x.transform(data.bb_x), dtype=torch.float)
        data.global_x = torch.tensor(
            self.scaler_global_x.transform(data.global_x), dtype=torch.float
        )
        return data

    def save(self, path):
        with open(path, "wb") as f:
            pickle.dump(self, f)


# -----------------------------------------------------------------------------
# Dataset definition: converting events to graph objects with physics-motivated edges
# -----------------------------------------------------------------------------
class HEPEventDataset(InMemoryDataset):
    def __init__(
        self,
        jets,
        bb_features,
        event_features,
        labels,
        weights,
        spectators=None,
        transform=None,
    ):
        super(HEPEventDataset, self).__init__(".", transform)

        self.jets = jets
        self.bb_features = bb_features
        self.event_features = event_features
        self.labels = labels
        self.weights = weights
        self.spectators = spectators
        self.data, self.slices = self.process_data()

    def process_data(self):
        print("Processing data...")
        data_list = []
        N = self.jets.shape[0]
        for i in range(N):
            jet_feat = self.jets[i]

            mask = ~np.isnan(jet_feat[:, 0])

            valid_jets = jet_feat[mask]

            if valid_jets.shape[0] == 0:
                continue  # skip event if no valid jets
            # Node features for valid jets.
            num_nodes = valid_jets.shape[0]
            # Use the physics-motivated edge construction.

            edge_index_np = construct_edges(valid_jets, dr_threshold=0.4)
            if edge_index_np.shape[1] == 0:
                edge_index_np = np.vstack([np.arange(num_nodes), np.arange(num_nodes)])
            edge_index = torch.tensor(edge_index_np, dtype=torch.long)

            x = torch.tensor(jet_feat, dtype=torch.float)

            # Global features: flatten bb_features (which has shape (3,6)) and concatenate with event feature.
            # unsqueeze so that shape becomes (1, global_dim)
            bb_x = torch.tensor(self.bb_features[i], dtype=torch.float)
            evt_feat = torch.tensor(self.event_features[i], dtype=torch.float)
            global_x = torch.cat([evt_feat]).unsqueeze(0)

            # Convert one-hot label to class index.
            label = torch.tensor(np.argmax(self.labels[i]), dtype=torch.long)

            weight = torch.tensor(self.weights[i], dtype=torch.float)
            spectator = (
                torch.tensor(self.spectators[i], dtype=torch.float)
                if self.spectators is not None
                else None
            )

            # Create a PyTorch Geometric Data object.
            data_list.append(
                Data(
                    x=x,
                    bb_x=bb_x,
                    edge_index=edge_index,
                    global_x=global_x,
                    y=label,
                    weights=weight,
                    spectator=spectator,
                    data_idx=i,
                )
            )
        return self.collate(data_list)


# -----------------------------------------------------------------------------
# Helper: physics-motivated edge construction based on ΔR in η–φ space.
# -----------------------------------------------------------------------------
def construct_edges(valid_jets, dr_threshold=0.4):
    """
    valid_jets: numpy array of shape (N_valid, 3) with columns [jet_px, jet_py, jet_pz].
    Returns an edge_index array of shape (2, num_edges) connecting jets if ΔR < dr_threshold.
    """
    px = valid_jets[:, 0]
    py = valid_jets[:, 1]
    pz = valid_jets[:, 2]

    # Compute transverse momentum and azimuthal angle φ.
    phi = np.arctan2(py, px)

    # Compute total momentum (avoid division by zero).
    p = np.sqrt(px**2 + py**2 + pz**2)
    p = np.maximum(p, 1e-6)

    # Compute polar angle and pseudorapidity η.
    theta = np.arccos(np.clip(pz / p, -1.0, 1.0))
    eta = -np.log(np.tan(theta / 2))

    edges_i = []
    edges_j = []
    N = len(eta)
    # Loop over pairs of jets.
    for i in range(N):
        for j in range(N):
            if i == j:
                continue  # skip self-connection
            deta = eta[i] - eta[j]
            # Compute delta_phi with periodicity in mind.
            dphi = np.abs(phi[i] - phi[j])
            if dphi > np.pi:
                dphi = 2 * np.pi - dphi
            dr = np.sqrt(deta**2 + dphi**2)
            if dr < dr_threshold:
                edges_i.append(i)
                edges_j.append(j)
    if len(edges_i) == 0:  # if no pair passes the threshold, return minimal self-loop edges.
        return np.empty((2, 0), dtype=np.int64)
    return np.vstack([edges_i, edges_j])


# -----------------------------------------------------------------------------
# Helper: get normalized weights for each class
# -----------------------------------------------------------------------------
def get_normed_weights(sample_weights, sample_labels):
    normed_sample_weights = np.zeros(len(sample_weights), dtype=np.float32)
    for i in range(sample_labels.shape[1]):
        class_mask = sample_labels[:, i] == 1
        class_weights = sample_weights[class_mask]
        normed_sample_weights[class_mask] = class_weights / class_weights.sum()
    return normed_sample_weights


# -----------------------------------------------------------------------------
# Training loop
# -----------------------------------------------------------------------------
def train(
    model,
    train_loader,
    val_loader,
    num_epochs=30,
    lr=0.001,
    patience=5,
    initial_dropout_gat=0.3,
    final_dropout_gat=0.1,
    initial_dropout_transformer=0.1,
    final_dropout_transformer=0.05,
    reduction="none",
    device=device,
    model_id="gnn",
):

    stale_epochs = 0
    best_val_loss = 999_999
    total_train_loss = []
    total_val_loss = []

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    criterion = nn.CrossEntropyLoss(reduction=reduction)

    for epoch in range(num_epochs):
        # Calculate adaptive dropout values by linearly decaying over epochs.
        dropout_gat = initial_dropout_gat - (initial_dropout_gat - final_dropout_gat) * (
            epoch / num_epochs
        )
        dropout_transformer = initial_dropout_transformer - (
            initial_dropout_transformer - final_dropout_transformer
        ) * (epoch / num_epochs)

        # Update the model's dropout rates
        model.set_dropout_rates(dropout_gat, dropout_transformer)

        model.train()
        train_loss = 0
        print(f"\nEpoch {epoch+1}/{num_epochs}")
        for i, data in enumerate(train_loader):
            data = data.to(device)
            if i == 0:
                print("Training batch info:")
                print("  data.x shape:", data.x.shape)
                print("  data.edge_index shape:", data.edge_index.shape)
                print("  data.global_x shape:", data.global_x.shape)
                print("  data.y shape:", data.y.shape)
            optimizer.zero_grad()
            out = model(data)
            loss = criterion(out, data.y)
            if reduction == "none":
                # convert w to tensor which is -1 if w is negative, 1 if w is positive,
                # and 0 if w is zero. this is because the batches are already weighted
                # with abs(w) in the dataloader
                wgts_sign = torch.sign(data.weights)
                loss = torch.mean(loss * wgts_sign)
            loss.backward()
            optimizer.step()
            train_loss += loss.item() * data.num_graphs
        avg_train_loss = train_loss / len(train_loader.dataset)
        total_train_loss.append(avg_train_loss)

        # Validation loop
        model.eval()
        val_loss = 0
        correct = 0
        with torch.no_grad():
            for data in val_loader:
                data = data.to(device)
                out = model(data)
                loss = criterion(out, data.y)
                if reduction == "none":
                    wgts_sign = torch.sign(data.weights)
                    loss = torch.mean(loss * wgts_sign)
                val_loss += loss.item() * data.num_graphs
                pred = out.argmax(dim=1)
                correct += (pred == data.y).sum().item()
        avg_val_loss = val_loss / len(val_loader.dataset)
        total_val_loss.append(avg_val_loss)
        val_accuracy = correct / len(val_loader.dataset)

        print(
            f"Epoch {epoch+1:02d}: Train Loss: {avg_train_loss:.4f}, Val Loss: {avg_val_loss:.4f}, Val Acc: {val_accuracy:.4f}"
        )

        ########################################
        # Early stopping
        ########################################
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            model_path = f"{model_id}.pth"
            torch.save(model.state_dict(), model_path)
            print("New best model saved to:", model_path)
            stale_epochs = 0
        else:
            print("Stale epoch")
            stale_epochs += 1
        if stale_epochs >= patience:
            print("Early stopping after %i stale epochs" % patience)
            break

    return total_train_loss, total_val_loss


# -----------------------------------------------------------------------------
# Evaluation loop
# -----------------------------------------------------------------------------
def eval(
    model,
    test_loader,
    device=device,
):
    model.eval()

    y_truth = []
    y_pred = []
    y_weights = []
    data_idx = []
    correct = 0
    for data in test_loader:
        data = data.to(device)
        out = model(data)
        pred = out.softmax(dim=1)
        y_pred.append(dh.to_numpy(pred))
        y_truth.append(dh.to_numpy(data.y))
        y_weights.append(dh.to_numpy(data.weights))
        data_idx.append(dh.to_numpy(data.data_idx))
        correct += (pred.argmax(dim=1) == data.y).sum().item()
    test_acc = correct / len(test_loader.dataset)

    test_acc = correct / len(test_loader.dataset)
    print(f"Test Acc: {test_acc:.4f}")

    y_pred = np.concatenate(y_pred)
    y_truth = np.concatenate(y_truth)
    y_weights = np.concatenate(y_weights)
    data_idx = np.concatenate(data_idx)

    return y_pred, y_truth, y_weights, data_idx


# -----------------------------------------------------------------------------
# Configuration
# -----------------------------------------------------------------------------
def get_config():
    config = {
        "epochs": 30,
        "patience": 5,
        "batch_size": 128,
        "num_jets": 13,
        "jet_feature_names": [
            "jet_px",
            "jet_py",
            "jet_pz",
        ],
        "bb_feature_names": [
            "bb_dM",
            "bb_dR",
            "bb_dEta",
        ],
        "event_feature_names": [
            "m_4b",
        ],
        "label_names": ["label_HH", "label_QCD", "label_ttbar"],
        "weight_names": ["event_weight"],
        "spectator_jet_names": ["jet_pt", "jet_eta", "jet_phi", "jet_mass"],
        "spectator_event_names": ["X_Wt", "deltaEta_hh", "X_hh"],
        "rnd_seed": 42,
        "test_frac": 0.20,
        "valid_frac": 0.20,
        "learning_rate": 1e-3,
        "hidden_dim": 64,
        "initial_dropout_gat": 0.3,
        "final_dropout_gat": 0,
        "initial_dropout_transformer": 0.2,
        "final_dropout_transformer": 0,
        "train_split": {"event_number": "%10<=7"},  # 80% of data
        "val_split": {"event_number": "%10==8"},  # 10% of data
        "test_split": {"event_number": "%10==9"},  # 10% of data
    }
    return config


# -----------------------------------------------------------------------------
# Main function
# -----------------------------------------------------------------------------
def run(
    file_paths,
    sample_campaign=None,
    fast_dev_run=False,
):
    print("Using device:", device)

    config = get_config()

    model_id = f"gnn_{datetime.now().strftime('%Y%m%d_%H%M%S')}"

    epochs = config["epochs"] if not fast_dev_run else 3
    batch_size = config["batch_size"]
    num_jets = config["num_jets"]
    jet_feature_names = config["jet_feature_names"]
    bb_feature_names = config["bb_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    weight_names = config["weight_names"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    spectator_names = spectator_jet_names + spectator_event_names
    train_split = config["train_split"]
    val_split = config["val_split"]
    test_split = config["test_split"]
    rnd_seed = config["rnd_seed"]
    torchgen = torch.Generator().manual_seed(rnd_seed)
    rndgen = np.random.default_rng(seed=rnd_seed)
    luminosity = 126.1  # Run2 lumi in fb^-1

    node_in_dim = len(jet_feature_names)
    bb_in_dim = len(bb_feature_names)
    global_in_dim = len(event_feature_names)
    out_dim = len(label_names)

    spectator_feature_name = "jet_pt"

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    hh4b_dataset, cutflow = dh.load_data_from_parquet(file_paths, return_cutflow=True)

    # save cutflow to json
    with open("cutflow.json", "w") as f:
        json.dump(cutflow, f)

    for label in label_names:
        pltmetrics.plot_vars(
            x=hh4b_dataset[weight_names],
            y=hh4b_dataset[[label]],
            plot_name_postfix=label,
            plot_path=plot_path,
        )

    # -----------------------------------------------------------------------------
    # Split the dataset into training and validation sets
    # -----------------------------------------------------------------------------
    event_number = hh4b_dataset["event_number"].to_numpy()
    train_event_number, val_event_number, test_event_number, split_indices = dh.split_data(
        event_number,
        train_split,
        val_split,
        test_split,
        generator=rndgen,
        return_indices=True,
    )
    train_data_indices, valid_data_indices, test_data_indices = split_indices
    # create a mask for the split
    train_data_mask = np.isin(event_number, train_event_number)
    valid_data_mask = np.isin(event_number, val_event_number)
    test_data_mask = np.isin(event_number, test_event_number)

    n_total_samples = len(hh4b_dataset)
    n_train_samples = len(train_event_number)
    n_valid_samples = len(val_event_number)
    n_test_samples = len(test_event_number)
    print("Number of total samples:", n_total_samples)
    print("Number of training samples:", n_train_samples)
    print("Number of validation samples:", n_valid_samples)
    print("Number of testing samples:", n_test_samples)

    # save indices to avoid re-splitting
    np.save("test_event_number", test_event_number)
    np.save("train_event_number", train_event_number)
    # Save test dataset
    test_dataset = ak.with_parameter(hh4b_dataset[test_data_indices], "cutflow", cutflow)
    ak.to_parquet(test_dataset, "test_dataset.parquet")

    test_sample_fraction = len(test_dataset) / sum(
        [cutflow[target_class]["bjets_tagger_GN2v01_77_count_4"] for target_class in cutflow]
    )
    luminosity_normalized = luminosity * test_sample_fraction
    cutflow_normalized = {
        target_class: {
            key: value * test_sample_fraction for key, value in cutflow[target_class].items()
        }
        for target_class in cutflow
    }
    # Print benchmarks
    nominal_benchmarks = pltbench.create_yields_benchmarks(hh4b_dataset, cutflow=cutflow_normalized)
    print(
        f"Nomianl benchmarks:\n  total signal counts (scaled to {luminosity} fb^-1): {nominal_benchmarks['HH_SR']}\n total eff: {nominal_benchmarks['total_eff']}"
    )

    # -----------------------------------------------------------------------------
    # Create the dataset and data loaders
    # -----------------------------------------------------------------------------
    weights = hh4b_dataset["event_weight"].to_numpy()
    # Labels one-hot encoded
    labels = np.transpose([hh4b_dataset[y].to_numpy() for y in label_names])
    # jets are truncated at num_jets
    jet_features = np.transpose(
        [
            ak.to_numpy(
                ak.fill_none(
                    ak.pad_none(hh4b_dataset[jet_feature_name], num_jets, clip=True), float("nan")
                )
            )
            for jet_feature_name in jet_feature_names
        ],
        (1, 2, 0),  # Shape: (num_samples, num_jets, num_jet_features)
    )
    bb_features = np.transpose(
        [hh4b_dataset[bb_feature_name].to_numpy() for bb_feature_name in bb_feature_names],
        (1, 2, 0),  # Shape: (num_samples, combinatorial_pairs, num_bb_features)
    )
    event_features = np.transpose(
        [hh4b_dataset[event_feature_name].to_numpy() for event_feature_name in event_feature_names],
        (1, 2, 0),
    ).squeeze(
        -1
    )  # Shape: (num_samples, num_event_features)
    spectators = np.transpose(
        [
            ak.to_numpy(
                ak.fill_none(
                    ak.pad_none(hh4b_dataset[spectator_name], num_jets, clip=True), float("nan")
                )
            )
            for spectator_name in spectator_jet_names
        ],
        (1, 0, 2),  # Shape: (num_samples, num_jet_features, num_jets)
    )

    scaler = StandardScalerTransform()
    # # load scaler from file
    # with open("gnn_20250306_201411_scaler.pkl", "rb") as f:
    #     scaler = pickle.load(f)

    scaler.fit(
        jet_features[train_data_indices].reshape(-1, node_in_dim),
        bb_features[train_data_indices].reshape(-1, bb_in_dim),
        event_features[train_data_indices],
    )

    train_dataset = HEPEventDataset(
        jet_features[train_data_indices],
        bb_features[train_data_indices],
        event_features[train_data_indices],
        labels[train_data_indices],
        weights[train_data_indices],
        spectators[train_data_indices],
        transform=scaler,
    )
    val_dataset = HEPEventDataset(
        jet_features[valid_data_indices],
        bb_features[valid_data_indices],
        event_features[valid_data_indices],
        labels[valid_data_indices],
        weights[valid_data_indices],
        spectators[valid_data_indices],
        transform=scaler,
    )
    test_dataset = HEPEventDataset(
        jet_features[test_data_indices],
        bb_features[test_data_indices],
        event_features[test_data_indices],
        labels[test_data_indices],
        weights[test_data_indices],
        spectators[test_data_indices],
        transform=scaler,
    )

    print("Creating weighted data loaders")
    train_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(weights[train_data_indices], labels[train_data_indices])),
        num_samples=n_train_samples,
        replacement=True,
        generator=torchgen,
    )
    train_loader = gloader.DataLoader(
        train_dataset,
        sampler=train_sampler,
        batch_size=batch_size,
        drop_last=True,
        follow_batch=["bb_x"],
    )
    pltmetrics.visualize_class_count(
        train_loader, label_names=label_names, plot_name=plot_path / "class_counts_train.png"
    )
    pltmetrics.visualize_feature(
        train_loader,
        label_names,
        feature_name=spectator_feature_name,
        leading=True,
        bins=np.linspace(0, 1000, 200),
        dataset=hh4b_dataset[train_data_indices],
        density=True,
        log_scale=True,
        weight_negative=False,
        plot_name=plot_path / f"{spectator_feature_name}_leading_resampled_train.png",
    )

    val_sampler = WeightedRandomSampler(
        weights=np.abs(get_normed_weights(weights[valid_data_indices], labels[valid_data_indices])),
        num_samples=n_valid_samples,
        replacement=True,
        generator=torchgen,
    )
    val_loader = gloader.DataLoader(
        val_dataset,
        sampler=val_sampler,
        batch_size=batch_size,
        drop_last=True,
        follow_batch=["bb_x"],
    )

    # -----------------------------------------------------------------------------
    # Training and validation loop
    # -----------------------------------------------------------------------------
    hidden_dim = config["hidden_dim"]
    model = GNNClassifier(node_in_dim, bb_in_dim, global_in_dim, hidden_dim, out_dim).to(device)

    print("Training model")
    train_loss, val_loss = train(
        model,
        train_loader,
        val_loader,
        num_epochs=epochs,
        lr=config["learning_rate"],
        patience=config["patience"],
        initial_dropout_gat=config["initial_dropout_gat"],
        final_dropout_gat=config["final_dropout_gat"],
        initial_dropout_transformer=config["initial_dropout_transformer"],
        final_dropout_transformer=config["final_dropout_transformer"],
        model_id=model_id,
    )
    scaler.save(f"{model_id}_scaler.pkl")

    pltmetrics.plot_metrics(
        {"train_loss": train_loss, "valid_loss": val_loss},
        plot_path,
    )

    #############################
    # Evaluate
    #############################

    test_loader = gloader.DataLoader(
        test_dataset,
        batch_size=batch_size,
        shuffle=False,
        follow_batch=["bb_x"],
    )
    y_predict, y_test, weights, data_idx = eval(model, test_loader)

    # one-hot encoded labels
    y_test = np.eye(len(label_names))[y_test]

    pltmetrics.plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=weights,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_test,
        weights=weights,
        label_names=label_names,
        test_dataset=hh4b_dataset[test_data_indices[data_idx]],
        plot_path=plot_path,
        model_label="CLAHH GNN",
        luminosity=round(luminosity_normalized, 1),
        cutflow=cutflow_normalized,
        sample_campaign=sample_campaign,
    )
