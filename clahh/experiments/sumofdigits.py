import torch
import torch.nn as nn
import numpy as np
import random
import matplotlib.pyplot as plt
from clahh.models.deepsets import DeepSetLayer, DeepSetSum
from clahh.utils.inputs import count_parameters


def run():
    sample_size = 10_000
    batch_size = 16
    input_sizes = [3, 4]
    epochs = 30

    # Test if we got our tensor nonsense right. For a batch size of 6, and n = 7,
    # a DeepSetLayer(2, 3) should map shape (6, 2, 7) to (6, 3, 7).
    print(DeepSetLayer(2, 3)(torch.rand((6, 2, 7))).shape)

    # Check shapes. A DeepSetSum(3) should take shape (batch, 3, n) to (batch, 3)
    print(DeepSetSum(3)(torch.rand((6, 3, 7))).shape)

    # Model structure. Note that we don't specify the input size anywhere!!!
    model = nn.Sequential(
        DeepSetLayer(1, 20),
        nn.ReLU(),
        DeepSetLayer(20, 10),
        nn.ReLU(),
        DeepSetLayer(10, 1),
        DeepSetSum(1),
    )
    print(f"Model has {count_parameters(model)} parameters")

    # Learning rate and loss function.
    optimiser = torch.optim.SGD(model.parameters(), lr=0.005)
    loss_function = nn.L1Loss()

    data = []
    for n_inputs in input_sizes:
        samples = torch.from_numpy(
            np.random.random_sample((sample_size // len(input_sizes), 1, n_inputs))
        ).float()
        for i in range(0, samples.shape[0], batch_size):
            data += [samples[i : i + batch_size]]

    recorded_loss = torch.zeros(epochs)

    for epoch in range(epochs):
        total_loss = 0.0
        random.shuffle(data)
        for samples in data:
            batch_in = samples
            batch_target = batch_in.sum(-1)

            optimiser.zero_grad()
            result = model(batch_in)
            loss = loss_function(result, batch_target)
            total_loss += float(loss)
            loss.backward()
            optimiser.step()

        recorded_loss[epoch] = total_loss
        # print(f"Epoch {epoch}, loss {total_loss}")

    fig, ax = plt.subplots()
    ax.plot(recorded_loss)
    # ax.set_ylim([0, 1])
    plt.show()
    print(f"Final loss is {recorded_loss[-1]}")  # 28.403419494628906

    examples = torch.tensor(
        [
            [0, 1, 0, 1],
            [1, 0, 1, 0],
            [-1, 1, -1, 1],
            [1, -1, 1, -1],
            [0, 0, 0, 0],
        ],
        dtype=torch.float,
    ).reshape(-1, 1, 4)

    with torch.no_grad():
        results = model(examples)

    print(results.numpy().round(2))

    # [[ 2.01]
    #  [ 2.01]
    #  [-0.22]
    #  [-0.22]
    #  [-0.27]]
