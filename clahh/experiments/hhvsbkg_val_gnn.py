import json
import torch
import pickle
import numpy as np
import awkward as ak
import onnxruntime as ort
from pathlib import Path
from torch_geometric.data import Data
import clahh.utils.data_handler as dh
import torch_geometric.loader as gloader
import clahh.plotting.benchmarks as pltbench
import clahh.plotting.metrics as pltmetrics
from clahh.models.gnn_v2 import GNNClassifier
from clahh.utils.metrics import dump_recommended_working_points
from clahh.experiments.hhvsbkg_gnn_weighted_batches import (
    get_config,
    eval,
    HEPEventDataset,
    StandardScalerTransform,
)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def run(
    model_path,
    test_data_path=None,
    file_paths=None,
    scaler_path=None,
    luminosity=126.1,  # fb^-1 (Run 2)
    cutflow_path=None,
    export=False,
    sample_campaign=None,
):
    print("Using device:", device)

    config = get_config()

    batch_size = config["batch_size"]
    num_jets = config["num_jets"]
    jet_feature_names = config["jet_feature_names"]
    bb_feature_names = config["bb_feature_names"]
    event_feature_names = config["event_feature_names"]
    label_names = config["label_names"]
    spectator_jet_names = config["spectator_jet_names"]
    spectator_event_names = config["spectator_event_names"]
    spectator_names = spectator_jet_names + spectator_event_names

    node_in_dim = len(jet_feature_names)
    bb_in_dim = len(bb_feature_names)
    global_in_dim = len(event_feature_names)
    out_dim = len(label_names)

    plot_path = Path("plots")
    if not plot_path.exists():
        plot_path.mkdir(parents=True)

    cutflow = None
    # load test dataset indices if any
    if test_data_path and test_data_path.exists():
        if file_paths:
            if test_data_path.suffix != ".npy":
                raise ValueError(
                    "Test dataset should be in .npy format containing list of event numbers."
                )
            print("Loading datasets...")
            hh4b_dataset = dh.load_data_from_parquet(file_paths, return_cutflow=False)
            test_event_number = np.load(test_data_path)
            test_event_number_idx = dh.get_event_indices(
                hh4b_dataset["event_number"].to_numpy(), test_event_number
            )
            test_dataset = hh4b_dataset[test_event_number_idx]
        else:
            test_dataset = ak.from_parquet(test_data_path)
        print(f"Test dataset size:", len(test_dataset))
    elif file_paths:
        print("Loading datasets...")
        hh4b_dataset, cutflow = dh.load_data_from_parquet(file_paths, return_cutflow=True)
        test_dataset = hh4b_dataset
        print(f"Test dataset size:", len(test_dataset))
    else:
        raise ValueError("Test dataset not provided.")

    # load cutflow if provided
    sample_fraction = 1.0
    luminosity_normalized = luminosity
    cutflow_normalized = None
    if cutflow_path and cutflow_path.exists():
        cutflow = json.load(open(cutflow_path))

    if cutflow:
        sample_fraction = len(test_dataset) / sum(
            [cutflow[target_class]["bjets_tagger_GN2v01_77_count_4"] for target_class in cutflow]
        )
        luminosity_normalized = luminosity * sample_fraction
        cutflow_normalized = {
            target_class: {
                key: value * sample_fraction for key, value in cutflow[target_class].items()
            }
            for target_class in cutflow
        }
        test_benchmarks = pltbench.create_yields_benchmarks(
            test_dataset, cutflow=cutflow_normalized
        )
        print(
            f"Test benchmarks:\n  total signal counts (scaled by test_fract = {sample_fraction}): {test_benchmarks['HH_SR']}\n total eff: {test_benchmarks['total_eff']}"
        )

    # plot_vars(
    #     x=test_dataset[spectator_names],
    #     y=test_dataset[label_names],
    #     plot_path=plot_path,
    # )

    # plot_vars(
    #     x=test_dataset[spectator_jet_names],
    #     y=test_dataset[label_names],
    #     weights=ak.broadcast_arrays(test_dataset["event_weight"], test_dataset["jet_pt"])[0],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # plot_vars(
    #     x=test_dataset[spectator_event_names],
    #     y=test_dataset[label_names],
    #     weights=test_dataset["event_weight"],
    #     plot_name_postfix="weighted",
    #     plot_path=plot_path,
    # )

    # -----------------------------------------------------------------------------
    # Create the dataset and data loaders
    # -----------------------------------------------------------------------------
    weights = test_dataset["event_weight"].to_numpy()
    # Labels one-hot encoded
    labels = np.transpose([test_dataset[y].to_numpy() for y in label_names])
    # jets are truncated at num_jets
    jet_features = np.transpose(
        [
            ak.to_numpy(
                ak.fill_none(
                    ak.pad_none(test_dataset[jet_feature_name], num_jets, clip=True), float("nan")
                )
            )
            for jet_feature_name in jet_feature_names
        ],
        (1, 2, 0),  # Shape: (num_samples, num_jets, num_jet_features)
    )
    bb_features = np.transpose(
        [test_dataset[bb_feature_name].to_numpy() for bb_feature_name in bb_feature_names],
        (1, 2, 0),  # Shape: (num_samples, combinatorial_pairs, num_bb_features)
    )
    event_features = np.transpose(
        [test_dataset[event_feature_name].to_numpy() for event_feature_name in event_feature_names],
        (1, 2, 0),
    ).squeeze(
        -1
    )  # Shape: (num_samples, num_event_features)
    spectators = np.transpose(
        [
            ak.to_numpy(
                ak.fill_none(
                    ak.pad_none(test_dataset[spectator_name], num_jets, clip=True), float("nan")
                )
            )
            for spectator_name in spectator_jet_names
        ],
        (1, 0, 2),  # Shape: (num_samples, num_jet_features, num_jets)
    )

    with open(scaler_path, "rb") as f:
        scaler = pickle.load(f)

    test_dataset = HEPEventDataset(
        jet_features,
        bb_features,
        event_features,
        labels,
        weights,
        spectators,
        transform=scaler,
    )

    test_loader = gloader.DataLoader(
        test_dataset,
        batch_size=batch_size,
        shuffle=False,
        follow_batch=["bb_x"],
    )
    pltmetrics.visualize_class_count(
        test_loader, label_names=label_names, plot_name=plot_path / "class_counts_test.png"
    )

    print("Loading model..")
    hidden_dim = config["hidden_dim"]
    model = GNNClassifier(
        node_in_dim,
        bb_in_dim,
        global_in_dim,
        hidden_dim,
        out_dim,
    )
    model.load_state_dict(
        torch.load(
            model_path,
            map_location=torch.device(device),
        )
    )
    print(model)

    y_predict, y_test, data_wgts, data_idx = eval(model, test_loader)

    # Convert to one-hot encoding
    y_test = np.eye(node_in_dim)[y_test]

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        weights=data_wgts,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_probabilities(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        weights=data_wgts,
        plot_name_postfix="weighted",
        plot_path=plot_path,
    )

    pltmetrics.plot_discriminants(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    pltmetrics.plot_roc_rej_v_eff(
        y_predict=y_predict,
        y_truth=y_test,
        label_names=label_names,
        main_class="label_HH",
        plot_path=plot_path,
    )

    hists_dict = pltbench.create_fitting_hists(
        y_predict=y_predict,
        y_target=y_test,
        weights=data_wgts,
        label_names=label_names,
        test_dataset=hh4b_dataset[data_idx],
        target_signal_eff=test_benchmarks["total_eff"],
        plot_path=plot_path,
        vars={"clahh": "discrim", "nominal": "hh_mass"},
        cutflow=cutflow_normalized,
        return_hists=True,
    )
    pltbench.plot_limit_histograms(
        hists_dict, plot_path, clahh_x_label="Discriminant", nominal_x_label="m(HH) [GeV]"
    )

    pltbench.create_benchmark_rocs(
        y_predict=y_predict,
        y_target=y_test,
        weights=data_wgts,
        label_names=label_names,
        test_dataset=hh4b_dataset[data_idx],
        plot_path=plot_path,
        model_label="CLAHH GNN",
        luminosity=round(luminosity_normalized, 1),
        cutflow=cutflow_normalized,
        sample_campaign=sample_campaign,
    )

    if export:
        print("Saving recommended working points...")
        dump_recommended_working_points(
            y_predict=y_predict,
            y_target=y_test,
            label_names=label_names,
            weights=data_wgts,
            total=cutflow_normalized["label_HH"]["initial_events_weighted"],
            save_path="deepsets_working_points.json",
        )
        # print("Exporting model to ONNX...")
        # model_save_pathname = f"{model_path.stem}.onnx"
        # model_in = Data(
        #     x=torch.zeros((num_jets, node_in_dim), dtype=torch.float32),
        #     edge_index=torch.zeros((2, num_jets), dtype=torch.long),
        #     bb_x=torch.zeros((6, bb_in_dim), dtype=torch.float32),
        #     global_x=torch.zeros((1, global_in_dim), dtype=torch.float32),
        #     batch=torch.zeros((1,), dtype=torch.long),
        # )

        # model_out = model(model_in)

        # torch.onnx.export(
        #     model,
        #     model_in,
        #     model_save_pathname,
        #     input_names=["jet_features", "bb_features", "event_features"],
        #     output_names=["output"],
        #     dynamic_axes={
        #         "jet_features": {0: "batch", 1: "num_jets", 2: "node_in_dim"},
        #         "bb_features": {0: "batch", 1: "num_bb_pairs", 2: "bb_in_dim"},
        #         "event_features": {0: "batch", 1: "global_in_dim"},
        #         "output": {0: "batch", 1: "out_dim"},
        #     },
        # )

        # ort_session = ort.InferenceSession(model_save_pathname)
        # ort_inputs = {
        #     "jet_features": np.zeros((1, num_jets, node_in_dim), dtype=np.float32),
        #     "bb_features": np.zeros((1, 6, bb_in_dim), dtype=np.float32),
        #     "event_features": np.zeros((1, global_in_dim), dtype=np.float32),
        # }
        # ort_outs = ort_session.run(None, ort_inputs)
        # print(f"ONNX model output: {ort_outs}")
        # np.testing.assert_allclose(model_out.detach().numpy(), ort_outs[0], rtol=1e-03, atol=1e-05)
        # print("Exported model has been tested with ONNXRuntime, and the result looks good!")
