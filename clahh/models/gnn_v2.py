import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import GATConv, global_mean_pool
from torch.nn import TransformerEncoder, TransformerEncoderLayer


class GNNClassifier(nn.Module):
    def __init__(
        self,
        node_in_dim,
        bb_in_dim,
        global_in_dim,
        hidden_dim,
        out_dim,
        num_heads=4,
        num_layers=2,
        init_dropout_gat=0.1,
        init_dropout_transformer=0.1,
    ):
        super(GNNClassifier, self).__init__()
        self.node_in_dim = node_in_dim
        self.bb_in_dim = bb_in_dim
        self.global_in_dim = global_in_dim
        self.hidden_dim = hidden_dim
        self.out_dim = out_dim

        # Adaptive dropout rates stored as attributes;
        # Updated during training with self.set_dropout_rates().
        self.adaptive_dropout_gat = init_dropout_gat
        self.adaptive_dropout_transformer = init_dropout_transformer

        # Graph Attention Layers with internal dropout disabled
        self.gat1 = GATConv(node_in_dim, hidden_dim, heads=num_heads, dropout=0)
        self.gat2 = GATConv(hidden_dim * num_heads, hidden_dim, heads=1, dropout=0)

        # Transformer Encoder for node features with dropout disabled internally
        encoder_layer = TransformerEncoderLayer(d_model=hidden_dim, nhead=num_heads, dropout=0)
        self.transformer = TransformerEncoder(encoder_layer, num_layers=num_layers)
        self.layer_norm = nn.LayerNorm(hidden_dim)

        # Transformer Encoder for bb_x features
        self.bb_dense = nn.Linear(bb_in_dim, hidden_dim)
        bb_encoder_layer = TransformerEncoderLayer(d_model=hidden_dim, nhead=num_heads, dropout=0)
        self.bb_transformer = TransformerEncoder(bb_encoder_layer, num_layers=num_layers)
        self.bb_layer_norm = nn.LayerNorm(hidden_dim)

        # Fully Connected Layers
        self.fc1 = nn.Linear(hidden_dim + hidden_dim + global_in_dim, 128)
        self.fc2 = nn.Linear(128, out_dim)

    def forward(self, data):
        # Handle missing values in node features
        valid_mask = ~torch.isnan(data.x)
        x = data.x.masked_fill(~valid_mask, 0)

        # GAT Layers with adaptive dropout applied after each layer
        x = F.elu(self.gat1(x, data.edge_index))
        x = F.dropout(x, p=self.adaptive_dropout_gat, training=self.training)
        x = F.elu(self.gat2(x, data.edge_index))
        x = F.dropout(x, p=self.adaptive_dropout_gat, training=self.training)

        # Transformer Encoder for node features
        x = self.transformer(x)
        x = self.layer_norm(x)
        x = F.dropout(x, p=self.adaptive_dropout_transformer, training=self.training)
        # Global Pooling to obtain event-level representation for nodes
        pad_mask = valid_mask.all(dim=1).unsqueeze(1)
        x = global_mean_pool(x * pad_mask.float(), data.batch)

        # Process bb_x through its own transformer pathway
        bb_x = self.bb_dense(data.bb_x)
        bb_x = self.bb_transformer(bb_x)
        bb_x = self.bb_layer_norm(bb_x)
        bb_x = F.dropout(bb_x, p=self.adaptive_dropout_transformer, training=self.training)
        # Global Pooling to obtain event-level representation for bb_x
        bb_x = global_mean_pool(bb_x, data.bb_x_batch)

        # Concatenate node, bb_x, and global features
        x = torch.concat([x, bb_x, data.global_x], dim=1)

        # Fully Connected Layers with dropout before final classification
        x = self.fc1(x)
        x = F.relu(x)
        x = F.dropout(x, p=self.adaptive_dropout_transformer, training=self.training)
        x = self.fc2(x)
        return x

    def set_dropout_rates(self, dropout_gat, dropout_transformer):
        """
        Update the adaptive dropout rates. You can call this method
        from your training loop as the training dynamics evolve.
        """
        self.adaptive_dropout_gat = dropout_gat
        self.adaptive_dropout_transformer = dropout_transformer
