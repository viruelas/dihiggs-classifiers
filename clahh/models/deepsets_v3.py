import torch
import torch.nn as nn
from torch.nn import (
    Sequential,
    Linear,
    ReLU,
    BatchNorm1d,
    Conv1d,
    Dropout,
)
from clahh.modules.batchnorm import MaskedBatchNorm1d
from clahh.modules.attention import Attention


class DeepSets(nn.Module):
    def __init__(
        self,
        input_jet_size=0,  # (n_jet_features)
        input_bb_size=0,  # (n_bb_features)
        input_event_size=0,  # (n_evt_features)
        hidden1=64,
        hidden2=32,
        hidden3=16,
        hidden4=50,
        outputs=3,
        njets=10,
        dropout=0.0,
    ):
        super(DeepSets, self).__init__()

        phi_input_sizes = [input_jet_size, input_bb_size]

        self.input_jet_size = input_jet_size
        self.input_bb_size = input_bb_size
        self.input_event_size = input_event_size
        self.hidden1 = hidden1
        self.hidden2 = hidden2
        self.hidden3 = hidden3
        self.hidden4 = hidden4
        self.outputs = outputs
        self.njets = njets
        self.rho_input_size = (
            hidden3 * sum([input_size > 0 for input_size in phi_input_sizes]) + input_event_size
        )

        def create_phi(input_size):
            layers = [
                MaskedBatchNorm1d(input_size),
                Conv1d(input_size, hidden1, kernel_size=1),
                BatchNorm1d(hidden1),
                ReLU(),
                Conv1d(hidden1, hidden2, kernel_size=1),
                BatchNorm1d(hidden2),
                ReLU(),
                Dropout(p=dropout),
                Conv1d(hidden2, hidden3, kernel_size=1),
                BatchNorm1d(hidden3),
                ReLU(),
            ]
            return Sequential(*layers)

        self.phi_jet = create_phi(input_jet_size)
        self.phi_bb = create_phi(input_bb_size)

        self.rho = Sequential(
            BatchNorm1d(self.rho_input_size),
            Linear(self.rho_input_size, hidden4),
            ReLU(),
            Dropout(p=dropout),
            Attention(hidden4),
            Linear(hidden4, outputs),
        )

    def forward(self, jet_x, bb_x, evt_x):
        # Create a mask for valid jets
        valid_jet_mask = ~torch.isnan(jet_x)
        # Replace NaNs with zeros to prevent propagation of NaNs
        jet_x = jet_x.masked_fill(torch.isnan(jet_x), 0)

        # iterate over the self.phi network layers and apply them sequentially
        phi_jet_out = jet_x
        for layer in self.phi_jet:
            if isinstance(layer, MaskedBatchNorm1d):
                phi_jet_out = layer(phi_jet_out, valid_jet_mask)
            else:
                phi_jet_out = layer(phi_jet_out)

        # Create a mask for the pooling operation
        pad_mask = valid_jet_mask.all(dim=1)  # True where at least one feature is not NaN
        # Expand pad_mask to match logits dimensions for broadcasting
        pad_mask_expanded = pad_mask.unsqueeze(1)  # (batch_size, 1, njets)
        phi_jet_out = (phi_jet_out * pad_mask_expanded.float()).sum(dim=-1)

        phi_bb_out = self.phi_bb(bb_x)
        phi_bb_out = phi_bb_out.sum(dim=-1)

        # Concatenate the event features to the output of the phi network
        rho_in = torch.cat((phi_jet_out, phi_bb_out, evt_x), dim=-1)
        rho_out = self.rho(rho_in)

        return rho_out
