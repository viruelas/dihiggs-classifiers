import torch
import torch.nn as nn

def rand(shape, low, high):
    """Tensor of random numbers, uniformly distributed on [low, high]."""
    return torch.rand(shape) * (high - low) + low

class DeepSetLayer(nn.Module):
    """
    DeepSetLayer(in_blocks, out_blocks) takes shape (batch, in_blocks, n) to (batch, out_blocks, n).
    Each block of n scalars is treated as the S_n permutation representation, and maps between blocks are
    S_n-equivariant.
    """
    def __init__(self, in_blocks, out_blocks):
        super().__init__()

        self.in_blocks = in_blocks
        self.out_blocks = out_blocks

        # Initialisation tactic copied from nn.Linear in PyTorch
        lim = (in_blocks)**-0.5 / 2

        # Alpha corresponds to the identity, beta to the all-ones matrix, and gamma to the additive bias.
        self.alpha = torch.nn.Parameter(data=rand((out_blocks, in_blocks), -lim, lim))
        self.beta = torch.nn.Parameter(data=rand((out_blocks, in_blocks), -lim, lim))
        self.gamma = torch.nn.Parameter(data=rand((out_blocks), -lim, lim))

    def forward(self, x):
        # x has shape (batch, in_blocks, n)
        return (
            torch.einsum('...jz, ij -> ...iz', x, self.alpha)
            + torch.einsum('...jz, ij -> ...iz', x.sum(axis=-1)[..., None], self.beta)
            + self.gamma[..., None]
        )


class DeepSetSum(nn.Module):
    """
    DeepSetSum(blocks) takes a deep set layer of shape (batch, blocks, n) to a regular layer
    of shape (batch, blocks) by projecting to the trivial representation and then extracting
    a coordinate, eg
        (1, 2, 3, 4) => project to trivial => (2.5, 2.5, 2.5, 2.5) => extract component => 2.5
    """
    def __init__(self, blocks):
        super().__init__()

        lim = (blocks)**-0.5 / 2
        self.weight = torch.nn.Parameter(data=rand(blocks, -lim, lim))
        self.bias = torch.nn.Parameter(data=rand(blocks, -lim, lim))

    def forward(self, x):
        return x.sum(dim=-1) * self.weight + self.bias

