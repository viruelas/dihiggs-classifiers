import torch.nn as nn


class SimpleNeuralNetwork(nn.Module):
    def __init__(self, input_size, hidden_size=512, output_size=3):
        super().__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size),
        )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits
