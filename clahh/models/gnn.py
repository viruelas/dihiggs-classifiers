import torch
import torch.nn as nn
from torch_geometric.nn import GATConv, global_mean_pool
import torch.nn.functional as F


class GNN(nn.Module):
    """
    GNN model with two GAT layers and two fully connected layers.
    """

    def __init__(
        self, n_node_features, n_graph_features, n_classes=3, dropout=0.3, dropout_gat=0.1
    ):
        super(GNN, self).__init__()
        self.conv1 = GATConv(n_node_features, 16, heads=4, concat=True, dropout=dropout_gat)
        self.conv2 = GATConv(64, 32, heads=4, concat=True, dropout=dropout_gat)
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.fc1 = nn.Linear(128 + n_graph_features, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, n_classes)
        self.dropout = nn.Dropout(p=dropout)
        self.n_node_features = n_node_features
        self.n_graph_features = n_graph_features
        self.n_classes = n_classes

    def forward(self, data):
        x, edge_index = data.x, data.edge_index
        # Apply mask to x
        node_mask = data.node_mask
        x = x.masked_fill(~node_mask, 0.0)
        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = self.bn1(x)
        x = self.conv2(x, edge_index)
        x = F.relu(x)
        x = self.bn2(x)
        x = global_mean_pool(x, data.batch)
        # Reshape graph_features to match the batch size
        graph_features = data.graph_features.view(
            x.size(0), -1
        )  # Reshape to (batch_size, num_features)
        graph_mask = data.graph_mask.view(x.size(0), -1)
        graph_features = graph_features.masked_fill(~graph_mask, 0.0)
        x = torch.cat([x, graph_features], dim=1)
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        x = F.relu(self.fc2(x))
        x = self.dropout(x)
        x = self.fc3(x)
        return F.log_softmax(x, dim=1)
