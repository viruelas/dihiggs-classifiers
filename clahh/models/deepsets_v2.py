import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import (
    Sequential as Seq,
    Linear as Lin,
    ReLU,
    BatchNorm1d,
    Conv1d,
    Softmax,
)


class DeepSets(nn.Module):
    def __init__(
        self,
        input_jet_size=0,  # (n_jet_features
        input_event_size=0,  # (n_evt_features)
        hidden1=100,
        hidden2=100,
        hidden3=128,
        classify1=30,
        outputs=3,
        njets=10,
    ):
        super(DeepSets, self).__init__()

        self.input_jet_size = input_jet_size
        self.input_event_size = input_event_size
        self.hidden1 = hidden1
        self.hidden2 = hidden2
        if input_jet_size > 0:
            self.hidden3 = hidden3
        if input_jet_size < 1 and input_event_size > 0:
            self.hidden3 = 0
        else:
            self.hidden3 = hidden3
        self.classify1 = classify1
        self.outputs = outputs
        self.njets = njets

        self.phi = Seq(
            Conv1d(self.input_jet_size, self.hidden1, 1),
            BatchNorm1d(self.hidden1),
            ReLU(),
            Conv1d(self.hidden1, self.hidden2, 1),
            BatchNorm1d(self.hidden2),
            ReLU(),
            Conv1d(self.hidden2, self.hidden3, 1),
            BatchNorm1d(self.hidden3),
            ReLU(),
        )
        self.rho = Seq(
            Lin(self.hidden3 + self.input_event_size, self.hidden1),
            BatchNorm1d(self.hidden1),
            ReLU(),
            Lin(self.hidden1, self.hidden1),
            BatchNorm1d(self.hidden1),
            ReLU(),
            Lin(self.hidden1, self.classify1),
            BatchNorm1d(self.classify1),
            ReLU(),
            Lin(self.classify1, self.outputs),
            BatchNorm1d(self.outputs),
            ReLU(),
            Softmax(dim=-1),
        )

    def forward(self, jet_x, evt_x):
        invalid_jet_x = torch.all(jet_x == 0)
        invalid_event_x = torch.all(evt_x == 0)

        if invalid_jet_x and not invalid_event_x:
            rho_out = self.rho(evt_x)
        elif not invalid_jet_x and invalid_event_x:
            phi_out = self.phi(jet_x)
            phi_out = phi_out.sum(dim=-1)
            rho_out = self.rho(phi_out)
        else:
            phi_out = self.phi(jet_x)
            phi_out = phi_out.sum(dim=-1)
            rho_in = torch.cat((phi_out, evt_x), dim=-1)
            rho_out = self.rho(rho_in)
        return rho_out
