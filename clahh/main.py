import argparse
from pathlib import Path
from clahh.experiments import (
    hhvsbkg_simple,
    hhvsbkg_deep_sets_weighted_batches,
    hhvsbkg_gnn_weighted_batches,
    hhvsbkg_val_simple,
    hhvsbkg_val_deep_sets,
    hhvsbkg_val_gnn,
)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--datasets",
        nargs="+",
        type=Path,
        default=[],
        help="List of paths to datasets.",
    )
    parser.add_argument(
        "-a",
        "--model-version",
        type=str,
        choices=["deepsets", "gnn", "nn"],
        help="Model architecture version to train.",
    )
    parser.add_argument(
        "-m",
        "--model-path",
        type=Path,
        help="Path to model checkpoint. Runs the experiments in validation mode.",
    )
    parser.add_argument(
        "-t",
        "--test-data",
        type=Path,
        help="Path to test dataset file (may be indices).",
    )
    parser.add_argument(
        "-x",
        "--export",
        action="store_true",
        help="Export the model to ONNX format.",
    )
    parser.add_argument(
        "-s",
        "--scaler-path",
        type=Path,
        help="List of path to scalers. Node scalers first, then graph scalers if arch is GNN.",
    )
    parser.add_argument(
        "-l",
        "--luminosity",
        type=float,
        default=126.1,
        help="Luminosity in fb^-1 for the whole dataset (ie Run 2) (default: %(default)s)",
    )
    parser.add_argument(
        "-c",
        "--cutflow",
        type=Path,
        help="Path to json file containing cutflow.",
    )
    parser.add_argument(
        "-n",
        "--campaign",
        type=str,
        default="MC20",
        help="Name of the sample campaign.",
    )
    parser.add_argument(
        "-f",
        "--fast-dev-run",
        action="store_true",
        help="Run the experiments in fast dev run mode.",
    )
    return parser.parse_args(), parser


def main():
    args, parser = parse_args()

    # Get the choices list from parser for the --model-version flag
    model_version_choices = next(x for x in parser._actions if x.dest == "model_version").choices
    # set variable called model_version which is parsed from the command line argument --model-path and stores the value if it contains a string from the model_version_choices list
    if args.model_version is None:
        args.model_version = next(
            (x for x in model_version_choices if x in str(args.model_path)), None
        )
    if args.model_version is None:
        parser.print_help()
        raise ValueError(
            "Model version not found in model path! Please provide the model version using the --model-version flag."
        )

    print("Running the experiments!")
    if not args.model_path:
        if args.model_version == "nn":
            hhvsbkg_simple.run(args.datasets, fast_dev_run=args.fast_dev_run)
        elif args.model_version == "deepsets":
            hhvsbkg_deep_sets_weighted_batches.run(
                args.datasets, sample_campaign=args.campaign, fast_dev_run=args.fast_dev_run
            )
        elif args.model_version == "gnn":
            hhvsbkg_gnn_weighted_batches.run(
                args.datasets, sample_campaign=args.campaign, fast_dev_run=args.fast_dev_run
            )
    else:
        if args.model_version == "nn":
            hhvsbkg_val_simple.run(args.model_path, args.datasets, fast_dev_run=args.fast_dev_run)
        elif args.model_version == "deepsets":
            hhvsbkg_val_deep_sets.run(
                args.model_path,
                test_data_path=args.test_data,
                file_paths=args.datasets,
                export=args.export,
                luminosity=args.luminosity,
                cutflow_path=args.cutflow,
                sample_campaign=args.campaign,
            )
        elif args.model_version == "gnn":
            hhvsbkg_val_gnn.run(
                args.model_path,
                test_data_path=args.test_data,
                file_paths=args.datasets,
                scaler_path=args.scaler_path,
                export=args.export,
                luminosity=args.luminosity,
                cutflow_path=args.cutflow,
                sample_campaign=args.campaign,
            )


if __name__ == "__main__":
    main()
