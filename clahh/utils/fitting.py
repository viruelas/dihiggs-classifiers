import torch
import torch.nn as nn
from tqdm import tqdm


def train_epoch(
    model,
    optimizer,
    loader,
    epoch,
    device,
    class_weights=None,
    reduction="mean",
):
    model.train()

    loss_fn = nn.CrossEntropyLoss(reduction=reduction, weight=class_weights)

    sum_loss = 0.0
    for batch in tqdm(
        loader,
        total=len(loader),
        ncols=80,
        desc=f"Train Epoch {epoch}",
        leave=False,
    ):
        # Compute the prediction and loss
        X_jet, X_bb, X_evnt, y, wgts = (
            batch.x_jet.to(device),
            batch.x_bb.to(device),
            batch.x_event.to(device),
            batch.y.to(device),
            batch.weights.to(device),
        )
        pred = model(X_jet, X_bb, X_evnt)
        y = torch.argmax(y, dim=1)
        batch_loss = loss_fn(pred, y)
        if reduction == "none":
            # convert w to tensor which is -1 if w is negative, 1 if w is positive,
            # and 0 if w is zero. this is because the batches are already weighted
            # with abs(w) in the dataloader
            wgts_sign = torch.sign(wgts)
            batch_loss = torch.mean(batch_loss * wgts_sign)

        # Backpropagation
        batch_loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        # Sum up the batch loss
        sum_loss += batch_loss.item()

    return sum_loss / len(loader.dataset)


@torch.no_grad()
def test_epoch(
    model,
    loader,
    epoch,
    device,
    class_weights=None,
    reduction="mean",
    return_accuracy=False,
):
    model.eval()

    loss_fn = nn.CrossEntropyLoss(reduction=reduction, weight=class_weights)

    sum_loss = 0.0
    correct = 0
    for batch in tqdm(
        loader,
        total=len(loader),
        ncols=80,
        desc=f"Validation Epoch {epoch}",
        leave=False,
    ):
        X_jet, X_bb, X_evnt, y, wgts = (
            batch.x_jet.to(device),
            batch.x_bb.to(device),
            batch.x_event.to(device),
            batch.y.to(device),
            batch.weights.to(device),
        )
        pred = model(X_jet, X_bb, X_evnt)
        y = torch.argmax(y, dim=1)
        batch_loss = loss_fn(pred, y)
        y_hat = nn.Softmax(dim=1)(pred)
        y_hat = torch.argmax(y_hat, dim=1)
        if reduction == "none":
            # convert w to tensor which is -1 if w is negative, 1 if w is positive,
            # and 0 if w is zero. this is because the batches are already weighted
            # with abs(w) in the dataloader
            wgts_sign = torch.sign(wgts)
            batch_loss = torch.mean(batch_loss * wgts_sign)
        sum_loss += batch_loss.item()
        correct += (y_hat == y).type(torch.float).sum().item()

    if return_accuracy:
        return sum_loss / len(loader.dataset), correct / len(loader.dataset)
    return sum_loss / len(loader.dataset)
