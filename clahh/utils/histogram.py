import numpy as np


def hist_w_unc(a, bins, normed: bool = True):
    """
    Computes histogram and the associated statistical uncertainty.

    Parameters
    ----------
    a : array_like
        Input data. The histogram is computed over the flattened array.
    bins: int or sequence of scalars or str
        bins parameter from np.histogram
    normed: bool
        If True (default) the calculated histogram is normalised to an integral
        of 1.

    Returns
    -------
    bin_edges : array of dtype float
        Return the bin edges (length(hist)+1)
    hist : numpy array
        The values of the histogram. If normed is true (default), returns the
        normed counts per bin
    unc : numpy array
        Statistical uncertainty per bin.
        If normed is true (default), returns the normed values.
    band : numpy array
        lower uncertainty band location: hist - unc
        If normed is true (default), returns the normed values.
    """
    arr_length = len(a)

    # Calculate the counts and the bin edges
    counts, bin_edges = np.histogram(a, bins=bins)

    unc = save_divide(np.sqrt(counts), arr_length, 0) if normed else np.sqrt(counts)
    band = save_divide(counts, arr_length, 0) - unc if normed else counts - unc
    hist = save_divide(counts, arr_length, 0) if normed else counts

    return bin_edges, hist, unc, band


def save_divide(numerator, denominator, default=1.0):
    """
    Division using numpy divide function returning default value in cases where
    denominator is 0.

    Parameters
    ----------
    numerator: array_like
        Numerator in the ratio calculation.
    denominator: array_like
        Denominator in the ratio calculation.
    default: float
        default value which is returned if denominator is 0.

    Returns
    -------
    ratio: array_like
    """
    if isinstance(numerator, (int, float, np.number)) and isinstance(
        denominator, (int, float, np.number)
    ):
        output_shape = 1
    else:
        try:
            output_shape = denominator.shape
        except AttributeError:
            output_shape = numerator.shape

    ratio = np.divide(
        numerator,
        denominator,
        out=np.ones(
            output_shape,
            dtype=float,
        )
        * default,
        where=(denominator != 0),
    )
    if output_shape == 1:
        return float(ratio)
    return ratio
