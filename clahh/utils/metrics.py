import json
import operator
import numpy as np

#####
# Plotting helper functions
# Many of the functions are taken from the puma-hep package to work with python<3.10
# https://github.com/umami-hep/puma.git
#####


def get_score(
    y_pred: np.ndarray,
    class_labels: list,
    main_class: str,
    frac_dict: dict,
) -> np.ndarray:
    """
    Similar to CalcDiscValues but uses directly the output of the
    NN (shape: (n_jets, nClasses)) for calculation.

    Parameters
    ----------
    y_pred : numpy.ndarray
        The prediction output of the NN.
    class_labels : list
        A list of the class_labels which are used.
    main_class : str
        The main discriminant class. For HH-tagging obviously "HH".
    frac_dict : dict
        A dict with the respective fractions for each class provided
        except main_class.
    use_keras_backend : bool
        Decide, if the values are calculated with the keras backend
        or numpy (Keras is needed for the saliency maps).

    Returns
    -------
    disc_score : numpy.ndarray
        Discriminant Score for the jets provided.

    Raises
    ------
    KeyError
        If for the given class label no frac_dict entry is given

    Examples
    --------
    >>> y_pred = np.array(
    ...     [
    ...         [0.1, 0.1, 0.8],
    ...         [0.0, 0.1, 0.9],
    ...         [0.2, 0.6, 0.2],
    ...         [0.1, 0.8, 0.1],
    ...     ]
    ... )
    array([[0.1, 0.1, 0.8],
           [0. , 0.1, 0.9],
           [0.2, 0.6, 0.2],
           [0.1, 0.8, 0.1]])

    >>> class_labels = ["HH", "QCD", "ttbar"]
    ['HH', 'QCD', 'ttbar']

    >>> main_class = "HH"
    'HH'

    >>> frac_dict = {"ttbar": 0.018, "QCD": 0.982}
    {'ttbar': 0.018, 'QCD': 0.982}

    Now we can call the function which will return the discriminant values
    for the given jets based on their given NN outputs (y_pred).

    >>> disc_scores = get_score(
    ...     y_pred=y_pred,
    ...     class_labels=class_labels,
    ...     main_class=main_class,
    ...     frac_dict=frac_dict,
    ... )
    [2.07944154, 6.21460804, -0.03536714, -0.11867153]
    """

    # Check if y_pred and class_labels have the same shapes
    assert np.shape(y_pred)[1] == len(class_labels)

    # Assert that frac_dict has class_labels as keys except main_class
    assert set(frac_dict.keys()) == set(class_labels) - set([main_class])

    # Ensure that y_pred is full precision (float32)
    if isinstance(y_pred, np.ndarray):
        y_pred = y_pred.astype("float32")

    # Get class_labels_idx for class_labels
    class_labels_idx = {label: idx for idx, label in enumerate(class_labels)}

    # Get list of class_labels without main_class
    class_labels_wo_main = [
        class_label for class_label in class_labels if class_label != main_class
    ]

    # Init denominator of disc_score and add_small
    denominator = 0
    numerator = 0
    add_small = 1e-10

    # Calculate numerator of disc_score
    numerator += y_pred[:, class_labels_idx[main_class]]
    numerator += add_small

    # Calculate denominator of disc_score
    for class_label in class_labels_wo_main:
        denominator += frac_dict[class_label] * y_pred[:, class_labels_idx[class_label]]

    denominator += add_small

    # Calculate final disc_score and return it
    disc_value = np.log(numerator / denominator)

    return disc_value


def safe_divide(
    numerator,
    denominator,
    default: float = np.inf,
):
    """
    Division using numpy divide function returning default value in cases where
    denominator is 0.

    Parameters
    ----------
    numerator: array_like, int, float
        Numerator in the ratio calculation.
    denominator: array_like, int, float
        Denominator in the ratio calculation.
    default: float
        Default value which is returned if denominator is 0.

    Returns
    -------
    ratio: array_like, float
    """
    if isinstance(numerator, (int, float, np.number)) and isinstance(
        denominator, (int, float, np.number)
    ):
        output_shape = 1
    else:
        try:
            output_shape = denominator.shape
        except AttributeError:
            output_shape = numerator.shape

    ratio = np.divide(
        numerator,
        denominator,
        out=np.ones(
            output_shape,
            dtype=float,
        )
        * default,
        where=(denominator != 0),
    )
    if output_shape == 1:
        return float(ratio)
    return ratio


def weighted_percentile(data, percentiles, weights=None):
    """
    Calculate the weighted percentiles of a 1D numpy array.

    Parameters:
    data (numpy.array): The data array.
    weights (numpy.array): The weights array.
    percentiles (numpy.array): The percentiles to calculate, which must be between 0 and 100 inclusive.

    Returns:
    numpy.array: The weighted percentiles of the data.
    """
    if not np.all((0 <= percentiles) & (percentiles <= 100)):
        raise ValueError("all percentiles must be between 0 and 100")

    if weights is None:
        weights = np.ones_like(data)

    # Sort the data and weights by the data values
    sorted_indices = np.argsort(data)
    sorted_data = data[sorted_indices]
    sorted_weights = weights[sorted_indices]

    # Calculate the cumulative sum of weights
    cum_weights = np.cumsum(sorted_weights)

    # Find the indices where the cumulative sum of weights exceeds the percentiles
    indices = np.searchsorted(cum_weights, cum_weights[-1] * percentiles / 100)

    return sorted_data[indices]


def get_discriminant_cut(clahh_discs, weights, target_eff, bins: int = 1000, total: float = None):
    """
    Calculates the discriminant cut value that corresponds to the desired target efficiency.
    Args:
        clahh_discs (np.ndarray): Array of discriminant values for HH.
        weights (np.ndarray): Array of event weights corresponding to clahh_discs.
        bins (int): Number of bins used to histogram the discriminant.
        target_eff (float or np.ndarray): Desired signal efficiency (between 0 and 1). Can be a single float or an array.
        total (float): Total weighted number of HH events. If None, the sum of weights is used.

    Returns:
        float or np.ndarray: The discriminant cut value such that events with discriminant > cut_value
                give an efficiency close to target_eff.
    """
    # Histogram the discriminant using the provided bins and weights.
    hist_counts, edges = np.histogram(clahh_discs, bins=bins, weights=weights)
    # Compute cumulative efficiency from high discriminant values downwards.
    cum_eff_rev = hist_counts[::-1].cumsum() / (hist_counts.sum() if total is None else total)

    # Ensure target_eff is a numpy array
    target_eff_arr = np.atleast_1d(target_eff)

    # For each target efficiency, find the index in the reversed cumulative distribution that is closest to the target.
    diff = np.abs(cum_eff_rev - target_eff_arr[:, None])
    idx_rev = np.argmin(diff, axis=1)
    orig_idx = len(hist_counts) - 1 - idx_rev
    cutvalues = edges[orig_idx]

    # If a scalar input was provided, return a scalar output.
    if np.isscalar(target_eff) or target_eff_arr.shape[0] == 1:
        return float(cutvalues[0])
    return cutvalues


def calc_eff(
    sig_disc: np.ndarray,
    bkg_disc: np.ndarray,
    target_eff: float or list or np.ndarray,
    sig_weights: np.ndarray = None,
    bkg_weights: np.ndarray = None,
    unconditional_disc: np.array = None,
    unconditional_weights: np.ndarray = None,
    return_cuts: bool = False,
):
    """Calculate efficiency.

    Parameters
    ----------
    sig_disc : np.ndarray
        Signal discriminant
    bkg_disc : np.ndarray
        Background discriminant
    target_eff : float or list or np.ndarray
        Working point which is used for discriminant calculation
    return_cuts : bool
        Specifies if the cut values corresponding to the provided WPs are returned.
        If target_eff is a float, only one cut value will be returned. If target_eff
        is an array, target_eff is an array as well.
    sig_weights : np.ndarray
        Weights for signal events
    bkg_weights : np.ndarray
        Weights for background events

    Returns
    -------
    eff : float or np.ndarray
        Efficiency.
        Return float if target_eff is a float, else np.ndarray
    cutvalue : float or np.ndarray
        Cutvalue if return_cuts is True.
        Return float if target_eff is a float, else np.ndarray
    """
    target_eff_arr = np.asarray([target_eff]).flatten()
    # because we want to start from the highest signal efficiency
    percentiles = (1 - target_eff_arr) * 100
    cutvalue = weighted_percentile(sig_disc, percentiles, weights=sig_weights)
    sorted = np.argsort(percentiles)
    hist, _ = np.histogram(
        bkg_disc,
        (-np.inf, *cutvalue[sorted], np.inf),
        weights=bkg_weights,
    )
    n_tot = hist.sum()
    if unconditional_disc is not None:
        hist_unconditional, _ = np.histogram(
            unconditional_disc,
            (-np.inf, *cutvalue[sorted], np.inf),
            weights=unconditional_weights,
        )
        n_tot = hist_unconditional.sum()
    eff = hist[::-1].cumsum()[-2::-1] / n_tot
    eff = eff[sorted]

    if isinstance(target_eff, float):
        eff = eff[0]
        cutvalue = cutvalue[0]

    if return_cuts:
        return eff, cutvalue
    return eff


def calc_rej(
    sig_disc: np.ndarray,
    bkg_disc: np.ndarray,
    target_eff: float or list or np.ndarray,
    sig_weights: np.ndarray = None,
    bkg_weights: np.ndarray = None,
    unconditional_disc: np.ndarray = None,
    unconditional_weights: np.ndarray = None,
    return_cuts: bool = False,
):
    """Calculate efficiency.

    Parameters
    ----------
    sig_disc : np.ndarray
        Signal discriminant
    bkg_disc : np.ndarray
        Background discriminant
    target_eff : float or list
        Working point which is used for discriminant calculation
    return_cuts : bool
        Specifies if the cut values corresponding to the provided WPs are returned.
        If target_eff is a float, only one cut value will be returned. If target_eff
        is an array, target_eff is an array as well.
    sig_weights : np.ndarray
        Weights for signal events, by default None
    bkg_weights : np.ndarray
        Weights for background events, by default None

    Returns
    -------
    rej : float or np.ndarray
        Rejection.
        If target_eff is a float, a float is returned if it's a list a np.ndarray
    cut_value : float or np.ndarray
        Cutvalue if return_cuts is True.
        If target_eff is a float, a float is returned if it's a list a np.ndarray
    """
    eff = calc_eff(
        sig_disc=sig_disc,
        bkg_disc=bkg_disc,
        target_eff=target_eff,
        sig_weights=sig_weights,
        bkg_weights=bkg_weights,
        unconditional_disc=unconditional_disc,
        unconditional_weights=unconditional_weights,
        return_cuts=return_cuts,
    )
    rej = safe_divide(1, eff[0] if return_cuts else eff, np.inf)

    if return_cuts:
        return rej, eff[1]
    return rej


def eff_err(
    arr: np.ndarray,
    n_counts: int,
    norm: bool = False,
) -> np.ndarray:
    """Calculate statistical efficiency uncertainty.

    Parameters
    ----------
    arr : numpy.array
        Efficiency values
    n_counts : int
        Number of used statistics to calculate efficiency
    norm : bool, optional
        If True, normed (relative) error is being calculated, by default False

    Returns
    -------
    numpy.array
        Efficiency uncertainties

    Raises
    ------
    ValueError
        If n_counts <=0

    Notes
    -----
    This method uses binomial errors as described in section 2.2 of
    https://inspirehep.net/files/57287ac8e45a976ab423f3dd456af694
    """
    if n_counts <= 0:
        raise ValueError(f"You passed as argument `N` {n_counts} but it has to be larger 0.")
    if norm:
        return np.sqrt(arr * (1 - arr) / n_counts) / arr
    return np.sqrt(arr * (1 - arr) / n_counts)


def rej_err(
    arr: np.ndarray,
    n_counts: int,
    norm: bool = False,
) -> np.ndarray:
    """Calculate the rejection uncertainties.

    Parameters
    ----------
    arr : numpy.array
        Rejection values
    n_counts : int
        Number of used statistics to calculate rejection
    norm : bool, optional
        If True, normed (relative) error is being calculated, by default False

    Returns
    -------
    numpy.array
        Rejection uncertainties

    Raises
    ------
    ValueError
        If n_counts <=0
    ValueError
        If any rejection value is 0

    Notes
    -----
    Special case of `eff_err()`
    """
    if np.any(n_counts <= 0):
        raise ValueError(f"You passed as argument `n_counts` {n_counts} but it has to be larger 0.")
    if np.any(arr == 0):
        raise ValueError("One rejection value is 0, cannot calculate error.")
    if norm:
        return np.power(arr, 2) * eff_err(1 / arr, n_counts) / arr
    return np.power(arr, 2) * eff_err(1 / arr, n_counts)


def non_zero_mask(
    sig_eff: np.ndarray,
    bkg_rej: np.ndarray,
    xmin: float = None,
    xmax: float = None,
):
    """Masking points where rejection is 0 and no signal efficiency change present.

    Returns
    -------
    numpy.array
        Masked indices
    """
    # Mask the points where there was no change in the signal eff
    delta_x = np.concatenate((np.ones(1), np.diff(sig_eff)))

    # Also mask the rejections that are 0
    nonzero = (bkg_rej != 0) & (delta_x > 0)
    if xmin is not None:
        nonzero = nonzero & (sig_eff >= xmin)
    if xmax is not None:
        nonzero = nonzero & (sig_eff <= xmax)
    return nonzero


def binomial_error(
    sig_eff: np.ndarray,
    bkg_rej: np.ndarray,
    norm: bool = False,
    n_test: int = None,
    return_nonzero_mask: bool = False,
) -> np.ndarray:
    """Calculate binomial error of roc curve.

    Parameters
    ----------
    norm : bool
        If True calulate relative error, by default False
    n_test : int
        Number of events used to calculate the background efficiencies,
        by default None

    Returns
    -------
    numpy.array
        Binomial error

    Raises
    ------
    ValueError
        If no `n_test` was provided
    """
    if n_test is None:
        raise ValueError("No `n_test` provided, cannot calculate binomial error!")
    valid = non_zero_mask(sig_eff, bkg_rej)
    if return_nonzero_mask:
        return rej_err(bkg_rej[valid], n_test, norm=norm), valid
    return rej_err(bkg_rej[valid], n_test, norm=norm)


def get_op(op):
    return {
        "<": operator.lt,
        ">": operator.gt,
        "<=": operator.le,
        ">=": operator.ge,
        "==": operator.eq,
        "!=": operator.ne,
        "+": operator.add,
        "-": operator.sub,
        "*": operator.mul,
        "/": operator.truediv,
        "and": operator.and_,
        "or": operator.or_,
    }[op]


def dump_recommended_working_points(
    y_predict,
    y_target,
    label_names,
    weights=None,
    total=None,
    save_path="recommended_working_points.json",
):
    output = {}

    # define classes
    signal_class, QCD_bkg_class, ttbar_bkg_class = label_names

    f_fract_dict = {QCD_bkg_class: 0.92}
    f_fract_dict[ttbar_bkg_class] = 1 - f_fract_dict[QCD_bkg_class]
    output["f_fract"] = {k: round(v, 2) for k, v in f_fract_dict.items()}

    clahh_discs = get_score(
        y_predict,
        class_labels=label_names,
        main_class=signal_class,
        frac_dict=f_fract_dict,
    )

    # set mask for each class
    is_HH = y_target[:, 0] == 1

    target_signal_eff_first = np.arange(0, 0.010001, 0.001)[1:-1]
    target_signal_eff_second = np.arange(0.01, 1.01, 0.01)
    target_signal_eff = np.concatenate((target_signal_eff_first, target_signal_eff_second))

    cutvalues_sig = get_discriminant_cut(
        clahh_discs[is_HH],
        weights[is_HH],
        target_signal_eff,
        bins=1000,
        total=total,
    )

    wp_dict = {
        "{:.3f}".format(eff): round(float(cut), 3)
        for eff, cut in zip(target_signal_eff, cutvalues_sig)
    }

    output["signal_eff"] = wp_dict

    # save to json
    with open(save_path, "w") as f:
        json.dump(output, f, indent=4)
