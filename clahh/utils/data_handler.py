import torch
import uproot
import vector
import numpy as np
import awkward as ak
from pathlib import Path
from tqdm import tqdm
from sklearn import preprocessing
from torch.utils.data import TensorDataset, Subset, Dataset
from typing import List
from functools import reduce
import warnings


class DummyDataGenerator:
    """
    Generate data with variable number of jets and truth labels at the event level:

    jet = [evt_no, px, py, pz, btag]
    data = [
        [jet, jet, jet, jet, label_HH, label_QCD, label_ttbar],
        [jet, jet, label_HH, label_QCD, label_ttbar],
        [jet, jet, jet, label_HH, label_QCD, label_ttbar],
    ]
    """

    def __init__(self, n_samples=10_000, seed=42) -> None:
        self.n_samples = n_samples
        self.seed = seed
        self.rng = np.random.default_rng(self.seed)
        self.data_builder = ak.ArrayBuilder()

    def _random_truncated_sample(self, low, high):
        return self.rng.random() * (high - low) + low

    def _append_event_info(self, signal=False, QCD=False, ttbar=False, evt_no=0):
        assert sum([signal, QCD, ttbar]) == 1
        signal_kin_ranges = {
            "pt": [40, 100],  # GeV
            "eta": [-2.5, 2.5],
            "phi": [-3, 3],
            "mass": [0, 10],  # GeV
        }
        QCD_kin_ranges = {
            "pt": [20, 3_900],  # GeV
            "eta": [-5, 5],
            "phi": [-3, 3],
            "mass": [0, 20],  # GeV
        }
        top_kin_ranges = {
            "pt": [20, 900],  # GeV
            "eta": [-5, 5],
            "phi": [-3, 3],
            "mass": [0, 20],  # GeV
        }
        signal_njets = self.rng.integers(4, 6)
        QCD_njets = self.rng.integers(2, 10)
        top_njets = self.rng.integers(2, 6)
        jets = {
            "px": [],
            "py": [],
            "pz": [],
            "btag": [],
            "pt": [],
            "eta": [],
            "phi": [],
            "mass": [],
        }
        njets = signal_njets if signal else QCD_njets if QCD else top_njets
        kin_ranges = signal_kin_ranges if signal else QCD_kin_ranges if QCD else top_kin_ranges
        for _ in range(njets):
            jet_p4 = vector.obj(
                pt=self._random_truncated_sample(*kin_ranges["pt"]),
                eta=self._random_truncated_sample(*kin_ranges["eta"]),
                phi=self._random_truncated_sample(*kin_ranges["phi"]),
                mass=self._random_truncated_sample(*kin_ranges["mass"]),
            )
            jets["px"].append(jet_p4.x)
            jets["py"].append(jet_p4.y)
            jets["pz"].append(jet_p4.z)
            jets["btag"].append(1 if signal else self.rng.integers(0, 1))
            jets["pt"].append(jet_p4.pt)
            jets["eta"].append(jet_p4.eta)
            jets["phi"].append(jet_p4.phi)
            jets["mass"].append(jet_p4.mass)

        with self.data_builder.record():
            self.data_builder.field("evt_no").integer(evt_no)
            self.data_builder.field("label_HH").integer(int(signal))
            self.data_builder.field("label_QCD").integer(int(QCD))
            self.data_builder.field("label_ttbar").integer(int(ttbar))
            for key, val in jets.items():
                with self.data_builder.field(key).list():
                    for v in val:
                        self.data_builder.real(v)

    def get_data(self):
        sample_idx = list(range(self.n_samples))
        self.rng.shuffle(sample_idx)
        for i in sample_idx:
            # generate signal events at 0.1 fraction of self.n_samples
            if i < self.n_samples * 0.1:
                self._append_event_info(signal=True, evt_no=i)
            # generate top events at 0.3 fraction of sample_size
            elif i < self.n_samples * 0.4:
                self._append_event_info(ttbar=True, evt_no=i)
            else:
                self._append_event_info(QCD=True, evt_no=i)
        return self.data_builder.snapshot()

    def __len__(self):
        return self.n_samples


class NormalizeDataset(Subset):
    def __init__(self, subset, transform=None):
        super().__init__(subset.dataset, subset.indices)

        self.subset = subset
        self.transform = transform

    def __getitem__(self, index):
        x_jet, x_evt, y, w = self.subset[index]
        if self.transform:
            # transform x
            x_evt_transformed = self.transform(x_evt.reshape(1, -1))
            x_evt_transformed = torch.from_numpy(x_evt_transformed.ravel()).float()
        return x_jet, x_evt_transformed, y, w

    def __len__(self):
        return len(self.subset)


class DataHandler:
    """
    Load root filenames in batches and concatenate them. Return awkward array.

    Parameters
    ----------
    jet_feature_names : list of strings
        Names of jet features to load from root files.
    event_feature_names : list of strings
        Names of event wide features to load from root files.
    label_names : list of strings
        Names of labels to load from root files.
    start_event : int
        Starting event number.
    stop_event : int
        Stopping event number.
    npad : int
        Number of jets to pad to.
    sources : list of strings
        List of root filenames to load.
    pad_with : string
        Feature to pad with.
    remove_unlabeled : boolean
        Remove unlabeled data samples.
    spectator_names : list of strings
        Names of spectators to load from root files.
    weight_names : list of strings
        Names of weights to load from root files.
    """

    def __init__(
        self,
        jet_feature_names=[],
        label_names=[],
        event_feature_names=[],
        spectator_names=[],
        weight_names=[],
        sources=[],
        start_event=0,
        stop_event=None,
        npad=0,
        pad_with=-9999,
        remove_unlabeled=True,
        rnd_seed=42,
    ):
        self.jet_feature_names = jet_feature_names
        self.event_feature_names = event_feature_names
        self.label_names = label_names
        self.start_event = start_event
        self.stop_event = stop_event
        self.npad = npad
        self.pad_with = pad_with
        self.remove_unlabeled = remove_unlabeled
        self.sources = sources
        self.spectator_names = spectator_names
        self.weight_names = weight_names
        self.class_weights = None
        self.rng = np.random.default_rng(seed=rnd_seed)
        self.datasets = []

    def load(self):
        """
        Load root filenames in batches and concatenate them. Saves awkward array.

        """
        # concatenate all the feature names into one list
        field_names = (
            self.jet_feature_names
            + self.event_feature_names
            + self.label_names
            + self.spectator_names
            + self.weight_names
        )
        sample_couts = {}
        for raw_path in tqdm(
            self.sources,
            total=len(self.sources),
            ncols=80,
            desc="Loading data",
        ):
            with uproot.open(raw_path) as root_file:
                for tree_name in root_file.keys():
                    tree = root_file[tree_name]
                    if raw_path not in sample_couts:
                        sample_couts[raw_path] = tree.num_entries
                    else:
                        sample_couts[raw_path] += tree.num_entries
                    data = tree.arrays(
                        field_names,
                        entry_start=self.start_event,
                        entry_stop=self.stop_event,
                        library="ak",
                    )
            # truncate jets at npad
            if self.npad > 0:
                for feature in self.jet_feature_names:
                    data[feature] = ak.from_regular(
                        self._pad_ak(data[feature], pad_with=self.pad_with)
                    )
            if self.remove_unlabeled:
                valid = np.sum([data[label] for label in self.label_names], axis=0) == 1
                data = data[valid]
            # remove rows if unlabeled
            if self.remove_unlabeled:
                valid = np.sum([data[label] for label in self.label_names], axis=0) == 1
                data = data[valid]
            if self.weight_names:
                # remove events with weights that are not finite
                valid = np.all([np.isfinite(data[weight]) for weight in self.weight_names], axis=0)
                data = data[valid]
                if np.sum(~valid) > 0:
                    print(f"\nNumber of events dropped due to non-finite weight: {np.sum(~valid)}")
                weights = np.transpose([data[weight] for weight in self.weight_names])
                # multiply weights together into one weight
                data["event_weight"] = np.prod(weights, axis=1)
            self.datasets.append(data)
        print("Number of samples available in each file:")
        for raw_path, count in sample_couts.items():
            print(f"{raw_path}: {count}")
        self.datasets = ak.concatenate(self.datasets, axis=0)

    def _pad_ak(self, x, pad_with):
        x = ak.pad_none(x, self.npad, axis=1, clip=True)
        return ak.fill_none(x, pad_with)

    def to_torch(self, dataset=None):
        """
        Handles conversion of sample files into torch datasets.
        """
        if dataset is None:
            dataset = self.datasets
        if len(self.jet_feature_names) == 0:
            X_JET = torch.zeros((len(dataset), 1, 1))
        else:
            X_JET = dataset[self.jet_feature_names]
            # X_JET = self._pad_ak(X_JET, pad_with=self.pad_with)
            X_JET = np.transpose([X_JET[f].to_numpy() for f in X_JET.fields], axes=(1, 0, 2))
            X_JET = torch.from_numpy(X_JET).float()
        if len(self.event_feature_names) == 0:
            X_EVENT = torch.zeros((len(dataset), 1))
        else:
            X_EVENT = dataset[self.event_feature_names]
            X_EVENT = np.transpose([X_EVENT[f].to_numpy() for f in X_EVENT.fields])
            X_EVENT = torch.from_numpy(X_EVENT).float()
        y = dataset[self.label_names]
        y = np.transpose([y[f].to_numpy() for f in y.fields])
        y = torch.from_numpy(y).long()
        w = dataset["event_weight"]
        w = torch.from_numpy(w.to_numpy()).float()
        return TensorDataset(X_JET, X_EVENT, y, w)

    def get_class_counts(self, labels: np.ndarray, weights=None):
        return np.bincount(labels.argmax(axis=1), weights=weights)

    def get_class_weights(self, labels: np.ndarray, weights=None):
        """Computes class weights using all the data.

        Parameters
        ----------
        labels : np.ndarray
            Array of labels. Shape (n_samples, n_labels). Labels are one-hot encoded.

        Returns
        -------
        class_weights : np.ndarray
            Array of class weights. Shape (n_labels,).

        """

        class_counts = self.get_class_counts(labels, weights=weights)
        class_weights = labels.shape[0] / (labels.shape[1] * class_counts)
        return class_weights

    def preprocess(self, dataset):
        """
        Preprocess data by normalizing, etc.
        """

        ########################################
        # Scale along the second dimension (jets) #
        ########################################

        # Create a scaler
        scaler = preprocessing.StandardScaler()

        X_evt = dataset.dataset[dataset.indices][1].numpy()

        scaler.fit(X_evt)

        scaled_dataset = NormalizeDataset(dataset, scaler.transform)

        return scaled_dataset

    def __len__(self):
        return len(self.datasets)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        return self.datasets[idx]

    def __iter__(self):
        return iter(self.datasets)


class Dotdict(dict):
    """dot.notation access to dictionary attributes"""

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class TensorDatasetDict(Dataset):
    def __init__(self, **tensors):
        assert all(
            tensors[next(iter(tensors))].size(0) == tensor.size(0) for tensor in tensors.values()
        ), "Size mismatch between tensors"
        self.tensors = tensors

    def __getitem__(self, index):
        return Dotdict({key: tensor[index] for key, tensor in self.tensors.items()})

    def __getattr__(self, name):
        if name in self.tensors:
            return self.tensors[name]
        raise AttributeError(f"'TensorDatasetDict' object has no attribute '{name}'")

    def __len__(self):
        return len(next(iter(self.tensors.values())))


def root_data_merger(
    sources,
    save_path: Path,
    start_event=0,
    stop_event=None,
    verbose=False,
):
    """
    Load root filenames in batches and concatenate them. Saves as parquet file.

    Parameters
    ----------
    sources : list of strings
        List of root filenames to load.
    start_event : int
        Starting event number.
    stop_event : int
        Stopping event number.
    save_path : Path
        Path to save the concatenated data.
    """

    files = [src for d_dir in sources for src in Path(d_dir).glob("*.root")]

    dataset = []
    # concatenate all the feature names into one list
    sample_couts = {}
    for raw_path in tqdm(
        files,
        total=len(files),
        ncols=80,
        desc="Loading data",
    ):
        with uproot.open(raw_path) as root_file:
            for tree_name in root_file.keys():
                tree = root_file[tree_name]
                if raw_path not in sample_couts:
                    sample_couts[raw_path] = tree.num_entries
                else:
                    sample_couts[raw_path] += tree.num_entries
                data = tree.arrays(
                    tree.keys(),
                    entry_start=start_event,
                    entry_stop=stop_event,
                    library="ak",
                )
        dataset.append(data)
    dataset = ak.concatenate(dataset, axis=0)
    # Save dataset
    ak.to_parquet(dataset, save_path)
    if verbose:
        print("Number of samples available in each file:")
        for raw_path, count in sample_couts.items():
            print(f"{raw_path}: {count}")


def load_data_from_parquet(file_paths, return_cutflow=False):
    """
    Load parquet filenames in batches and concatenate them. Merge cutflows if return_cutflow is True.

    Parameters
    ----------
    file_paths : list of strings
        List of parquet filenames to load. The paths could be directories.
    return_cutflow : bool
        If True, return cutflows.

    Returns
    -------
    dataset : ak.Array
        Concatenated dataset.
    cutflows : dict
        Dictionary of cutflows for each sample.
    """

    # go through every item in file_paths and check if it is a directory, if it is then get all the parquet files in that directory. If it is not a directory, then just keep the file path
    files = []
    for file_path in file_paths:
        if Path(file_path).is_dir():
            files.extend(list(Path(file_path).glob("*.parquet")))
        elif Path(file_path).suffix == ".parquet":
            files.append(file_path)
        else:
            warnings.warn(
                f"File {file_path} is not a parquet file or directory. Skipping...", UserWarning
            )

    dataset = []
    cutflows = {}
    for file_path in tqdm(
        files,
        total=len(files),
        ncols=80,
        desc="Loading data",
    ):
        if not file_path.exists():
            raise FileNotFoundError(f"File {file_path} not found")
        data = ak.from_parquet(file_path)
        params = ak.parameters(data)
        dataset.append(data)
        # update cutflows
        if (
            return_cutflow
            and "parameters" in params
            and "metadata" in params["parameters"]
            and "cutflow" in params["parameters"]
        ):
            label = params["parameters"]["metadata"]["label"]
            cutflow = params["parameters"]["cutflow"]
            if label in cutflows:
                cutflows[label].append(cutflow)
            else:
                cutflows[label] = [cutflow]

    dataset = ak.concatenate(dataset)
    if return_cutflow:
        for sample in cutflows:
            cutflows[sample] = reduce(
                lambda acc, it: {k: acc[k] + it[k] for k in acc}, cutflows[sample]
            )
        return dataset, cutflows
    return dataset


def get_class_counts(labels: np.ndarray, weights=None):
    return np.bincount(labels.argmax(axis=1), weights=weights)


def get_class_weights(labels: np.ndarray, weights=None):
    """Computes class weights using all the data.

    Parameters
    ----------
    labels : np.ndarray
        Array of labels. Shape (n_samples, n_labels). Labels are one-hot encoded.

    Returns
    -------
    class_weights : np.ndarray
        Array of class weights. Shape (n_labels,).

    """

    class_counts = get_class_counts(labels, weights=weights)
    class_weights = labels.shape[0] / (labels.shape[1] * class_counts)
    return class_weights


def pad(x: ak.Array, pad_end: int = 20, pad_with: float = np.nan):
    x = ak.pad_none(x, pad_end, axis=1, clip=True)
    return ak.fill_none(x, float(pad_with))


def to_tensordataset(
    dataset: ak.Array,
    jet_feature_names: List[str],
    bb_feature_names: List[str],
    event_feature_names: List[str],
    label_names: List[str],
    event_weight_name: str,
    spectator_feature_name=None,
    max_njets: int = 20,
    pad_value: float = np.nan,
):
    """
    Handles conversion of sample files into torch datasets.
    """
    if len(jet_feature_names) != 0:
        X_JET = dataset[jet_feature_names]
        X_JET = np.transpose(
            [pad(X_JET[f], pad_end=max_njets, pad_with=pad_value).to_numpy() for f in X_JET.fields],
            axes=(1, 0, 2),
        )
        X_JET = torch.from_numpy(X_JET).float()
    else:
        X_JET = torch.zeros((len(dataset), len(jet_feature_names), max_njets))

    if len(bb_feature_names) != 0:
        X_BB = dataset[bb_feature_names]
        X_BB = np.transpose(
            [X_BB[f].to_numpy() for f in X_BB.fields],
            axes=(1, 0, 2),
        )
        X_BB = torch.from_numpy(X_BB).float()
    else:
        X_BB = torch.zeros((len(dataset), len(bb_feature_names), max_njets))

    if len(event_feature_names) != 0:
        X_EVENT = dataset[event_feature_names]
        X_EVENT = np.transpose(
            np.concatenate([X_EVENT[f].to_numpy() for f in X_EVENT.fields], axis=1), axes=(0, 1)
        )
        X_EVENT = torch.from_numpy(X_EVENT).float()
    else:
        X_EVENT = torch.zeros((len(dataset), len(event_feature_names)))

    X_BB = dataset[bb_feature_names]
    X_BB = np.transpose(
        [X_BB[f].to_numpy() for f in X_BB.fields],
        axes=(1, 0, 2),
    )
    X_BB = torch.from_numpy(X_BB).float()

    if len(event_feature_names) != 0:
        X_EVENT = dataset[event_feature_names]
        X_EVENT = np.transpose(
            np.concatenate([X_EVENT[f].to_numpy() for f in X_EVENT.fields], axis=1), axes=(0, 1)
        )
        X_EVENT = torch.from_numpy(X_EVENT).float()
    else:
        X_EVENT = torch.zeros((len(dataset), len(event_feature_names)))

    y = dataset[label_names]
    y = np.transpose([y[f].to_numpy() for f in y.fields])
    y = torch.from_numpy(y).long()
    w = dataset[event_weight_name]
    w = torch.from_numpy(w.to_numpy()).float()
    idx = torch.from_numpy(np.arange(len(dataset)))
    if spectator_feature_name:
        spectator = pad(
            dataset[spectator_feature_name], pad_end=max_njets, pad_with=pad_value
        ).to_numpy()
        spectator = torch.from_numpy(spectator).float()
        return TensorDatasetDict(
            x_jet=X_JET, x_bb=X_BB, x_event=X_EVENT, y=y, weights=w, idx=idx, spectator=spectator
        )
    return TensorDatasetDict(x_jet=X_JET, x_bb=X_BB, x_event=X_EVENT, y=y, weights=w, idx=idx)


def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()


def custom_loader(weights, data, batch_size, npgen=None):
    if npgen is None:
        npgen = np.random.default_rng()
    probs = np.abs(weights)
    probs = probs / np.sum(probs)
    data_idx = np.arange(len(data))
    num_batches = len(data) // batch_size  # Calculate the number of batches

    for i in range(num_batches):
        start_idx = i * batch_size
        end_idx = start_idx + batch_size
        batch_data_idx = npgen.choice(
            data_idx[start_idx:end_idx], batch_size, replace=False, p=probs[start_idx:end_idx]
        )
        yield data[batch_data_idx]

    # Handle the last batch if it's smaller than batch_size
    if len(data) % batch_size != 0:
        start_idx = num_batches * batch_size
        batch_data_idx = npgen.choice(
            data_idx[start_idx:], len(data) % batch_size, replace=False, p=probs[start_idx:]
        )
        yield data[batch_data_idx]


def split_data(total, train_split, val_split, test_split, generator=None, return_indices=False):
    """Splits data using train, validation, and test splits. The format of train_split, val_split, and test_split is is a string with column name and operation, like '{"event_number": "%10==9"}'."""
    if not all(
        [
            t == v and v == train
            for train in train_split.keys()
            for v in val_split.keys()
            for t in test_split.keys()
        ]
    ):
        raise KeyError("All split keys must be the same")
    if generator:
        idx = np.arange(len(total))
        generator.shuffle(idx)
        total = total[idx]
    train_split_mask = eval([f"total{v}" for k, v in train_split.items()][0])
    val_split_mask = eval([f"total{v}" for k, v in val_split.items()][0])
    test_split_mask = eval([f"total{v}" for k, v in test_split.items()][0])

    if return_indices:
        inv_idx = np.arange(len(total))
        if generator:
            # create an inverse index to map back to the original data
            inv_idx[idx] = np.arange(len(idx))
        # return indices of the original data unshuffled
        train_split_idx = np.where(train_split_mask[inv_idx])[0]
        val_split_idx = np.where(val_split_mask[inv_idx])[0]
        test_split_idx = np.where(test_split_mask[inv_idx])[0]
        return (
            total[train_split_mask],
            total[val_split_mask],
            total[test_split_mask],
            [
                train_split_idx,
                val_split_idx,
                test_split_idx,
            ],
        )
    return (
        total[train_split_mask],
        total[val_split_mask],
        total[test_split_mask],
    )


def get_event_indices(event_number: np.ndarray, test_event_number: np.ndarray) -> np.ndarray:
    """
    Vectorized retrieval of indices for test_event_number in event_number.

    Parameters:
    - event_number (np.ndarray): Array of all event numbers.
    - test_event_number (np.ndarray): Array of event numbers to find.

    Returns:
    - np.ndarray: Array of indices corresponding to test_event_number in event_number.

    Raises:
    - ValueError: If any test_event_number is not found in event_number.
    """
    # Step 1: Sort event_number and keep track of the original indices
    sorted_idx = np.argsort(event_number)
    sorted_event_number = event_number[sorted_idx]

    # Step 2: Use searchsorted to find the positions of test_event_number in sorted_event_number
    positions = np.searchsorted(sorted_event_number, test_event_number)

    # Step 3: Verify that each test_event_number exists in event_number
    mask = (positions < len(event_number)) & (sorted_event_number[positions] == test_event_number)

    if not np.all(mask):
        missing_events = test_event_number[~mask]
        raise ValueError(
            f"The following test_event_number(s) were not found in event_number: {missing_events}"
        )

    # Step 4: Retrieve the original indices using the sorted_idx
    test_event_number_idx = sorted_idx[positions]

    return test_event_number_idx
