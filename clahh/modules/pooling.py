import torch
import torch.nn as nn
from torch import Tensor
from torch.nn.functional import softmax


### Very much WIP
class GlobalAttentionPooling(nn.Module):
    def __init__(self, input_size: int, output_size: int):
        super().__init__()
        self.gate_nn = nn.Linear(input_size, output_size)

    def forward(
        self,
        x: Tensor,
        pad_mask: Tensor = None,
        dim: int = -1,
    ) -> Tensor:
        weights = masked_softmax(self.gate_nn(x), pad_mask, dim=1)
        # # add padded track to avoid error in onnx model when there are no tracks in the jet
        # weight_pad = torch.zeros((weights.shape[0], 1, weights.shape[2]), device=weights.device)
        # x_pad = torch.zeros((x.shape[0], 1, x.shape[2]), device=x.device)
        # weights = torch.cat([weights, weight_pad], dim=1)
        # x = torch.cat([x, x_pad], dim=1)

        return (x * weights).sum(dim=dim)


def masked_softmax(
    x: Tensor,
    mask: Tensor = None,
    dim: int = -1,
) -> Tensor:
    if mask is not None:
        mask = add_dims(mask, x.dim())
        # mask has shape 128, 1, 20, x has shape 128, 16, 1. Broadcast mask to x
        x = x.masked_fill(mask, -torch.inf)

    x = softmax(x, dim=dim)

    if mask is not None:
        x = x.masked_fill(mask, 0)

    return x


def add_dims(x: Tensor, ndim: int):
    """Adds dimensions to a tensor to match the shape of another tensor."""
    if (dim_diff := ndim - x.dim()) < 0:
        raise ValueError(f"Target ndim ({ndim}) is smaller than input ndim ({x.dim()})")

    if dim_diff > 0:
        x = x.view(x.shape[0], *dim_diff * (1,), *x.shape[1:])

    return x
