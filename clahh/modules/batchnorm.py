import torch
import torch.nn as nn


class MaskedBatchNorm1d(nn.BatchNorm1d):
    """
    Custom BatchNorm1D that computes mean and variance over batch_size and njets dimensions,
    while handling masking to ignore certain elements during normalization.

    Args:
        num_features (int): C from an expected input of size (N, C, L)
        eps (float): A value added to the denominator for numerical stability. Default: 1e-5
        momentum (float): The value used for the running_mean and running_var computation. Default: 0.1
        affine (bool): If True, this module has learnable affine parameters. Default: True
        track_running_stats (bool): If True, this module tracks the running mean and variance, and when set to False,
    """

    def __init__(
        self, num_features, eps=1e-5, momentum=0.1, affine=True, track_running_stats=True, **kwargs
    ):
        super(MaskedBatchNorm1d, self).__init__(
            num_features,
            eps,
            momentum,
            affine,
            track_running_stats,
            **kwargs,
        )

    # def forward(self, x, mask: torch.Tensor = None):
    #     """
    #     Forward pass for MaskedBatchNorm1d.

    #     Parameters:
    #     ----------
    #     x : torch.Tensor
    #         Input tensor of shape (batch_size, num_features, njets)

    #     Returns:
    #     -------
    #     torch.Tensor
    #         Normalized tensor of the same shape as input.
    #     """
    #     if mask is None:
    #         return super(MaskedBatchNorm1d, self).forward(x)

    #     self._check_input_dim(x)

    #     # exponential_average_factor is set to self.momentum
    #     # (when it is available) only so that it gets updated
    #     # in ONNX graph when this node is exported to ONNX.
    #     if self.momentum is None:
    #         exponential_average_factor = 0.0
    #     else:
    #         exponential_average_factor = self.momentum

    #     if self.training and self.track_running_stats:
    #         if self.num_batches_tracked is not None:
    #             self.num_batches_tracked.add_(1)
    #             if self.momentum is None:  # use cumulative moving average
    #                 exponential_average_factor = 1.0 / float(self.num_batches_tracked)
    #             else:  # use exponential moving average
    #                 exponential_average_factor = self.momentum

    #     if self.training:
    #         # Count of valid entries per feature
    #         count = mask.sum(dim=[0], keepdim=False)  # Shape: (num_features,)
    #         # Avoid division by zero
    #         count = count.clamp(min=1)
    #         # convert mask to probability mask
    #         mask = mask / count

    #         # Compute mean
    #         mean = (x * mask).sum(dim=[0], keepdim=False)  # Shape: (num_features,)

    #         # Using Var(X) = E[X^2] - E[X]^2 as the biased variance estimator
    #         var = (x**2 * mask).sum(dim=[0], keepdim=False) - mean**2  # Shape: (num_features,)

    #         # Update running estimates
    #         with torch.no_grad():
    #             self.running_mean = (
    #                 1 - exponential_average_factor
    #             ) * self.running_mean + exponential_average_factor * mean.detach()
    #             self.running_var = (
    #                 1 - exponential_average_factor
    #             ) * self.running_var + exponential_average_factor * var.detach()

    #         # Increment batch count
    #         self.num_batches_tracked += 1
    #     else:
    #         mean = self.running_mean
    #         var = self.running_var

    #     # Normalize
    #     x_normalized = (x - mean) / torch.sqrt(var + self.eps)

    #     if self.affine:
    #         # Reshape weight and bias for broadcasting
    #         weight = self.weight
    #         bias = self.bias
    #         x_normalized = x_normalized * weight + bias

    #     return x_normalized

    def forward(self, x, mask: torch.Tensor = None):
        """
        Forward pass for MaskedBatchNorm1d.

        Parameters:
        ----------
        x : torch.Tensor
            Input tensor of shape (batch_size, num_features, njets)

        Returns:
        -------
        torch.Tensor
            Normalized tensor of the same shape as input.
        """
        if mask is None:
            return super(MaskedBatchNorm1d, self).forward(x)

        self._check_input_dim(x)

        # exponential_average_factor is set to self.momentum
        # (when it is available) only so that it gets updated
        # in ONNX graph when this node is exported to ONNX.
        if self.momentum is None:
            exponential_average_factor = 0.0
        else:
            exponential_average_factor = self.momentum

        if self.training and self.track_running_stats:
            if self.num_batches_tracked is not None:
                self.num_batches_tracked.add_(1)
                if self.momentum is None:  # use cumulative moving average
                    exponential_average_factor = 1.0 / float(self.num_batches_tracked)
                else:  # use exponential moving average
                    exponential_average_factor = self.momentum

        if self.training:
            # Count of valid entries per feature
            count = mask.sum(dim=[0, 2], keepdim=False)  # Shape: (num_features,)
            # Avoid division by zero
            count = count.clamp(min=1)
            # convert mask to probability mask
            mask = mask / count.view(1, -1, 1)

            # Compute mean
            mean = (x * mask).sum(dim=[0, 2], keepdim=False)  # Shape: (num_features,)

            # Using Var(X) = E[X^2] - E[X]^2 as the biased variance estimator
            var = (x**2 * mask).sum(dim=[0, 2], keepdim=False) - mean**2  # Shape: (num_features,)

            # Update running estimates
            with torch.no_grad():
                self.running_mean = (
                    1 - exponential_average_factor
                ) * self.running_mean + exponential_average_factor * mean.detach()
                self.running_var = (
                    1 - exponential_average_factor
                ) * self.running_var + exponential_average_factor * var.detach()

            # Increment batch count
            self.num_batches_tracked += 1
        else:
            mean = self.running_mean
            var = self.running_var

        # Reshape mean and var for broadcasting
        mean = mean.view(1, -1, 1)
        var = var.view(1, -1, 1)

        # Normalize
        x_normalized = (x - mean) / torch.sqrt(var + self.eps)

        if self.affine:
            # Reshape weight and bias for broadcasting
            weight = self.weight.view(1, -1, 1)
            bias = self.bias.view(1, -1, 1)
            x_normalized = x_normalized * weight + bias

        return x_normalized
