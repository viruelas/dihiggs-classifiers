import h5py
import numpy as np
from pathlib import Path
from puma import Roc, RocPlot
from ftag import Flavour
import mplhep as hplt
from clahh.utils.metrics import get_score, get_op
from clahh.utils.hist import Histogram
from clahh.utils.metrics import get_discriminant_cut
import matplotlib.pyplot as plt
import pyhf

plt.style.use(hplt.style.ATLAS)


def create_yields_benchmarks(
    hh4b_dataset,
    top_veto: dict = {"operator": ">", "value": 1.5},
    hh_deltaeta_veto: dict = {"operator": "<", "value": 1.5},
    hh_mass_veto: dict = {"operator": "<", "value": 1.6},
    hist_edges: np.ndarray = None,
    cutflow: dict = None,
) -> dict:
    """Create benchmarks for Run 2."""
    output = {}

    HH_total = None
    QCD_total = None
    ttbar_total = None
    if cutflow is not None:
        HH_total = cutflow["label_HH"]["initial_events_weighted"]
        QCD_total = cutflow["label_QCD"]["initial_events_weighted"]
        ttbar_total = cutflow["label_ttbar"]["initial_events_weighted"]

    ########################################
    # define the baseline discriminants
    ########################################
    if hist_edges is None:
        infar = np.array([np.inf])
        hist_edges = np.concatenate([-infar, np.linspace(-20, 20, 1000), infar])

    event_weight = hh4b_dataset["event_weight"].to_numpy()

    HH_mask = (hh4b_dataset["label_HH"] == 1).to_numpy()
    QCD_mask = (hh4b_dataset["label_QCD"] == 1).to_numpy()
    ttbar_mask = (hh4b_dataset["label_ttbar"] == 1).to_numpy()

    X_Wt_disc = hh4b_dataset["X_Wt"].to_numpy()
    X_Wt_mask = get_op(top_veto["operator"])(X_Wt_disc, top_veto["value"])
    Delta_eta_HH_disc = hh4b_dataset["dEta_HH"].to_numpy()
    Delta_eta_HH_mask = get_op(hh_deltaeta_veto["operator"])(
        Delta_eta_HH_disc, hh_deltaeta_veto["value"]
    )
    X_HH_disc = hh4b_dataset["X_HH"].to_numpy()
    X_HH_mask = get_op(hh_mass_veto["operator"])(X_HH_disc, hh_mass_veto["value"])

    top_eta_mask = X_Wt_mask & Delta_eta_HH_mask
    x_eta_mask = X_HH_mask & Delta_eta_HH_mask
    x_top_mask = X_HH_mask & X_Wt_mask

    #############################
    # define the X_Wt discriminant
    #############################
    disc_X_Wt = X_Wt_disc
    X_Wt_disc_hists = [
        np.histogram(
            disc_X_Wt[class_mask & x_eta_mask],
            hist_edges,
            weights=event_weight[class_mask & x_eta_mask],
        )[0]
        for class_mask in [HH_mask, QCD_mask, ttbar_mask]
    ]
    X_Wt_HH_eff = X_Wt_disc_hists[0][::-1].cumsum() / (
        X_Wt_disc_hists[0].sum() if HH_total is None else HH_total
    )
    X_Wt_QCD_eff = X_Wt_disc_hists[1][::-1].cumsum() / (
        X_Wt_disc_hists[1].sum() if QCD_total is None else QCD_total
    )
    X_Wt_ttbar_eff = X_Wt_disc_hists[2][::-1].cumsum() / (
        X_Wt_disc_hists[2].sum() if ttbar_total is None else ttbar_total
    )
    valid = (X_Wt_HH_eff > 0) & (X_Wt_QCD_eff > 0) & (X_Wt_ttbar_eff > 0)
    X_Wt_QCD_eff = X_Wt_QCD_eff[valid]
    X_Wt_ttbar_eff = X_Wt_ttbar_eff[valid]
    X_Wt_HH_eff = X_Wt_HH_eff[valid]

    output["X_Wt"] = {
        "HH": X_Wt_HH_eff,
        "QCD": X_Wt_QCD_eff,
        "ttbar": X_Wt_ttbar_eff,
    }

    # #############################
    # # define the Delta eta discriminant
    # #############################
    disc_Delta_eta_HH = (
        -Delta_eta_HH_disc
    )  # multiply disc_Delta_eta_HH by a negative sign to make it a rejection
    Delta_eta_HH_disc_hists = [
        np.histogram(
            disc_Delta_eta_HH[class_mask & x_top_mask],
            hist_edges,
            weights=event_weight[class_mask & x_top_mask],
        )[0]
        for class_mask in [HH_mask, QCD_mask, ttbar_mask]
    ]
    Delta_eta_HH_HH_eff = Delta_eta_HH_disc_hists[0][::-1].cumsum() / (
        Delta_eta_HH_disc_hists[0].sum() if HH_total is None else HH_total
    )
    Delta_eta_HH_QCD_eff = Delta_eta_HH_disc_hists[1][::-1].cumsum() / (
        Delta_eta_HH_disc_hists[1].sum() if QCD_total is None else QCD_total
    )
    Delta_eta_HH_ttbar_eff = Delta_eta_HH_disc_hists[2][::-1].cumsum() / (
        Delta_eta_HH_disc_hists[2].sum() if ttbar_total is None else ttbar_total
    )
    valid = (Delta_eta_HH_HH_eff > 0) & (Delta_eta_HH_QCD_eff > 0) & (Delta_eta_HH_ttbar_eff > 0)
    Delta_eta_HH_QCD_eff = Delta_eta_HH_QCD_eff[valid]
    Delta_eta_HH_ttbar_eff = Delta_eta_HH_ttbar_eff[valid]
    Delta_eta_HH_HH_eff = Delta_eta_HH_HH_eff[valid]

    output["HH_dEta"] = {
        "HH": Delta_eta_HH_HH_eff,
        "QCD": Delta_eta_HH_QCD_eff,
        "ttbar": Delta_eta_HH_ttbar_eff,
    }

    #############################
    # define the X_HH discriminant
    #############################
    disc_X_HH = -X_HH_disc  # multiply disc_X_HH by a negative sign to make it a rejection
    X_HH_disc_hists = [
        np.histogram(
            disc_X_HH[class_mask & top_eta_mask],
            hist_edges,
            weights=event_weight[class_mask & top_eta_mask],
        )[0]
        for class_mask in [HH_mask, QCD_mask, ttbar_mask]
    ]
    X_HH_HH_eff = X_HH_disc_hists[0][::-1].cumsum() / (
        X_HH_disc_hists[0].sum() if HH_total is None else HH_total
    )
    X_HH_QCD_eff = X_HH_disc_hists[1][::-1].cumsum() / (
        X_HH_disc_hists[1].sum() if QCD_total is None else QCD_total
    )
    X_HH_ttbar_eff = X_HH_disc_hists[2][::-1].cumsum() / (
        X_HH_disc_hists[2].sum() if ttbar_total is None else ttbar_total
    )
    valid = (X_HH_HH_eff > 0) & (X_HH_QCD_eff > 0) & (X_HH_ttbar_eff > 0)
    X_HH_QCD_eff = X_HH_QCD_eff[valid]
    X_HH_ttbar_eff = X_HH_ttbar_eff[valid]
    X_HH_HH_eff = X_HH_HH_eff[valid]

    output["X_HH"] = {
        "HH": X_HH_HH_eff,
        "QCD": X_HH_QCD_eff,
        "ttbar": X_HH_ttbar_eff,
    }

    #############################
    # calculate total efficiencies
    #############################
    signal_event_mask = X_Wt_mask & Delta_eta_HH_mask & X_HH_mask
    # total eff and rejections
    sig_total_eff = event_weight[HH_mask & signal_event_mask].sum() / (
        event_weight[HH_mask].sum() if HH_total is None else HH_total
    )
    qcd_total_eff = event_weight[QCD_mask & signal_event_mask].sum() / (
        event_weight[QCD_mask].sum() if QCD_total is None else QCD_total
    )
    ttbar_total_eff = event_weight[ttbar_mask & signal_event_mask].sum() / (
        event_weight[ttbar_mask].sum() if ttbar_total is None else ttbar_total
    )
    output["total_eff"] = sig_total_eff
    output["total_qcd_eff"] = qcd_total_eff
    output["total_ttbar_eff"] = ttbar_total_eff
    output["HH_SR"] = event_weight[HH_mask & signal_event_mask].sum()
    output["QCD_SR"] = event_weight[QCD_mask & signal_event_mask].sum()
    output["ttbar_SR"] = event_weight[ttbar_mask & signal_event_mask].sum()

    return output


def create_benchmark_rocs(
    y_predict,
    y_target,
    weights,
    label_names,
    test_dataset,
    plot_path,
    top_veto: dict = {"operator": ">", "value": 1.5},
    hh_deltaeta_veto: dict = {"operator": "<", "value": 1.5},
    hh_mass_veto: dict = {"operator": "<", "value": 1.6},
    model_label: str = "CLAHH",
    luminosity: float = None,
    x_min: float = 0.02,
    x_max: float = 0.07,
    y_max: float = 10e9,
    y_min: float = 10e3,
    cutflow: dict = None,
    nominal_points: list = None,
    mus: dict = None,
    sample_campaign: str = "MC20",
    plot_random: bool = False,
) -> None:
    """Create ROC plots for the benchmarks.

    Args:
        y_predict: The model predictions.
        y_target: The target labels.
        weights: The event weights.
        label_names: The label names.
        test_dataset: The test dataset.
        plot_path: The path to save the plots.
        top_veto: The top veto.
        hh_deltaeta_veto: The hh delta eta veto.
        hh_mass_veto: The hh mass veto.
        model_label: The model label.
        luminosity: The luminosity.
        x_min: The minimum x value.
        x_max: The maximum x value.
        cutflow: The cutflow.
        nominal_points: The nominal points. List of dicts each should have keys: 'label', 'nominal_eff', 'nominal_qcd_eff', 'nominal_tt_eff'.

    Returns:
        None
    """

    # create the nominal benchmarks yields
    infar = np.array([np.inf])
    edges = np.concatenate([-infar, np.linspace(-20, 20, 1000), infar])
    benchmarks = create_yields_benchmarks(
        test_dataset,
        top_veto=top_veto,
        hh_deltaeta_veto=hh_deltaeta_veto,
        hh_mass_veto=hh_mass_veto,
        hist_edges=edges,
        cutflow=cutflow,
    )

    sig_total_eff = benchmarks["total_eff"]
    qcd_total_eff = benchmarks["total_qcd_eff"]
    ttbar_total_eff = benchmarks["total_ttbar_eff"]

    nominal_significance = benchmarks["HH_SR"] / np.sqrt(
        benchmarks["QCD_SR"] + benchmarks["ttbar_SR"]
    )

    # get total yields
    HH_total = None
    QCD_total = None
    ttbar_total = None
    if cutflow is not None:
        HH_total = cutflow["label_HH"]["initial_events_weighted"]
        QCD_total = cutflow["label_QCD"]["initial_events_weighted"]
        ttbar_total = cutflow["label_ttbar"]["initial_events_weighted"]

    # define classes
    signal_class = Flavour(name=label_names[0], label="HH", cuts=None, colour="blue", category=None)
    QCD_bkg_class = Flavour(label_names[1], "Multijet", None, "green", None)
    ttbar_bkg_class = Flavour(label_names[2], "ttbar", None, "orange", None)
    fract_dict = {QCD_bkg_class.name: 0.92}
    fract_dict[ttbar_bkg_class.name] = 1 - fract_dict[QCD_bkg_class.name]

    clahh_discs = get_score(
        y_predict,
        class_labels=label_names,
        main_class=signal_class.name,
        frac_dict=fract_dict,
    )

    # set mask for each class
    is_HH = y_target[:, 0] == 1
    is_QCD = y_target[:, 1] == 1
    is_ttbar = y_target[:, 2] == 1

    clahh_disc_hists = [
        np.histogram(clahh_discs[class_mask], edges, weights=weights[class_mask])[0]
        for class_mask in [is_HH, is_QCD, is_ttbar]
    ]
    clahh_HH_eff = clahh_disc_hists[0][::-1].cumsum() / (
        clahh_disc_hists[0].sum() if HH_total is None else HH_total
    )
    clahh_QCD_eff = clahh_disc_hists[1][::-1].cumsum() / (
        clahh_disc_hists[1].sum() if QCD_total is None else QCD_total
    )
    clahh_ttbar_eff = clahh_disc_hists[2][::-1].cumsum() / (
        clahh_disc_hists[2].sum() if ttbar_total is None else ttbar_total
    )
    valid = (clahh_HH_eff > 0) & (clahh_QCD_eff > 0) & (clahh_ttbar_eff > 0)
    clahh_HH_eff = clahh_HH_eff[valid]
    clahh_QCD_eff = clahh_QCD_eff[valid]
    clahh_ttbar_eff = clahh_ttbar_eff[valid]
    clahh_QCD_rej = 1 / clahh_QCD_eff
    clahh_ttbar_rej = 1 / clahh_ttbar_eff

    disc_cut = get_discriminant_cut(
        clahh_discs[is_HH],
        weights[is_HH],
        sig_total_eff,
        bins=1000,
        total=HH_total,
    )
    clahh_signal_mask = clahh_discs > disc_cut
    clahh_significance = weights[is_HH & clahh_signal_mask].sum() / np.sqrt(
        weights[is_QCD & clahh_signal_mask].sum() + weights[is_ttbar & clahh_signal_mask].sum()
    )

    ########################################
    # define the baseline discriminants
    ########################################

    X_Wt_HH_eff = benchmarks["X_Wt"]["HH"]
    X_Wt_QCD_eff = benchmarks["X_Wt"]["QCD"]
    X_Wt_ttbar_eff = benchmarks["X_Wt"]["ttbar"]

    Delta_eta_HH_HH_eff = benchmarks["HH_dEta"]["HH"]
    Delta_eta_HH_QCD_eff = benchmarks["HH_dEta"]["QCD"]
    Delta_eta_HH_ttbar_eff = benchmarks["HH_dEta"]["ttbar"]

    X_HH_HH_eff = benchmarks["X_HH"]["HH"]
    X_HH_QCD_eff = benchmarks["X_HH"]["QCD"]
    X_HH_ttbar_eff = benchmarks["X_HH"]["ttbar"]

    X_Wt_QCD_rej = 1 / X_Wt_QCD_eff
    X_Wt_ttbar_rej = 1 / X_Wt_ttbar_eff

    Delta_eta_HH_QCD_rej = 1 / Delta_eta_HH_QCD_eff
    Delta_eta_HH_ttbar_rej = 1 / Delta_eta_HH_ttbar_eff

    X_HH_QCD_rej = 1 / X_HH_QCD_eff
    X_HH_ttbar_rej = 1 / X_HH_ttbar_eff

    qcd_total_rej = 1 / qcd_total_eff
    ttbar_total_rej = 1 / ttbar_total_eff

    #############################
    # build random curve
    #############################
    random_eff = np.linspace(x_min, x_max, 100)
    random_rej = 1 / (random_eff)
    valid = random_eff > 0
    random_eff = random_eff[valid]
    random_rej = random_rej[valid]

    # define common labels
    com_label = f"$\sqrt{{s}}={13.6 if sample_campaign.lower() == 'mc23' else 13}$ TeV, ${luminosity} \mathrm{{fb}}^{{-1}}$ ({sample_campaign})"
    f_fract_label = "$f_{\\mathrm{Multijet}}=0.92$"
    get_jet_selections_label = (
        lambda pt, eta: rf"$\geq$ 4 jets $(p_{{\mathrm{{T}}}} > {pt}$ GeV, $|\eta| < {eta})$"
    )
    get_btagging_label = lambda ntags, wp: rf"$\geq$ {ntags} b-tags ({wp})"
    gn2v01_label = get_btagging_label(4, "GN2v01@77%")
    nominal_selections = (
        f"$\mathrm{{Nominal}}_\mathrm{{sel}}=$ trigs, {get_jet_selections_label(40, 2.5)}"
    )
    clahh_selections = (
        f"$\mathrm{{CLAHH}}_\mathrm{{sel}}=$ trigs, {get_jet_selections_label(20, 2.5)}"
    )
    x_hh_eta_hh_selections_label = "$X_{HH}$ ($X_{Wt} > 1.5$ & $|\\Delta\\eta_{HH}| < 1.5$)"
    eta_hh_x_wt_selections_label = "$|\\Delta\\eta_{HH}|$ ($X_{Wt} > 1.5$ & $X_{HH} < 1.6$)"
    x_wt_x_hh_selections_label = "$X_{Wt}$ ($|\\Delta\\eta_{HH}| < 1.5$ & $X_{HH} < 1.6$)"

    ########################################
    # create ROC plot for QCD rejection
    ########################################
    plot_sig_vs_QCD_roc = RocPlot(
        ylabel=QCD_bkg_class.rej_str,
        xlabel=signal_class.eff_str + " [%]",
        n_ratio_panels=1,
        atlas_first_tag="Simulation Private Work",
        atlas_second_tag="\n".join([com_label, f_fract_label, ""])
        + "\n".join(
            [
                f"{nominal_selections},",
                gn2v01_label,
                f"{clahh_selections},",
                gn2v01_label,
            ]
        ),
        atlas_fontsize=8,
        figsize=(6.5, 6),
        xmin=x_min,
        xmax=x_max,
        ymax=y_max,
        ymin=y_min,
    )
    plot_sig_vs_QCD_roc.add_roc(
        Roc(
            X_HH_HH_eff,
            X_HH_QCD_rej,
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            label=x_hh_eta_hh_selections_label,
            key="X_HH",
        )
    )
    plot_sig_vs_QCD_roc.add_roc(
        Roc(
            Delta_eta_HH_HH_eff,
            Delta_eta_HH_QCD_rej,
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            label=eta_hh_x_wt_selections_label,
            key="Delta_eta_HH",
        )
    )
    plot_sig_vs_QCD_roc.add_roc(
        Roc(
            X_Wt_HH_eff,
            X_Wt_QCD_rej,
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            label=x_wt_x_hh_selections_label,
            key="X_Wt",
        )
    )
    plot_sig_vs_QCD_roc.add_roc(
        Roc(
            clahh_HH_eff,
            clahh_QCD_rej,
            rej_class=QCD_bkg_class,
            signal_class=signal_class,
            # label=clahh_selections_label + (f", $\\mu={mus['CLAHH']}$" if mus is not None else ""),
            label=model_label + (f", $S/\\sqrt{{B}}={clahh_significance:.2g}$"),
            key=model_label,
        ),
        reference=True,
    )
    if plot_random:
        plot_sig_vs_QCD_roc.add_roc(
            Roc(
                random_eff,
                random_rej,
                label="Random",
                key="Random",
            )
        )
    plot_sig_vs_QCD_roc.set_ratio_class(1, QCD_bkg_class)
    plot_sig_vs_QCD_roc.ratio_axes[0].set_ylim(0, 1.5)
    plt_handles_QCD = plot_sig_vs_QCD_roc.plot_roc()
    plot_sig_vs_QCD_roc.draw()
    plot_sig_vs_QCD_roc.axis_top.get_legend().remove()
    total_eff_plot = plot_sig_vs_QCD_roc.axis_top.plot(
        sig_total_eff,
        qcd_total_rej,
        "*",
        color="tab:pink",
        # label="Nominal analysis (R24)" + (f", $\\mu={mus['Nominal']}$" if mus is not None else ""),
        label="Nominal ana (R24)" + (f", $S/\\sqrt{{B}}={nominal_significance:.2g}$"),
    )
    plt_handles_QCD += total_eff_plot
    if nominal_points is not None:
        for i, pt in enumerate(nominal_points):
            # Plot the marker for QCD rejection using the provided nominal efficiencies
            neff = pt["nominal_eff"]
            nqcd_eff = pt["nominal_qcd_eff"]
            nominal_eff_plot = plot_sig_vs_QCD_roc.axis_top.plot(
                neff,
                1 / nqcd_eff,
                "*",
                color=f"C{i}",
                label=pt["label"],
            )
            plt_handles_QCD += nominal_eff_plot
    plot_sig_vs_QCD_roc.make_legend(handles=plt_handles_QCD, ax_mpl=plot_sig_vs_QCD_roc.axis_top)
    plot_sig_vs_QCD_roc.axis_top.xaxis.set_major_formatter(
        plt.FuncFormatter(lambda x, _: f"{x * 100:.2f}")
    )
    plot_sig_vs_QCD_roc.savefig(
        plot_path / "roc_sig_vs_QCD_weighted_conditional.png", transparent=False
    )

    ########################################
    # create ROC plot for ttbar rejection
    ########################################
    plot_sig_vs_ttbar_roc = RocPlot(
        ylabel=ttbar_bkg_class.rej_str,
        xlabel=signal_class.eff_str + " [%]",
        n_ratio_panels=1,
        atlas_first_tag="Simulation Private Work",
        atlas_second_tag="\n".join([com_label, f_fract_label, ""])
        + "\n".join(
            [
                f"{nominal_selections},",
                gn2v01_label,
                f"{clahh_selections},",
                gn2v01_label,
            ]
        ),
        atlas_fontsize=8,
        figsize=(6.5, 6),
        xmin=x_min,
        xmax=x_max,
        ymax=y_max,
        ymin=y_min,
    )
    plot_sig_vs_ttbar_roc.add_roc(
        Roc(
            X_HH_HH_eff,
            X_HH_ttbar_rej,
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            label=x_hh_eta_hh_selections_label,
            key="X_HH",
        )
    )
    plot_sig_vs_ttbar_roc.add_roc(
        Roc(
            Delta_eta_HH_HH_eff,
            Delta_eta_HH_ttbar_rej,
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            label=eta_hh_x_wt_selections_label,
            key="Delta_eta_HH",
        )
    )
    plot_sig_vs_ttbar_roc.add_roc(
        Roc(
            X_Wt_HH_eff,
            X_Wt_ttbar_rej,
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            label=x_wt_x_hh_selections_label,
            key="X_Wt",
        )
    )
    plot_sig_vs_ttbar_roc.add_roc(
        Roc(
            clahh_HH_eff,
            clahh_ttbar_rej,
            rej_class=ttbar_bkg_class,
            signal_class=signal_class,
            # label=clahh_selections_label + (f", $\\mu={mus['CLAHH']}$" if mus is not None else ""),
            label=model_label + (f", $S/\\sqrt{{B}}={clahh_significance:.2g}$"),
            key=model_label,
        ),
        reference=True,
    )
    if plot_random:
        plot_sig_vs_ttbar_roc.add_roc(
            Roc(
                random_eff,
                random_rej,
                label="Random",
                key="Random",
            )
        )
    plot_sig_vs_ttbar_roc.set_ratio_class(1, ttbar_bkg_class)
    plot_sig_vs_ttbar_roc.ratio_axes[0].set_ylim(0, 1.5)
    plt_handles_ttbar = plot_sig_vs_ttbar_roc.plot_roc()
    plot_sig_vs_ttbar_roc.draw()
    plot_sig_vs_ttbar_roc.axis_top.get_legend().remove()
    total_eff_plot = plot_sig_vs_ttbar_roc.axis_top.plot(
        sig_total_eff,
        ttbar_total_rej,
        "*",
        color="tab:pink",
        # label=clahh_selections_label + (f", $\\mu={mus['Nominal']}$" if mus is not None else ""),
        label="Nominal ana (R24)" + (f", $S/\\sqrt{{B}}={nominal_significance:.2g}$"),
    )
    plt_handles_ttbar += total_eff_plot
    if nominal_points is not None:
        for i, pt in enumerate(nominal_points):
            neff = pt["nominal_eff"]
            ntt_eff = pt["nominal_tt_eff"]
            nominal_eff_plot = plot_sig_vs_ttbar_roc.axis_top.plot(
                neff,
                1 / ntt_eff,
                "*",
                color=f"C{i}",
                label=pt["label"],
            )
            plt_handles_ttbar += nominal_eff_plot
    plot_sig_vs_ttbar_roc.make_legend(
        handles=plt_handles_ttbar, ax_mpl=plot_sig_vs_ttbar_roc.axis_top
    )
    plot_sig_vs_ttbar_roc.axis_top.xaxis.set_major_formatter(
        plt.FuncFormatter(lambda x, _: f"{x * 100:.2f}")
    )
    plot_sig_vs_ttbar_roc.savefig(
        plot_path / "roc_sig_vs_ttbar_weighted_conditional.png", transparent=False
    )


# def dynamic_binning(data, weights=None, threshold=0.05):
#     """
#     Creates dynamic bins such that the relative error in each bin is less than the specified threshold.

#     For weighted events, the relative error is defined as:
#         relative_error = sqrt(sum(w^2)) / sum(w)
#     which is equivalent to 1/sqrt(n_eff), where:
#         n_eff = (sum(w))^2 / sum(w^2)

#     Parameters
#     ----------
#     data : array-like
#         Data points to be binned.
#     weights : array-like, optional
#         Weights associated with each data point. If None, each event is assigned a weight of 1.
#     threshold : float, optional
#         Maximum allowed relative error per bin (default is 0.05).

#     Returns
#     -------
#     bin_edges : list
#         List of bin edges defining the dynamic bins.
#         If the effective number of events in the entire dataset is less than 1/threshold^2,
#         returns [min(data), max(data)].
#     """
#     data = np.array(data)
#     n = len(data)

#     # Assign a weight of 1 if none are provided.
#     if weights is None:
#         weights = np.ones(n)
#     else:
#         weights = np.array(weights)
#         if len(weights) != n:
#             raise ValueError("Data and weights must have the same length.")

#     # Sort data and weights by the data values.
#     sort_idx = np.argsort(data)
#     data_sorted = data[sort_idx]
#     weights_sorted = weights[sort_idx]

#     # Compute the effective count for the entire dataset.
#     total_w = weights_sorted.sum()
#     total_w2 = np.sum(weights_sorted**2)
#     n_eff_total = (total_w**2) / total_w2 if total_w2 > 0 else 0

#     target_eff = 1 / threshold**2  # effective events required in each bin.

#     # If the overall effective count is too low, return [min, max].
#     if n_eff_total < target_eff:
#         return [data_sorted[0], data_sorted[-1]]

#     bin_edges = [data_sorted[0]]
#     cumulative_w = 0.0
#     cumulative_w2 = 0.0

#     # Accumulate events until effective count reaches the target.
#     for x, w in zip(data_sorted, weights_sorted):
#         cumulative_w += w
#         cumulative_w2 += w**2
#         n_eff = (cumulative_w**2) / cumulative_w2 if cumulative_w2 > 0 else 0

#         if n_eff >= target_eff:
#             bin_edges.append(x)
#             cumulative_w = 0.0
#             cumulative_w2 = 0.0

#     # If no bin edge was created or the last bin is incomplete, ensure full coverage.
#     if len(bin_edges) == 1:
#         bin_edges = [data_sorted[0], data_sorted[-1]]
#     elif bin_edges[-1] != data_sorted[-1]:
#         bin_edges[-1] = data_sorted[-1]

#     return bin_edges


def dynamic_binning(data, weights=None, min_weight=5):
    """
    Creates dynamic bins such that each bin has a cumulative weight of at least min_weight.

    Parameters
    ----------
    data : array-like
        Data points to be binned.
    weights : array-like, optional
        Weights associated with each data point. If None, each event is assumed to have weight 1.
    min_weight : float, optional
        Minimum cumulative weight required per bin (default is 5).

    Returns
    -------
    bin_edges : list
        List of bin edges that partition the data.
        If the total weight is less than min_weight, returns [min(data), max(data)].
    """
    data = np.array(data)
    n = len(data)

    # If no weights are provided, assign a weight of 1 to each data point.
    if weights is None:
        weights = np.ones(n)
    else:
        weights = np.array(weights)
        if len(weights) != n:
            raise ValueError("Data and weights must have the same length.")

    # Sort data and corresponding weights in ascending order of data values.
    sort_idx = np.argsort(data)
    data_sorted = data[sort_idx]
    weights_sorted = weights[sort_idx]

    bin_edges = [data_sorted[0]]
    cumulative_weight = 0.0

    # Loop through the sorted data, accumulating weights.
    for x, w in zip(data_sorted, weights_sorted):
        cumulative_weight += w
        if cumulative_weight >= min_weight:
            bin_edges.append(x)
            cumulative_weight = 0.0  # reset for the next bin

    # If no bin separation occurred (i.e. the total weight is below min_weight),
    # ensure the output contains both the lower and upper bounds.
    if len(bin_edges) == 1:
        bin_edges = [data_sorted[0], data_sorted[-1]]
    # Otherwise, if the last bin edge is not the maximum value, append the maximum.
    elif bin_edges[-1] != data_sorted[-1]:
        bin_edges.append(data_sorted[-1])

    return bin_edges


# def dynamic_binning(data, weights=None, min_weight=5):
#     return [data.min(), data.max()]


def create_fitting_hists(
    y_predict,
    y_target,
    weights,
    label_names,
    test_dataset,
    target_signal_eff: float,
    plot_path: Path,
    top_veto: dict = {"operator": ">", "value": 1.5},
    hh_deltaeta_veto: dict = {"operator": "<", "value": 1.5},
    hh_mass_veto: dict = {"operator": "<", "value": 1.6},
    vars={"clahh": "hh_mass", "nominal": "hh_mass"},
    cutflow: dict = None,
    return_hists: bool = False,
):
    """Create histograms using the given WP for the discriminant."""

    HH_total = None
    if cutflow is not None:
        HH_total = cutflow["label_HH"]["initial_events_weighted"]

    # set mask for each class
    is_HH = y_target[:, 0] == 1
    is_QCD = y_target[:, 1] == 1
    is_ttbar = y_target[:, 2] == 1

    print("Number of signal events:", is_HH.sum())
    print("Number of QCD events:", is_QCD.sum())
    print("Number of ttbar events:", is_ttbar.sum())

    m_4b = test_dataset["m_4b"].to_numpy().flatten()

    ########################################
    # define the CLAHH discriminant
    ########################################
    signal_class, QCD_bkg_class, ttbar_bkg_class = label_names
    fract_dict = {QCD_bkg_class: 0.92}
    fract_dict[ttbar_bkg_class] = 1 - fract_dict[QCD_bkg_class]

    clahh_discs = get_score(
        y_predict,
        class_labels=label_names,
        main_class=signal_class,
        frac_dict=fract_dict,
    )

    disc_cut = get_discriminant_cut(
        clahh_discs[is_HH],
        weights[is_HH],
        target_signal_eff,
        bins=1000,
        total=HH_total,
    )
    print(f"Cut value for {target_signal_eff:.3f} signal efficiency: {disc_cut:.3f}")

    signal_clahh_mask = clahh_discs > disc_cut

    print("Number of ggF raw events in signal region (CLAHH): ", (is_HH & signal_clahh_mask).sum())

    clahh_values_map = {
        "hh_mass": m_4b,
        "discrim": clahh_discs,
    }
    clahh_values = clahh_values_map[vars["clahh"]]
    clahh_postfix = f"clahh_analysis"

    # Make histograms
    clahh_optimized_edges = dynamic_binning(
        clahh_values[is_HH & signal_clahh_mask], weights=weights[is_HH & signal_clahh_mask]
    )
    HH_clahh_hist = Histogram(f"no_sys_{clahh_postfix}", clahh_optimized_edges)
    QCD_clahh_hist = Histogram(f"no_sys_{clahh_postfix}", clahh_optimized_edges)
    ttbar_clahh_hist = Histogram(f"no_sys_{clahh_postfix}", clahh_optimized_edges)
    # Fill histograms
    HH_clahh_hist.fill(
        clahh_values[is_HH & signal_clahh_mask],
        weights=weights[is_HH & signal_clahh_mask],
    )
    QCD_clahh_hist.fill(
        clahh_values[is_QCD & signal_clahh_mask],
        weights=weights[is_QCD & signal_clahh_mask],
    )
    ttbar_clahh_hist.fill(
        clahh_values[is_ttbar & signal_clahh_mask],
        weights=weights[is_ttbar & signal_clahh_mask],
    )

    ########################################
    # define the baseline discriminants
    ########################################
    X_Wt_disc = test_dataset["X_Wt"].to_numpy()
    X_Wt_mask = get_op(top_veto["operator"])(X_Wt_disc, top_veto["value"])
    Delta_eta_HH_disc = test_dataset["dEta_HH"].to_numpy()
    Delta_eta_HH_mask = get_op(hh_deltaeta_veto["operator"])(
        Delta_eta_HH_disc, hh_deltaeta_veto["value"]
    )
    X_HH_disc = test_dataset["X_HH"].to_numpy()
    X_HH_mask = get_op(hh_mass_veto["operator"])(X_HH_disc, hh_mass_veto["value"])
    signal_nominal_mask = X_Wt_mask & Delta_eta_HH_mask & X_HH_mask

    print(
        "Number of ggF raw events in signal region (nominal): ", (is_HH & signal_nominal_mask).sum()
    )

    nominal_values_map = {
        "hh_mass": m_4b,
    }
    nominal_values = nominal_values_map[vars["nominal"]]
    nominal_postfix = f"nominal_analysis"

    # Make histograms
    nominal_optimized_edges = dynamic_binning(
        nominal_values[is_HH & signal_nominal_mask], weights=weights[is_HH & signal_nominal_mask]
    )
    HH_nominal_hist = Histogram(f"no_sys_{nominal_postfix}", nominal_optimized_edges)
    QCD_nominal_hist = Histogram(f"no_sys_{nominal_postfix}", nominal_optimized_edges)
    ttbar_nominal_hist = Histogram(f"no_sys_{nominal_postfix}", nominal_optimized_edges)
    # Fill histograms
    HH_nominal_hist.fill(
        nominal_values[is_HH & signal_nominal_mask],
        weights=weights[is_HH & signal_nominal_mask],
    )
    QCD_nominal_hist.fill(
        nominal_values[is_QCD & signal_nominal_mask],
        weights=weights[is_QCD & signal_nominal_mask],
    )
    ttbar_nominal_hist.fill(
        nominal_values[is_ttbar & signal_nominal_mask],
        weights=weights[is_ttbar & signal_nominal_mask],
    )

    sample_names = ["ggF_k01", "multijet", "ttbar"]

    for sample_name, sample_clahh_hist, sample_nominal_hist in zip(
        sample_names,
        [HH_clahh_hist, QCD_clahh_hist, ttbar_clahh_hist],
        [HH_nominal_hist, QCD_nominal_hist, ttbar_nominal_hist],
    ):
        hists_output_name = f"{sample_name}_hists.h5"
        with h5py.File(hists_output_name, "w") as output_file:
            print(f"Saving histograms to file: {hists_output_name}")
            sample_out = output_file.create_group(sample_name)
            sample_out.attrs["type"] = "sample_type"
            sample_clahh_hist.write(sample_out)
            sample_nominal_hist.write(sample_out)

    if return_hists:
        return {
            "clahh": {
                "HH": HH_clahh_hist,
                "QCD": QCD_clahh_hist,
                "ttbar": ttbar_clahh_hist,
            },
            "nominal": {
                "HH": HH_nominal_hist,
                "QCD": QCD_nominal_hist,
                "ttbar": ttbar_nominal_hist,
            },
            "sample_names": sample_names,
        }


def plot_limit_histograms(
    hists_dict,
    plot_path,
    clahh_x_label="m(HH) [GeV]",
    nominal_x_label="m(HH) [GeV]",
    scale_signal=100,
):
    """
    Plot histograms for CLAHH and Nominal analyses.

    Parameters:
        HH_clahh_hist, QCD_clahh_hist, ttbar_clahh_hist:
            Histogram objects for the CLAHH analysis.
        HH_nominal_hist, QCD_nominal_hist, ttbar_nominal_hist:
            Histogram objects for the Nominal analysis.
        sample_names: List of sample names.
        plot_path: Path (or string) where the plots will be saved.
        clahh_postfix: String postfix for CLAHH signal-background combined plot filename.
        nominal_postfix: String postfix for Nominal signal-background combined plot filename.
    """
    HH_clahh_hist = hists_dict["clahh"]["HH"]
    QCD_clahh_hist = hists_dict["clahh"]["QCD"]
    ttbar_clahh_hist = hists_dict["clahh"]["ttbar"]
    HH_nominal_hist = hists_dict["nominal"]["HH"]
    QCD_nominal_hist = hists_dict["nominal"]["QCD"]
    ttbar_nominal_hist = hists_dict["nominal"]["ttbar"]
    sample_names = hists_dict["sample_names"]

    ###### Get combined background histograms ######
    background_clahh_combined_counts = np.sum(
        [h.counts for h in [QCD_clahh_hist, ttbar_clahh_hist]], axis=0
    )
    background_clahh_combined_errors = np.sqrt(
        np.sum(
            [h.errors**2 for h in [QCD_clahh_hist, ttbar_clahh_hist]],
            axis=0,
        )
    )
    background_nominal_combined_counts = np.sum(
        [h.counts for h in [QCD_nominal_hist, ttbar_nominal_hist]], axis=0
    )
    background_nominal_combined_errors = np.sqrt(
        np.sum(
            [h.errors**2 for h in [QCD_nominal_hist, ttbar_nominal_hist]],
            axis=0,
        )
    )

    ###### Plot comparing histograms for the different sample if the bins are the same ######
    if np.array_equal(HH_clahh_hist.edges, HH_nominal_hist.edges):
        for h_clahh, h_nominal, sample_name in zip(
            [HH_clahh_hist, QCD_clahh_hist, ttbar_clahh_hist],
            [HH_nominal_hist, QCD_nominal_hist, ttbar_nominal_hist],
            sample_names,
        ):
            fig, ax = plt.subplots()
            hplt.histplot(
                [h_clahh.counts[1:-1], h_nominal.counts[1:-1]],
                bins=h_clahh.edges[1:-1],
                label=[
                    f"CLAHH ({round(h_clahh.counts.sum(), 3)})",
                    f"Nominal ({round(h_nominal.counts.sum(), 3)})",
                ],
                yerr=[np.sqrt(h_clahh.errors[1:-1]), np.sqrt(h_nominal.errors[1:-1])],
                ax=ax,
            )
            ax.legend()
            ax.set_xlabel(nominal_x_label)
            ax.set_ylabel("Events")
            hplt.atlas.text("Work In Progress", loc=1)
            hplt.atlas.label(label=sample_name, loc=2, rlabel="")
            fig.savefig(f"{plot_path}/{h_clahh.name}_{sample_name}.png")
            plt.close(fig)

        fig, ax = plt.subplots()
        hplt.histplot(
            [background_clahh_combined_counts[1:-1], background_nominal_combined_counts[1:-1]],
            bins=HH_nominal_hist.edges[1:-1],
            label=[
                f"CLAHH ({round(background_clahh_combined_counts.sum(), 3)})",
                f"Nominal ({round(background_nominal_combined_counts.sum(), 3)})",
            ],
            yerr=[
                np.sqrt(background_clahh_combined_errors[1:-1]),
                np.sqrt(background_nominal_combined_errors[1:-1]),
            ],
            ax=ax,
        )
        ax.legend()
        ax.set_xlabel(nominal_x_label)
        ax.set_ylabel("Events")
        hplt.atlas.text("Work In Progress", loc=1)
        hplt.atlas.label(label="QCD multijet + ttbar", loc=2, rlabel="")
        fig.savefig(f"{plot_path}/{HH_clahh_hist.name}_bkg_combined.png")
        plt.close(fig)

    ### Plot combined background and signal ###
    fig, ax = plt.subplots()
    hplt.histplot(
        [HH_clahh_hist.counts[1:-1] * scale_signal, background_clahh_combined_counts[1:-1]],
        bins=HH_clahh_hist.edges[1:-1],
        label=[
            f"CLAHH ggF k01 x {scale_signal} ({round(HH_clahh_hist.counts.sum() * scale_signal, 3)})",
            f"CLAHH background ({round(background_clahh_combined_counts.sum(), 3)})",
        ],
        yerr=[
            np.sqrt(HH_clahh_hist.errors[1:-1] * scale_signal),
            np.sqrt(background_clahh_combined_errors[1:-1]),
        ],
        ax=ax,
    )
    ax.legend()
    ax.set_xlabel(clahh_x_label)
    ax.set_ylabel("Events")
    hplt.atlas.text("Work In Progress", loc=1)
    fig.savefig(f"{plot_path}/{HH_clahh_hist.name}_signal_bkg_combined.png")
    plt.close(fig)

    fig, ax = plt.subplots()
    hplt.histplot(
        [HH_nominal_hist.counts[1:-1] * scale_signal, background_nominal_combined_counts[1:-1]],
        bins=HH_nominal_hist.edges[1:-1],
        label=[
            f"Nominal ggF k01 x {scale_signal} ({round(HH_nominal_hist.counts.sum() * scale_signal, 3)})",
            f"Nominal background ({round(background_nominal_combined_counts.sum(), 3)})",
        ],
        yerr=[
            np.sqrt(HH_nominal_hist.errors[1:-1] * scale_signal),
            np.sqrt(background_nominal_combined_errors[1:-1]),
        ],
        ax=ax,
    )
    ax.legend()
    ax.set_xlabel(nominal_x_label)
    ax.set_ylabel("Events")
    hplt.atlas.text("Work In Progress", loc=1)
    fig.savefig(f"{plot_path}/{HH_nominal_hist.name}_signal_bkg_combined.png")
    plt.close(fig)


def calculate_upper_limit(hists):
    """Calculate the upper limit on the number of signal events."""
    signal_clahh = hists["clahh"]["HH"]["mass"].counts[1:-1]
    signal_nominal = hists["nominal"]["HH"]["mass"].counts[1:-1]
    bkg_clahh = (
        hists["clahh"]["QCD"]["mass"].counts[1:-1] + hists["clahh"]["ttbar"]["mass"].counts[1:-1]
    )
    bkg_nominal = (
        hists["nominal"]["QCD"]["mass"].counts[1:-1]
        + hists["nominal"]["ttbar"]["mass"].counts[1:-1]
    )
    bkg_clahh_error = np.sqrt(
        hists["clahh"]["QCD"]["mass"].errors[1:-1] ** 2
        + hists["clahh"]["ttbar"]["mass"].errors[1:-1] ** 2
    )
    bkg_nominal_error = np.sqrt(
        hists["nominal"]["QCD"]["mass"].errors[1:-1] ** 2
        + hists["nominal"]["ttbar"]["mass"].errors[1:-1] ** 2
    )

    clahh_model = pyhf.simplemodels.uncorrelated_background(
        signal=signal_clahh,
        bkg=bkg_clahh,
        # bkg_uncertainty=bkg_clahh_error,
        bkg_uncertainty=np.sqrt(bkg_clahh),
    )

    nominal_model = pyhf.simplemodels.uncorrelated_background(
        signal=signal_nominal,
        bkg=bkg_nominal,
        # bkg_uncertainty=bkg_nominal_error,
        bkg_uncertainty=np.sqrt(bkg_nominal),
    )

    poi_values = np.linspace(0, 20, 100)

    clahh_observations = np.concatenate([bkg_clahh, clahh_model.config.auxdata])
    nominal_observations = np.concatenate([bkg_nominal, nominal_model.config.auxdata])

    clahh_obs_limit, clahh_exp_limits, (clahh_scan, clahh_results) = (
        pyhf.infer.intervals.upper_limits.upper_limit(
            clahh_observations,
            clahh_model,
            poi_values,
            level=0.05,
            return_results=True,
            init_pars=[1.0] * clahh_model.config.npars,
            par_bounds=[[0, 100]] * clahh_model.config.npars,
        )
    )
    print(f"CLAHH upper limit (obs): μ = {clahh_obs_limit:.4f}")
    print(f"CLAHH upper limit (exp): μ = {clahh_exp_limits[2]:.4f}")

    nominal_obs_limit, nominal_exp_limits, (nominal_scan, nominal_results) = (
        pyhf.infer.intervals.upper_limits.upper_limit(
            nominal_observations,
            nominal_model,
            poi_values,
            level=0.05,
            return_results=True,
            init_pars=[1.0] * nominal_model.config.npars,
            par_bounds=[[0, 100]] * nominal_model.config.npars,
        )
    )
    print(f"Nominal upper limit (obs): μ = {nominal_obs_limit:.4f}")
    print(f"Nominal upper limit (exp): μ = {nominal_exp_limits[2]:.4f}")

    return clahh_exp_limits, nominal_exp_limits

    # model = pyhf.simplemodels.uncorrelated_background(
    #     signal=[5.0, 10.0], bkg=[50.0, 60.0], bkg_uncertainty=[5.0, 12.0]
    # )
    # observations = [53.0, 65.0] + model.config.auxdata
    # poi_values = np.linspace(0.1, 5, 50)
    # obs_limit, exp_limits, (scan, results) = pyhf.infer.intervals.upper_limits.upper_limit(
    #     observations, model, poi_values, level=0.05, return_results=True
    # )
    # print(f"Upper limit (obs): μ = {obs_limit:.4f}")
    # print(f"Upper limit (exp): μ = {exp_limits[2]:.4f}")

    # return exp_limits, obs_limit
