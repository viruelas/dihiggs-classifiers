import torch
import numpy as np
import awkward as ak
from pathlib import Path
import matplotlib.pyplot as plt
import mplhep as hplt
from sklearn.metrics import roc_curve, auc
from clahh.utils.metrics import get_score, binomial_error

plt.style.use(hplt.style.ATLAS)

INV_GEV = 1 / 1000

KIN_LABELS = {
    "pt": r"$p_T$",
    "eta": r"$\eta$",
    "phi": r"$\phi$",
    "mass": r"$m$",
}


def get_feature_label(feature: str, energy_unit: bool = False) -> str:
    split_feature = feature.split("_")
    is_kin = any([s in KIN_LABELS for s in split_feature])
    if is_kin:
        xlabel = " ".join([KIN_LABELS[s] if s in KIN_LABELS else s for s in split_feature])
    else:
        xlabel = feature

    if energy_unit:
        is_pt_or_mass = any([v in feature for v in ["pt", "mass", "m_4b"]])
        if is_pt_or_mass:
            xlabel = f"{xlabel} [GeV]"

    return xlabel


def get_com_lumi_label(com=13, lumi=None):
    com_label = r"$\sqrt{s} = \mathrm{" + str(com) + "\ TeV}"
    if lumi is not None:
        lumi_label = ",\ " + str(format(lumi, ".1f")) + r"\ \mathrm{fb}^{-1}$" if lumi else "$"
    else:
        lumi_label = "$"
    return com_label + lumi_label


def plot_metrics(metrics, plot_path: Path):
    fig, ax = plt.subplots()
    ax.plot(metrics["train_loss"], label="train")
    ax.plot(metrics["valid_loss"], label="validation")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")
    ax.legend()
    hplt.atlas.text("Work In Progress", loc=1)
    fig.savefig(plot_path / "metrics.png")
    plt.close()


def plot_roc(y_predict, y_truth, label_names, main_class, plot_path: Path):
    # get index of main class
    main_class_idx = label_names.index(main_class)
    fpr, tpr, threshold = roc_curve(y_truth[:, main_class_idx], y_predict[:, main_class_idx])
    fig, ax = plt.subplots()
    ax.plot(
        fpr,
        tpr,
        lw=2.5,
        label=f"ROC curve (area = {auc(fpr, tpr)*100:.1f})",
    )
    ax.set_xlabel("False positive rate")
    ax.set_ylabel("True positive rate")
    ax.set_ylim(0.001, 1)
    ax.set_xlim(0, 1)
    ax.grid(True)
    ax.legend(loc="upper left")
    hplt.atlas.text("Work In Progress", loc=1)
    fig.savefig(plot_path / "roc.png")
    plt.close()


def plot_roc_rej_v_eff(
    y_truth: np.ndarray,
    label_names: list,
    y_predict: np.ndarray = None,
    main_class: str = None,
    discriminant: np.ndarray = None,
    plot_path: Path = Path("plots"),
    QCD_frac: float = 0.92,
    comparisons: dict = None,
    plot_name_postfix: str = "",
):
    # get discriminant scores
    if discriminant is not None:
        disc = discriminant
    else:
        disc = get_score(
            y_pred=y_predict,
            class_labels=label_names,
            main_class=main_class,
            frac_dict={"label_QCD": QCD_frac, "label_ttbar": 1 - QCD_frac},
        )

    # defining target efficiency
    xlim_lower = 0.4
    ylim_lower = 1.0

    is_HH = y_truth[:, label_names.index("label_HH")] == 1
    is_QCD = y_truth[:, label_names.index("label_QCD")] == 1
    is_ttbar = y_truth[:, label_names.index("label_ttbar")] == 1

    infar = np.array([np.inf])
    edges = np.concatenate([-infar, np.linspace(-20, 20, 1000), infar])
    clahh_disc_hists = [
        np.histogram(disc[class_mask], edges)[0] for class_mask in [is_HH, is_QCD, is_ttbar]
    ]
    HH_eff = clahh_disc_hists[0][::-1].cumsum() / clahh_disc_hists[0].sum()
    QCD_eff = clahh_disc_hists[1][::-1].cumsum() / clahh_disc_hists[1].sum()
    ttbar_eff = clahh_disc_hists[2][::-1].cumsum() / clahh_disc_hists[2].sum()
    valid = (HH_eff > 0) & (QCD_eff > 0) & (ttbar_eff > 0)
    QCD_rej = 1 / QCD_eff[valid]
    ttbar_rej = 1 / ttbar_eff[valid]
    HH_eff = HH_eff[valid]

    # binomial error for backgrounds
    QCD_err, QCD_valid = binomial_error(
        sig_eff=HH_eff,
        bkg_rej=QCD_rej,
        n_test=np.sum(is_QCD),
        return_nonzero_mask=True,
    )
    ttbar_err, ttbar_valid = binomial_error(
        sig_eff=HH_eff,
        bkg_rej=ttbar_rej,
        n_test=np.sum(is_ttbar),
        return_nonzero_mask=True,
    )
    plt.figure()
    plt.plot(
        HH_eff[QCD_valid],
        QCD_rej[QCD_valid],
        linestyle="-",
        color="tab:blue",
        label="QCD rejection",
    )
    plt.fill_between(
        HH_eff[QCD_valid],
        QCD_rej[QCD_valid] - QCD_err,
        QCD_rej[QCD_valid] + QCD_err,
        color="tab:blue",
        alpha=0.5,
    )
    plt.plot(
        HH_eff[ttbar_valid],
        ttbar_rej[ttbar_valid],
        linestyle="--",
        color="tab:orange",
        label="ttbar rejection",
    )
    plt.fill_between(
        HH_eff[ttbar_valid],
        ttbar_rej[ttbar_valid] - ttbar_err,
        ttbar_rej[ttbar_valid] + ttbar_err,
        color="tab:orange",
        alpha=0.5,
    )
    if comparisons is not None:
        for label, point in comparisons.items():
            plt.plot(
                point[0],
                point[1],
                marker="o",
                markersize=5,
                color=plt.cm.tab10(list(comparisons.keys()).index(label) + 2),
                label=label,
            )
    plt.xlim(xlim_lower, 1)
    plt.ylim(ylim_lower, plt.ylim()[1] * 5)
    plt.xlabel("Signal efficiency")
    plt.ylabel("Background rejection")
    plt.legend()
    plt.semilogy()
    hplt.atlas.text("Work In Progress", loc=1)
    plot_name = f"roc_rej_vs_eff{'_' + plot_name_postfix if plot_name_postfix else ''}.png"
    plt.savefig(plot_path / plot_name)
    plt.close()


def plot_probabilities(
    y_predict,
    y_truth,
    label_names,
    plot_path: Path,
    bins: int = 50,
    weights: np.ndarray = None,
    plot_name_postfix: str = "",
):
    fig, ax = plt.subplots()
    for i, class_label in enumerate(label_names):
        clean_label = class_label.replace("label_", "")
        class_mask = y_truth[:, i] == 1
        class_vals = y_predict[class_mask][:, i]
        class_wgts = weights[class_mask] if weights is not None else None
        ax.hist(
            class_vals,
            bins=bins,
            label=clean_label,
            histtype="step",
            weights=class_wgts,
        )
    ax.set_xlabel("Probability")
    ax.set_ylabel("Events")
    ax.legend()
    ax.semilogy()
    hplt.atlas.text("Work In Progress", loc=1)
    plot_name = f"probabilities{'_' + plot_name_postfix if plot_name_postfix else ''}.png"
    fig.savefig(plot_path / plot_name)
    plt.close()


def plot_discriminants(
    y_predict,
    y_truth,
    label_names,
    main_class,
    plot_path: Path,
    weights: np.ndarray = None,
    bins: int = 50,
    plot_name_postfix: str = "",
):
    discriminants = get_score(
        y_pred=y_predict,
        class_labels=label_names,
        main_class=main_class,
        frac_dict={"label_QCD": 0.82, "label_ttbar": 1 - 0.82},
    )

    fig, ax = plt.subplots()
    for i, class_label in enumerate(label_names):
        clean_label = class_label.replace("label_", "")
        class_mask = y_truth[:, i] == 1
        ax.hist(
            discriminants[class_mask],
            bins=bins,
            label=clean_label,
            histtype="step",
            weights=weights[class_mask] if weights is not None else None,
        )
    ax.set_xlabel(r"$\mathcal{D_{HH}}$")
    ax.set_ylabel("Events")
    ax.legend(loc="upper right")
    ax.semilogy()
    hplt.atlas.text("Work In Progress", loc=1)
    plot_name = f"discriminants{'_' + plot_name_postfix if plot_name_postfix else ''}.png"
    fig.savefig(plot_path / plot_name)
    plt.close()


def plot_vars(
    x,
    y=None,
    weights=None,
    plot_path: Path = Path("plots"),
    plot_name_postfix: str = "",
    bins: int = 50,
    lumi=None,
    log_scale=True,
):
    """Plot jet kinematics for each feature in x."""

    if weights is not None:
        weights = weights * lumi if lumi is not None else weights

    for feature in x.fields:
        feature_values = x[feature]

        is_pt_or_mass = any([v in feature for v in ["pt", "mass", "m_4b"]])
        if is_pt_or_mass:
            feature_values = feature_values * INV_GEV

        fig, ax = plt.subplots()
        if y is not None:
            for label in y.fields:
                class_mask = y[label] == 1
                class_feature = np.ravel(feature_values[class_mask]).to_numpy()
                valid = np.isfinite(class_feature)
                class_feature = class_feature[valid]
                class_weights = (
                    np.ravel(weights[class_mask])[valid].to_numpy() if weights is not None else None
                )
                hist, edges = np.histogram(
                    class_feature,
                    bins=bins,
                    weights=class_weights,
                )
                hplt.histplot(
                    hist,
                    bins=edges,
                    label=label,
                    ax=ax,
                )
        else:
            class_feature = np.ravel(feature_values).to_numpy()
            valid = np.isfinite(class_feature)
            class_weights = np.ravel(weights[valid]).to_numpy() if weights is not None else None
            hist, edges = np.histogram(
                class_feature,
                bins=bins,
                weights=class_weights,
            )
            hplt.histplot(
                hist,
                bins=edges,
                label=feature,
                ax=ax,
            )

        if log_scale:
            ax.semilogy()
        ax.set_xlabel(get_feature_label(feature, energy_unit=is_pt_or_mass))
        ax.set_ylabel("Events")
        ax.legend(loc="upper right")
        hplt.atlas.text("Work In Progress", loc=1)
        hplt.atlas.label(label=get_com_lumi_label(lumi=lumi), loc=2, rlabel="")
        plot_name = f"{feature}{'_' + plot_name_postfix if plot_name_postfix else ''}.png"
        fig.savefig(plot_path / plot_name)
        plt.close()


def visualize_feature(
    data_loder,
    label_names: list,
    feature_name: str,
    leading=False,
    bins: list = 50,
    lumi=None,
    dataset=None,
    density=False,
    log_scale=True,
    weight_negative=True,
    plot_name: str = "feature_resampled.png",
):
    """Plot jet feature for each label in label_names."""

    is_pt_or_mass = any([v in feature_name for v in ["pt", "mass", "m_4b"]])
    hists = [
        np.zeros(bins if type(bins) == int else len(bins) - 1, dtype=float) for _ in label_names
    ]
    num_jets = 0

    for batch in data_loder:
        labels = batch.y.numpy()
        if labels.ndim == 1:
            labels = np.eye(len(label_names), dtype=int)[labels]
        weights = batch.weights.numpy()
        feature = batch.spectator.numpy()
        num_jets = feature.shape[-1]
        # if feature first dimenstion is different than labels first dimension, it means it was flattened and needs to be reshaped by labels first dimension
        if feature.shape[0] != labels.shape[0]:
            feature = feature.reshape(labels.shape[0], -1)
        # sort feature values in descending order and ignore NaNs
        sorted_idx = np.argsort(np.where(np.isnan(feature), np.inf, -feature), axis=1)
        feature = feature[np.arange(feature.shape[0])[:, None], sorted_idx]
        if leading:
            feature = feature[:, 0]
        for label_idx in range(len(label_names)):
            class_mask = labels[:, label_idx] == 1
            feature_values = feature[class_mask] * (INV_GEV if is_pt_or_mass else 1)
            class_weights = np.sign(weights[class_mask])
            hist, _ = np.histogram(
                feature_values.flatten(),
                bins=bins,
                weights=class_weights if weight_negative else None,
            )
            hists[label_idx] += hist

    if dataset is not None:
        if type(dataset) == torch.utils.data.Subset:
            labels = dataset[:]["y"].numpy()
            weights = dataset[:]["weights"].numpy()
            feature = dataset[:]["spectator"].numpy()
        else:
            labels = np.transpose([dataset[y].to_numpy() for y in label_names])
            weights = dataset.event_weight.to_numpy()
            feature = ak.pad_none(dataset[feature_name], num_jets, clip=True).to_numpy()
        # sort feature values in descending order and ignore NaNs
        sorted_idx = np.argsort(np.where(np.isnan(feature), np.inf, -feature), axis=1)
        feature = feature[np.arange(feature.shape[0])[:, None], sorted_idx]
        if leading:
            feature = feature[:, 0]

    for label_idx, label in enumerate(label_names):
        fig, ax = plt.subplots()
        hist = hists[label_idx]
        binning = np.linspace(hist.min(), hist.max(), bins + 1) if type(bins) == int else bins
        hplt.histplot(
            hist,
            binning,
            label=f"{label} resampled",
            ax=ax,
            density=density,
        )
        if dataset is not None:
            class_mask = labels[:, label_idx] == 1
            feature_values = feature[class_mask] * (INV_GEV if is_pt_or_mass else 1)
            class_weights = weights[class_mask]
            hist, binning = np.histogram(feature_values.flatten(), bins=bins, weights=class_weights)
            hplt.histplot(
                hist,
                binning,
                label=f"{label} weighted",
                ax=ax,
                density=density,
            )
        if log_scale:
            ax.semilogy()
        xlabel = get_feature_label(feature_name, energy_unit=is_pt_or_mass)
        if leading:
            xlabel = f"Leading {xlabel}"
        ax.set_xlabel(xlabel)
        ax.set_ylabel("Events")
        ax.legend(loc="upper right")
        hplt.atlas.text("Work In Progress", loc=1)
        hplt.atlas.label(label=get_com_lumi_label(lumi=lumi), loc=2, rlabel="")
        # save figure, plot_name is a Path object which includes absolute path for the plot, get only the name of the plot without the extension but including the absolute path
        plot_directory = plot_name.parent
        fig.savefig(plot_directory / f"{plot_name.stem}_{label}{plot_name.suffix}")
        plt.close()


def visualize_class_count(data_loader, label_names=[], nbatches=None, plot_name="class_counts.png"):
    class_counts = []

    # enumerate only the first nbatches batches
    for batch in data_loader:
        # check if batch has attribute called 'y'
        labels = batch.y.numpy()
        if labels.ndim == 1:
            labels = np.eye(len(label_names), dtype=int)[labels]
        class_counts.append(
            [np.sum(labels[:, label_idx] == 1) for label_idx in range(len(label_names))]
        )

    class_counts = np.array(class_counts)
    batch_idices = np.arange(len(class_counts))

    width = 1 / len(label_names)
    fig, ax = plt.subplots()

    for class_idx in range(len(label_names)):
        ax.bar(
            batch_idices + width * class_idx,
            class_counts[:, class_idx],
            width,
            label=" ".join(
                [label_names[class_idx], f"(total {int(sum(class_counts[:, class_idx]))})"]
            ),
        )
    ax.legend(loc="upper right")
    ax.set_xlabel("Batch Number", fontsize=18)
    ax.set_xlim(xmin=0, xmax=nbatches if nbatches else batch_idices.max())
    ax.set_ylabel("Class Count", fontsize=18)
    # ax.set_xticks(batch_idices)
    # ax.xaxis.set_major_locator(plt.MultipleLocator(5))
    fig.savefig(plot_name)
    plt.close()
