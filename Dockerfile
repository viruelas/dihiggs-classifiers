# base image
FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime

# local and envs
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_NO_CACHE_DIR=false
ARG DEBIAN_FRONTEND=noninteractive

# add some packages
RUN apt-get update && apt-get install -y git git-lfs h5utils wget vim build-essential

# update python pip
RUN python3 -m pip install --upgrade pip
RUN python3 --version
RUN python3 -m pip --version

# copy source code
COPY . /opt/clahh
# install package
RUN python3 -m pip install --no-cache-dir -e /opt/clahh

# create working directory
WORKDIR /home
